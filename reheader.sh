#!/bin/bash
set -e

headerfile="source-header.txt"

if [ ! -f ${headerfile} ]; then
	echo "File ${headerfile} not found."
	exit 1
fi

for f in $(find -iname '*.java'|grep -v '\.svn'); do
	if [ 0 -eq $(grep -c '^package' ${f}) ]; then
		echo "No line starting with 'package' found in ${f}. Skipping file."
	else
		tmpfile=$(mktemp)
		awk '/^package/ {i=1} i>0 {print $LINE}' ${f} > ${tmpfile}
		cat ${headerfile} > ${f}
		cat ${tmpfile} >> ${f}
		rm -rf ${tmpfile}
	fi
done
