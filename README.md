# What is MoSDi?

MoSDi contains a lot of sequence analysis algorithms, including methods for

 1. motif statistics, e.g. compute the exact occurrence count distribution of a motif,
 2. exact motif discovery: extraction of motifs with provably optimal p-value,
 3. analysis of pattern matching algorithms: compute (for given algorithm and pattern) the exact distribution of the number of character accesses caused by searching a random text,
 4. statistics of fragment masses resulting from proteolytic cleavage of proteins,
 5. computing the expectated read length of sequencing reads for a given dispensation order (for 454 or IonTorrent?),
 6. analysing sensitivity of spaced alignment seeds. 

Besides that, MoSDi contains a lot of useful tiny features; for example, you can

 * count all q-grams in a text,
 * generate a random text distributed according to i.i.d. or Markovian text models,
 * enumerate all IUPAC motifs (subject user-specified constraints),
 * cut out all occurrences of a IUPAC motif from given sequence,
 * output a position frequency matrix of all occurrences of a IUPAC motif,
 * ... 

# Documentation

Right now, there are four sources of documentation on MoSDi you can use:

 1. this README
 2. the usage information provided by all tools when called without parameters,
 3. the [API documentation](http://homepages.cwi.nl/~tm/files/mosdi-api),
 4. of course, the source code itself. 

# Contributions
Most parts of MoSDi have been written by [Tobias Marschall (me)](http://people.mpi-inf.mpg.de/~marschal). I started
working on this project in 2007 at Bielefeld University, continued while doing my
PhD at TU Dortmund in Sven Rahmann's group (until 2011).
Some contributions have been made by Markus Kemmerling and 
Wolfgang Walz who worked as student assistants in Sven's group.

## jopt-simple

MoSDi uses [jopt-simple](http://jopt-simple.sourceforge.net/) to parse command line 
arguments. For convenience, jopt-simple is included in the MoSDi JAR file.

# Quick start
Call:
```
java -jar mosdi-XXX.jar
```

This calls mosdi.tools.All which means that you get a full list of all available
subcommands. You might prefer to use the tools Stat, Discovery, PatternMatchingAnalysis
Utils. In this way you get the same functionality but grouped such that you (hopefully)
find the right subcommand quicker:

```
java -classpath mosdi-XXX.jar mosdi.tools.Stat
java -classpath mosdi-XXX.jar mosdi.tools.Discovery
java -classpath mosdi-XXX.jar mosdi.tools.PatternMatchingAnalysis
java -classpath mosdi-XXX.jar mosdi.tools.Utils
java -classpath mosdi-XXX.jar mosdi.tools.ProteinFragmentStats
java -classpath mosdi-XXX.jar mosdi.tools.ReadLengthAnalysis
java -classpath mosdi-XXX.jar mosdi.tools.AlignmentSeedStats
```

# Installing MoSDi on UNIX-like systems
If you like MoSDi you might want to have all tools in your PATH. To this end,
install.sh copies the JAR file and creates some wrapper scripts to start the
different components of MoSDi. As a parameter to install.sh you need to give the
destination, e.g.

```
./install.sh /usr/local
```
(For system-wide installation.)

```
./install.sh $HOME/.local
```
(For installation in your home directory.)

After that, you can call mosdi-stat, mosdi-discovery, mosdi-pm-analysis, mosdi-fragment-stat,
mosdi-dispensation-order, mosdi-seed-stat, and mosdi-utils.

The environment variable MOSDI_JVM_ARGS can be used to pass additional arguments to the JVM.
For example, to allow a memory usage of one gigabyte, you can call

```
MOSDI_JVM_ARGS='-Xmx1g' mosdi-discovery ...
```

# Introduction 

This page contains examples on common use cases. It does not cover all of MoSDi's functionality, but I believe this page to be a good starting point to get your task done. MoSDi is not designed to be a monolithic application, but a flexible toolkit with small tools that can (hopefully) be combined to everyones need. After (or even before) having a look at some examples on this page, it might be a good idea to just call the different commands, read the usage instructions, and play around with them...

## MoSDi Tools

The tools perform the following tasks.

| *Tool*                         | *Task*                                                                                              |
|--------------------------------|-----------------------------------------------------------------------------------------------------|
| `mosdi-stat`                   | Pattern matching statistics. Supports exact, Poisson, and compound Possion computations.            |
| `mosdi-discovery`              | Motif discovery using IUPAC motifs and Markovian text models.                                       |      
| `mosdi-pm-analysis`            | Distribution of running time cost of pattern matching algorithms such as Horspool, BOM, B(N)DM.     |
| `mosdi-utils`                  | Utilities like counting q-grams, creating random text, enumerating patterns, counting matches, etc. |
| `mosdi-fragment-stat`          | Statistics of fragment masses resulting from proteolytic cleavage of proteins.                      |
| `mosdi-dispensation-order`     | Expectated read length of sequencing reads for a given dispensation order (for 454 or IonTorrent)   |
| `mosdi-seed-stat`              | Sensitivity of spaced alignment seeds                                                               |

## Output
All MoSDi tools have the global options `-v` and `-v2` to increase verbosity of output. By convention, the important lines of output (wherever possible) start with ">>". Such that you can turn verbose output on, write all output to a log file and grep for lines starting with ">>".

## IUPAC Motifs 
MoSDi uses IUPAC motifs. A IUPAC motif is a pattern given over the [IUPAC alphabet](http://www.bioinformatics.org/sms2/iupac.html). For example, the pattern `AYAYT` stands for `A[CT]A[CT]T` and matches `ACACT`, `ACATT`, `ATACT`, and `ATATT`. If the reverse complement is also considered (in many subcommands, this is switched on by option -r), `AYAYT` also matches `AATAT`, `AATGT`, `AGTAT`, and `AGTGT`.

# Use Cases 


## Compute Distribution of Occurrence Counts 
*Task:* What is the distribution of the number of occurrences of a IUPAC motif for a random text of a given length; i.e. compute the probability that the motif occurs k times for all k (up to a user specified maximum `<max>`).

```
mosdi-stat count-dist -n <text-length> -m <max> exact AYAYT
```

*Note:* The last value in the returned distribution has a special meaning; it is the probability that the motif occurs `<max>` *or more* times. Therefore, the given distribution always sums to 1.0.

The parameter `exact` says that the exact distribution should be computed. This can be quite slow when the text length is large. In some situations it can be faster to use the so-called "doubling algorithm". _Best practice_: Use the option `-C` to let the program decide which algorithm to use.

In most cases, computing a _compound Poisson_ approximation is a lot faster. It is pretty accurate for rare patterns. That means, it becomes inaccurate when the sequence "gets filled up with matches"; e.g. if you ask for the probability to find 9 matches of a length-10 pattern in a text of length 100.

```
mosdi-stat count-dist -n <text-length> -m <max> comp-poisson AYAYT
```

If no more parameters are specified, the distribution with respect to a uniform text model is computed. That means, all characters are assumed to have equal probability. To use other background models, a table of q-gram counts can be provided. If, for example, a q-gram table for q=3 is provided, a Markovian text model of order 2 is estimated from this table:

```
mosdi-utils count-qgrams background-sequences.fasta 3 > qgram-table
mosdi-stat count-dist -n <text-length> -m <max> -q qgram-table comp-poisson AYAYT
```

## Compute Motif Significance 
*Task:* You have sequences and a IUPAC motif which occurs k times in this sequences and want to know the probability that this happened by chance (p-value); i.e. the probability that the motif occurs k or more times in a random text of the same length.

In this case, `mosdi-stat` can determine the number of matches for you and compute the distribution of occurrence counts and sum up the tail probability.

```
mosdi-stat count-dist -p sequences.fasta comp-poisson AYAYT
```

As noted in Use Case _"Compute Distribution of Occurrence Counts"_ above, the last value in the returned distribution has the special meaning that it is the probability of finding `<max>` or more occurrences. It hence equals the sought tail probability.

By default, an i.i.d. text model estimated from the given sequences is used as a background model; e.g. if 22.7% of all characters in the given sequences are As, this is also assumed for the random text. For DNA, it is advisable to use context-aware text models: to estimate a Markovian text model from the provided sequences, use option `-V <model-order>`.

## Run Motif Discovery 
*Task:* Find motifs that extraordinarily abundant. More formally: Given one or more sequences, find a IUPAC motif with the lowest p-value (within a given space of motifs); i.e. the probability that this motif is a random hit is minimized.

You might want to start with a moderate pattern length (e.g. 8) to get first results rather quickly and then increase it.

```
mosdi-discovery -v discovery -i 8 occ-count sequences.fasta
```

The verbosity switch `-v` is useful to give you progress information. The switch `-i` tells MoSDi to ignore all unknown characters in the input sequences (such as `N`s). Specifying `occ-count` selects the total number of motif occurrences as an objective function. Alternatively, choosing `seq-count` would tell MoSDi to optimize with respect to the number of sequences with at least one motif occurrence.

*Note:* It is possible to use two objectives at once. Then, the set of
[Pareto optimal](http://en.wikipedia.org/wiki/Pareto_efficiency#Pareto_frontier) solutions is returned. This feature, however, is EXPERIMENTAL and has not been thoroughly tested.

By default, mosdi-discovery allows patterns composed of {A,C,G,T} and at most `<length>/2` Ns. To allow other wildcards (at the expense of running time), the switch -M is used.

```
mosdi-discovery -v discovery -i -E 5.0 -M 8,2,0,3 8 occ-count sequences.fasta
```

Here we allow 8 characters from {A,C,G,T}, 2 from {W,S,R,Y,K,M}, 0 from {B,D,H,V}, and 3 Ns. Furthermore, we have added `-E 5.0` to restrict the search to patterns with expectation <=5. In most practical settings, we are not interested in patterns with high expectation anyway and restricting the search space in this way can significantly improve running time.

### Specifying a P-Value Threshold 
In some situations you might want to not only compute the best motif, but to return all motifs with a p-value better than a specified threshold. This can be done using the option `-T`.

```
mosdi-discovery -v discovery -i -T 1e-50 -E 5.0 -M 8,2,0,3 8 occ-count sequences.fasta
```

### Note on Multiple Testing 
When optimizing over a large motif space, we are implicitly testing each motif for significance. Therefore, we perform [multiple hypothesis testing](http://en.wikipedia.org/wiki/Multiple_comparisons) and need to interpret p-values with some caution. In the spirit of [Bonferroni correction](http://en.wikipedia.org/wiki/Bonferroni_correction), it is useful to consider the reciprocals of the number of motifs in the considered motif space to get an idea of the order of magnitude of p-values that can be expected to occur due to multiple testing alone. To compute the size of a motif space, run:
`
mosdi-utils iupac-gen -c -M 8,2,0,3 8
`
In this case, the motif space consists of 19,476,480 motifs and thus a motif with p-value in the order of magnitude of 10^-8^ cannot be called significant when using Bonferroni correction.

### Going Parallel 
For large motif spaces, long input texts and high-order text models, exact motif discovery gets rather slow. The search, however, can perfectly be parallelized. To distribute the work load, `mosdi-discovery` can be instructed to search only parts of the search space using the options `-l` and `-u`:

```
mosdi-discovery -v discovery -i -l C -u CCGG -M 8,2,0,3 8 occ-count sequences.fasta
```

Now, `mosdi-discovery` only scans motifs lexicographically between C and CCGG. I find it useful to combine parallel motif search with a p-value threshold (option `-T`). To split a motif space into equally-sized chunks, run:

```
mosdi-utils iupac-ranges -M 8,2,0,3 100 8
```

## Re-Evaluating to Find the Second Best Motif 
*Task:* You have found the best motif and want to find the second best.

In practice, the second best motif often turns out to be strongly related to the best one. One option is to mask all occurrences of the best motif from the input sequences:

```
mosdi-utils cut-out-motif -M X sequences.fasta AYAYT
```

Using the switch `-M X`, all instances of the motif `AYAYT` are replaced by the character `X`.

After masking the best motif we can run the whole motif discovery procedure again. If you feel that this is too slow, it is an option to extract all motifs better than a p-value threshold in the first place and re-evaluate these motif with respect the cut sequences using the `mosdi-discovery calc-scores` command. Here, is a whole workflow:

```
# run thresholded motif discovery
mosdi-discovery -v discovery -i -T1e-20 8 occ-count sequences.fasta > output

# find best motif
grep '^>> ' output | sort -g -k 5 | tail -n 1

# mask best motif 
mosdi-utils cut-out-motif -M X sequences.fasta BESTMOTIF > masked-sequence.fasta

# extract all motifs from output of mosdi-discovery
grep '^>> ' output | cut -d ' ' -f 2 > all-motifs

# re-evaluate
mosdi-discovery calc-scores -i -F cut-sequence.fasta all-motifs
```

## Analyzing Pattern Matching Algorithms
*Task:* Compute the exact distribution of character accesses needed by one of the pattern matching algorithms Horspool, BOM, or B(N)DM to process a random text.

*Reference:* Tobias Marschall and Sven Rahmann. Exact Analysis of Pattern Matching Algorithms with Probabilistic Arithmetic Automata. [arxiv: 1009.6114](http://arxiv.org/abs/1009.6114 ).

Example: To compute the distribution of comparisons needed by BOM to search a random text of length 100 generated by a uniform i.i.d. text model, run

```
mosdi-pm-analysis cost-distribution bom ACACA 100
```

### Statistics on Automata Sizes
*Task:* For all patterns of a given length, compute the number of states for automata used to analysis pattern matching algorithms.

Sizes of automata for BOM algorithms for all patterns of length 4:

```
mosdi-pm-analysis automata-sizes bom 4
```

Note: When setting verbosity to 2, not only sizes but the automata themselves are given:

```
mosdi-pm-analysis -v2 automata-sizes bom 4
```

# References

## Motif Statistics
Tobias Marschall and Sven Rahmann. Probabilistic arithmetic automata and their application to pattern matching statistics. In Proceedings of the 19th Annual Symposium on Combinatorial Pattern Matching (CPM), pages 95–106, 2008. DOI: 10.1007/978-3-540-69068-9_11.

## Motif Discovery

Tobias Marschall and Sven Rahmann. Efficient exact motif discovery. Bioinformatics (Proceedings of ISMB), 25(12):i356–364, 2009. DOI: 10.1093/bioinformatics/btp188.

Tobias Marschall and Sven Rahmann. Speeding up Exact Motif Discovery by Bounding the Expected Clump Size. In Proceedings of the 10th Workshop on Algorithms in Bioinformatics (WABI), pages 337-349, 2010. DOI: 10.1007/978-3-642-15294-8_28

## Pattern Matching Analysis

Tobias Marschall and Sven Rahmann. Exact Analysis of Pattern Matching Algorithms with Probabilistic Arithmetic Automata. arxiv: 1009.6114. 

# Feedback
Please feel free to send all feedback/questions/suggestions to [Tobias Marschall](http://people.mpi-inf.mpg.de/~marschal).
