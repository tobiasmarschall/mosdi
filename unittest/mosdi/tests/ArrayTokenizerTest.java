/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.ArrayTokenizer;
import static org.junit.Assert.assertArrayEquals;

public class ArrayTokenizerTest extends TestCase {

	public void testTokenizer0() {
		int[] array = {77, 60, 63, 52, 36, 90, 9, 45, 55, 62, 91, 51, 95, 18, 45, 57, 93, 79, 69, 95};
		ArrayTokenizer at = new ArrayTokenizer(array,-1);
		assertTrue(at.hasNext());
		assertArrayEquals(array, at.next());
		assertFalse(at.hasNext());
	}


	public void testTokenizer1() {
		int[] array = {85, 87, 27, 40, 6, 100, 100, 100, 47, 67, 56, 92, 100, 36, 95, 95, 93, 15, 97, 51, 99, 2, 100, 80, 20, 5, 29, 97, 17, 60, 71, 59, 26, 16, 93, 60, 40, 97, 6, 80, 50, 59, 82, 51, 100, 100, 17, 44, 38, 94, 38, 64, 0, 16, 100, 100, 100};
		int[] part0 = {85, 87, 27, 40, 6};
		int[] part1 = {47, 67, 56, 92};
		int[] part2 = {36, 95, 95, 93, 15, 97, 51, 99, 2};
		int[] part3 = {80, 20, 5, 29, 97, 17, 60, 71, 59, 26, 16, 93, 60, 40, 97, 6, 80, 50, 59, 82, 51};
		int[] part4 = {17, 44, 38, 94, 38, 64, 0, 16};
		ArrayTokenizer at = new ArrayTokenizer(array,100);
		assertTrue(at.hasNext());
		assertArrayEquals(part0, at.next());
		assertTrue(at.hasNext());
		assertArrayEquals(part1, at.next());
		assertTrue(at.hasNext());
		assertArrayEquals(part2, at.next());
		assertTrue(at.hasNext());
		assertArrayEquals(part3, at.next());
		assertTrue(at.hasNext());
		assertArrayEquals(part4, at.next());
		assertFalse(at.hasNext());
	}

}
