/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import java.util.Arrays;

import junit.framework.TestCase;
import mosdi.util.iterators.AbelianPatternInstanceIterator;

public class AbelianPatternInstanceIteratorTest extends TestCase {

	static private void assertEqualsIntArray(int[] a, int b0, int b1, int b2, int b3) {
		assertEquals(4,a.length);
		if ((b0!=a[0])||(b1!=a[1])||(b2!=a[2])||(b3!=a[3])) {
			fail(String.format("expected: [%d, %d, %d, %d], actual: %s", b0, b1, b2, b3, Arrays.toString(a)));
		}
	}
	
	public void testBasic() {
		int[] abelianPattern = {1,0,1,2,0};
		AbelianPatternInstanceIterator iterator = new AbelianPatternInstanceIterator(abelianPattern);
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 0,2,3,3);
		assertEquals(0,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 0,3,2,3);
		assertEquals(1,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 0,3,3,2);
		assertEquals(2,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 2,0,3,3);
		assertEquals(0,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 2,3,0,3);
		assertEquals(1,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 2,3,3,0);
		assertEquals(2,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 3,0,2,3);
		assertEquals(0,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 3,0,3,2);
		assertEquals(2,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 3,2,0,3);
		assertEquals(1,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 3,2,3,0);
		assertEquals(2,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 3,3,0,2);
		assertEquals(1,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 3,3,2,0);
		assertEquals(2,iterator.getLeftmostChangedPosition());
		assertFalse(iterator.hasNext());
	}
	
	public void testSkip() {
		int[] abelianPattern = {1,0,1,2,0};
		AbelianPatternInstanceIterator iterator = new AbelianPatternInstanceIterator(abelianPattern);
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 0,2,3,3);
		assertEquals(0,iterator.getLeftmostChangedPosition());
		iterator.skip(3);
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 0,3,2,3);
		assertEquals(1,iterator.getLeftmostChangedPosition());
		iterator.skip(1);
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 2,0,3,3);
		assertEquals(0,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 2,3,0,3);
		assertEquals(1,iterator.getLeftmostChangedPosition());
		iterator.skip(1);
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 3,0,2,3);
		assertEquals(0,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEqualsIntArray(iterator.next(), 3,0,3,2);
		assertEquals(2,iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		iterator.skip(0);
		assertFalse(iterator.hasNext());
	}	
	
}
