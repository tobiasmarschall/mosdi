/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import java.util.Arrays;

import junit.framework.TestCase;
import mosdi.fa.SuffixOracle;
import mosdi.util.Alphabet;

public class SuffixOracleTest extends TestCase {

	public void testOracle() {
		Alphabet alphabet = new Alphabet("anq");
		int[] pattern = alphabet.buildIndexArray("nannannnq");
		SuffixOracle so = new SuffixOracle(alphabet.size(),pattern);
		assertTrue(so.mightBeFactor(alphabet.buildIndexArray("nnann")));
		assertTrue(so.mightBeFactor(alphabet.buildIndexArray("nnq")));
		assertTrue(so.mightBeFactor(alphabet.buildIndexArray("anna")));
		assertFalse(so.mightBeFactor(alphabet.buildIndexArray("aa")));
		assertFalse(so.mightBeFactor(alphabet.buildIndexArray("nnnn")));
		assertFalse(so.mightBeFactor(alphabet.buildIndexArray("nnna")));
		assertFalse(so.mightBeFactor(alphabet.buildIndexArray("qq")));
		assertFalse(so.mightBeFactor(alphabet.buildIndexArray("anan")));
	}
	
	public void testSuffixAutomaton() {
		Alphabet alphabet = Alphabet.getDnaAlphabet();
		int[] text = alphabet.buildIndexArray("TACACATGTGCTTCAAAACT");
		SuffixOracle sa = new SuffixOracle(alphabet.size(), text);
		for (int i=0; i<text.length; ++i) {
			for (int j=i+1; j<=text.length; ++j) {
				int[] query = Arrays.copyOfRange(text, i, j);
				assertTrue(sa.mightBeFactor(query));
			}
		}
	}

}
