/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import mosdi.util.Log;
import mosdi.util.Log.Level;

import junit.framework.TestCase;

public class LogTest extends TestCase {

   public void testPrintfLogLevel() {      
      // redirect System.out to an other stream
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      PrintStream ps = new PrintStream(os);
      System.setOut(ps);
      
      String output;
      Log.setLogLevel(Level.STANDARD);
      
      Log.setTimingActive(false);
      Log.printf(Level.STANDARD, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("Test String 1.000000e-02 10 10.010000", output);
      
      Log.printf(Level.VERBOSE, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("", output);
      
      Log.setLogLevel(Level.VERBOSE);
      Log.printf(Level.VERBOSE, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("Test String 1.000000e-02 10 10.010000", output);
      
      Log.printf(Level.DEBUG, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("", output);
      
      Log.setLogLevel(Level.DEBUG);
      Log.printf(Level.DEBUG, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("Test String 1.000000e-02 10 10.010000", output);
      
      Log.setLogLevel(Level.STANDARD);
      Log.printf(Level.DEBUG, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("", output);
      
      Log.setTimingActive(true);
      Log.printf(Level.STANDARD, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("Test String 1.000000e-02 10 10.010000", output);
      
      Log.printf(Level.VERBOSE, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("", output);
      
      Log.setLogLevel(Level.VERBOSE);
      Log.printf(Level.VERBOSE, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("Test String 1.000000e-02 10 10.010000", output);
      
      Log.printf(Level.DEBUG, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("", output);
      
      Log.setLogLevel(Level.DEBUG);
      Log.printf(Level.DEBUG, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("Test String 1.000000e-02 10 10.010000", output);
      
      Log.setLogLevel(Level.STANDARD);
      Log.printf(Level.DEBUG, "Test %s %e %d %f", "String", 0.01, 10, 10.01);
      output = os.toString();
      os.reset();
      assertEquals("", output);
      
   }
   
   public void testPrintfVerboseFormating() {
      // redirect System.out to an other stream
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      PrintStream ps = new PrintStream(os);
      System.setOut(ps);

      Log.setLogLevel(Level.STANDARD);
      String output;
      
      Log.setTimingActive(false);
      Log.printf(Level.STANDARD, "Test %2$15.3e %1$10s %4$2f %3$5d", "String", 0.0142319, 10, 10.0142319);
      output = os.toString();
      os.reset();
      assertEquals("Test       1.423e-02     String 10.014232    10", output);
      Log.setTimingActive(true);
      Log.printf(Level.STANDARD, "Test %2$15.3e %1$10s %4$2f %3$5d", "String", 0.0142319, 10, 10.0142319);
      output = os.toString();
      os.reset();
      assertEquals("Test       1.423e-02     String 10.014232    10", output);
   }
   
   public void testPrintfWithTimes() {
      // redirect System.out to an other stream
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      PrintStream ps = new PrintStream(os);
      System.setOut(ps);

      Log.setLogLevel(Level.STANDARD);
      String output;
      
      Log.setTimingActive(false);
      Log.printf(Level.STANDARD, ">>> %s %e %d %d %d %t %t %t %e", "A String", 0.23412, 98, 1234, Long.MIN_VALUE, 0.02, 0.099, 1.003, 0.95);
      output = os.toString();
      os.reset();
      assertEquals(">>> A String 2.341200e-01 98 1234 -9223372036854775808 NaN NaN NaN 9.500000e-01", output);
      
      Log.printf(Level.STANDARD, ">>> %t%n",0.234125);
      output = os.toString();
      os.reset();
      assertEquals(String.format(">>> %s%n","NaN"), output);
      
      Log.setTimingActive(true);
      Log.printf(Level.STANDARD, ">>> %s %e %d %d %d %t %t %t %e", "A String", 0.23412, 98, 1234, Long.MIN_VALUE, 0.02, 0.099, 1.003, 0.95);
      output = os.toString();
      os.reset();
      assertEquals(">>> A String 2.341200e-01 98 1234 -9223372036854775808 2.000000e-02 9.900000e-02 1.003000e+00 9.500000e-01", output);
      
      Log.printf(Level.STANDARD, ">>> %t%n",0.234125);
      output = os.toString();
      os.reset();
      assertEquals(String.format(">>> %e%n",0.234125), output);
   }
   
   public void testPrintfWithVerboseTimes() {
      // redirect System.out to an other stream
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      PrintStream ps = new PrintStream(os);
      System.setOut(ps);

      Log.setLogLevel(Level.STANDARD);
      String output;
      
      Log.setTimingActive(false);
      Log.printf(Level.STANDARD, ">>> %3$5d %2$.2e %6$20t %1$10s %7$12t %4$10d %8$6t %5$d %9$e", "A String", 0.23412, 98, 1234, Long.MIN_VALUE, 0.02, 0.099, 1.003, 0.95);
      output = os.toString();
      os.reset();
      assertEquals(">>>    98 2.34e-01                  NaN   A String          NaN       1234    NaN -9223372036854775808 9.500000e-01", output);
      Log.setTimingActive(true);
      Log.printf(Level.STANDARD, ">>> %3$5d %2$.2e %6$20t %1$10s %7$12t %4$10d %8$6t %5$d %9$e", "A String", 0.23412, 98, 1234, Long.MIN_VALUE, 0.02, 0.099, 1.003, 0.95);
      output = os.toString();
      os.reset();
      assertEquals(">>>    98 2.34e-01         2.000000e-02   A String 9.900000e-02       1234 1.003000e+00 -9223372036854775808 9.500000e-01", output);
   }
   
   public void testFormat() {
      Log.setTimingActive(false);
      String s = Log.format("Test %s %e %d %f", "String", 0.01, 10, 10.01);
      assertEquals("Test String 1.000000e-02 10 10.010000", s);
      Log.setTimingActive(true);
      s = Log.format("Test %s %e %d %f", "String", 0.01, 10, 10.01);
      assertEquals("Test String 1.000000e-02 10 10.010000", s);
   }
   
   public void testFormatVerbose() {
      Log.setTimingActive(false);
      String s = Log.format("Test %2$15.3e %1$10s %4$2f %3$5d", "String", 0.0142319, 10, 10.0142319);
      assertEquals("Test       1.423e-02     String 10.014232    10", s);
      Log.setTimingActive(true);
      s = Log.format("Test %2$15.3e %1$10s %4$2f %3$5d", "String", 0.0142319, 10, 10.0142319);
      assertEquals("Test       1.423e-02     String 10.014232    10", s);
   }
   
   public void testFormatWithTimes() {      
      Log.setTimingActive(false);
      String s = Log.format(">>> %s %e %d %d %d %t %t %t %e", "A String", 0.23412, 98, 1234, Long.MIN_VALUE, 0.02, 0.099, 1.003, 0.95);
      assertEquals(">>> A String 2.341200e-01 98 1234 -9223372036854775808 NaN NaN NaN 9.500000e-01", s);
      
      s = Log.format(">>> %t%n",0.234125);
      assertEquals(String.format(">>> %s%n","NaN"), s);
      
      Log.setTimingActive(true);
      s = Log.format(">>> %s %e %d %d %d %t %t %t %e", "A String", 0.23412, 98, 1234, Long.MIN_VALUE, 0.02, 0.099, 1.003, 0.95);
      assertEquals(">>> A String 2.341200e-01 98 1234 -9223372036854775808 2.000000e-02 9.900000e-02 1.003000e+00 9.500000e-01", s);
      
      s = Log.format(">>> %t%n",0.234125);
      assertEquals(String.format(">>> %e%n",0.234125), s);
   }
   
   public void testFormatwithVerboseTimes() {
      Log.setTimingActive(false);
      String s = Log.format(">>> %3$5d %2$.2e %6$20t %1$10s %7$12t %4$10d %8$6t %5$d %9$e", "A String", 0.23412, 98, 1234, Long.MIN_VALUE, 0.02, 0.099, 1.003, 0.95);
      assertEquals(">>>    98 2.34e-01                  NaN   A String          NaN       1234    NaN -9223372036854775808 9.500000e-01", s);
      Log.setTimingActive(true);
      s = Log.format(">>> %3$5d %2$.2e %6$20t %1$10s %7$12t %4$10d %8$6t %5$d %9$e", "A String", 0.23412, 98, 1234, Long.MIN_VALUE, 0.02, 0.099, 1.003, 0.95);
      assertEquals(">>>    98 2.34e-01         2.000000e-02   A String 9.900000e-02       1234 1.003000e+00 -9223372036854775808 9.500000e-01", s);
   }

}
