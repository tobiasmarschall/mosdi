package mosdi.tests;

import junit.framework.TestCase;
import mosdi.paa.apps.MassOccurrenceDAA;
import mosdi.util.Alphabet;

public class MassOccurrenceDAATest extends TestCase {

	private boolean hasMass(String protein, double minMass, double maxMass) {
		MassOccurrenceDAA daa = new MassOccurrenceDAA(0.01, minMass, maxMass);
		int value = daa.computeValue(Alphabet.getAminoAcidAlphabet().buildIndexArray(protein));
		return daa.massFound(value);
	}
	
	public void testDAA() {
		String protein = "RHAPNELGNDIEFCHCGNGILRVVVVCYNGTQCATKCPEMDMRRILEQLPPSPIGGEKEWNMYKPGHDVSKILNQKAQPDGYTVLANLHWIPVCGGFRQRLMQKKWGHAARPNVRKHHATNWAGEIVCHVDLCHAYTEPWYQFFWTGDQYDV";
		// Fragments:
		// R 156.1875
		// HAPNELGNDIEFCHCGNGILR 2291.5437
		// VVVVCYNGTQCATK 1466.7335
		// CPEMDMR 863.0323
		// R 156.1875
		// ILEQLPPSPIGGEK 1459.7055
		// EWNMYKPGHDVSK 1572.7584
		// ILNQK 596.7274
		// AQPDGYTVLANLHWIPVCGGFR 2396.7515
		// QR 284.3182
		// LMQK 500.6568
		// K 128.1741
		// WGHAARPNVR 1145.2919
		// K 128.1741
		// HHATNWAGEIVCHVDLCHAYTEPWYQFFWTGDQYDV 4322.7136
		assertTrue(hasMass(protein, 1, 1000));
		assertTrue(hasMass(protein, 128.1, 128.2));
		assertFalse(hasMass(protein, 128.3, 156.0));
		assertTrue(hasMass(protein, 156.1, 156.2));
		assertFalse(hasMass(protein, 156.3, 284.2));
		assertTrue(hasMass(protein, 284.3, 284.4));
		assertFalse(hasMass(protein, 284.5, 500.5));
		assertTrue(hasMass(protein, 500.6, 500.7));
		assertFalse(hasMass(protein, 500.8, 596.6));
		assertTrue(hasMass(protein, 596.7, 596.8));
		assertFalse(hasMass(protein, 596.9, 862.9));
		assertTrue(hasMass(protein, 863.0, 863.1));
		assertFalse(hasMass(protein, 863.2, 1145.1));
		assertTrue(hasMass(protein, 1145.2, 1145.3));
		assertFalse(hasMass(protein, 1145.4, 1459.6));
		assertTrue(hasMass(protein, 1459.7, 1459.8));
		assertFalse(hasMass(protein, 1459.9, 1466.6));
		assertTrue(hasMass(protein, 1466.7, 1466.8));
		assertFalse(hasMass(protein, 1466.9, 1572.6));
		assertTrue(hasMass(protein, 1572.7, 1572.8));
		assertFalse(hasMass(protein, 1572.9, 2291.4));
		assertTrue(hasMass(protein, 2291.5, 2291.6));
		assertFalse(hasMass(protein, 2291.7, 2396.6));
		assertTrue(hasMass(protein, 2396.7, 2396.8));
		assertFalse(hasMass(protein, 2396.9, 4322.6));
		assertTrue(hasMass(protein, 4322.7, 4322.8));
		assertFalse(hasMass(protein, 4322.9, 5000.0));
	}
	
}
