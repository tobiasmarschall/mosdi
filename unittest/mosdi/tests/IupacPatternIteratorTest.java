/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import java.util.ArrayList;

import static org.junit.Assert.*;
import junit.framework.TestCase;
import mosdi.util.Alphabet;
import mosdi.util.IupacStringConstraints;
import mosdi.util.iterators.IupacPatternGenerator;
import mosdi.util.iterators.IupacPatternIterator;

public class IupacPatternIteratorTest extends TestCase {

	public void testBounds() {
		int[] minFrequencies = {0,0,0,1};
		int[] maxFrequencies = {4,1,2,1};
		IupacStringConstraints constraints = new IupacStringConstraints(minFrequencies, maxFrequencies);
		IupacPatternIterator it = new IupacPatternIterator(4, constraints);
		String last = "AAAA";
		int n = 0;
		while (it.hasNext()) {
			String s = Alphabet.getIupacAlphabet().buildString(it.next());
			assertTrue(s.compareTo(last)>0);
			assertTrue(constraints.check(s));
			last = s;
			++n;
		}
		assertEquals(6400,n);
	}
	
	public void testSkip() {
		int length = 4; 
		int[] minFrequencies = {0,0,0,1};
		int[] maxFrequencies = {4,1,2,1};
		IupacStringConstraints constraints = new IupacStringConstraints(minFrequencies, maxFrequencies);
		long total = constraints.validStringCount(length);
		assertEquals(6400,total);
		ArrayList<int[]> list = new ArrayList<int[]>((int)total);
		for (int[] s : new IupacPatternGenerator(length, constraints)) {
			list.add(s);
		}
		int[] last = {-1,-1,-1,-1};
		IupacPatternIterator iterator = new IupacPatternIterator(length, constraints);
		int[] skips = {0,1000,100,23,4000,1,2,17,28,200,1018};
		int n = -1;
		for (int skip : skips) {
			iterator.skipByNumber(skip);
			n+=skip;
			int[] s = iterator.next();
			n+=1;
			assertArrayEquals(list.get(n), s);
			int j=0;
			for (;(j<s.length)&&(s[j]==last[j]); ++j);
			assertEquals(j, iterator.getLeftmostChangedPosition());
			last = s;
		}
		assertFalse(iterator.hasNext());
	}
}
