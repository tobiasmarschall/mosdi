package mosdi.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;
import mosdi.fa.CDFA;
import mosdi.fa.DFAFactory;
import mosdi.fa.GeneralizedString;
import mosdi.util.Alphabet;
import mosdi.util.Iupac;
import mosdi.util.iterators.DFALanguageIterator;
import mosdi.util.iterators.LexicographicalIterator;

public class DFALanguageIteratorTest extends TestCase {
	
	public void testIterator() {
		Alphabet dnaAlphabet = Alphabet.getDnaAlphabet();
		List<GeneralizedString> l = new ArrayList<GeneralizedString>();
		l.addAll(Iupac.toGeneralizedStrings("AKNB", false));
		l.addAll(Iupac.toGeneralizedStrings("YYCGGTA", false));
		l.addAll(Iupac.toGeneralizedStrings("TTTT", false));
		l.addAll(Iupac.toGeneralizedStrings("YTTA", false));
		Set<String> expected = new HashSet<String>(Arrays.asList("AGAC","AGAG","AGAT","AGCC",
				// "AGCG","AGCT","AGGC","AGGG","AGGT","AGTC","AGTG","AGTT",
				"ATAC","ATAG","ATAT","ATCC","ATCG","ATCT","ATGC","ATGG","ATGT","ATTC",
				// "ATTG","ATTT",
				"CTTA","TTTA","TTTT"));
		CDFA cdfa = DFAFactory.build(dnaAlphabet, l);
		LexicographicalIterator iterator = new DFALanguageIterator(cdfa,4,dnaAlphabet.size());
		Set<String> actual = new HashSet<String>();
		String last = null;
		while (iterator.hasNext()) {
			String s = dnaAlphabet.buildString(iterator.next());
			if (last==null) assertEquals(0, iterator.getLeftmostChangedPosition());
			else {
				int i=0;
				for (;s.charAt(i)==last.charAt(i); ++i);
				assertEquals(i, iterator.getLeftmostChangedPosition());
			}
			last = s;
			actual.add(s);
			if (s.equals("AGCC")) iterator.skip(1);
			if (s.equals("ATGT")) iterator.skip(2);
			if (s.equals("ATTC")) iterator.skip(2);
		}
		assertTrue(expected.equals(actual));
	}
	
}
