/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.paa.DAA;
import mosdi.paa.apps.IndexSumDAA;
import mosdi.util.Alphabet;

public class IndexSumDAATest extends TestCase {
	
	public void testIndexSum() {
		final String test = "037551662984572309129835";
		Alphabet alphabet = new Alphabet(test);
		DAA daa = new IndexSumDAA(alphabet.size(), 150);
		assertEquals(110, daa.computeValue(alphabet.buildIndexArray(test)));
	}
	
}
