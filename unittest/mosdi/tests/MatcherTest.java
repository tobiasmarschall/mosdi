/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.matching.BNDMatcher;
import mosdi.matching.BackwardOracleMatcher;
import mosdi.matching.HorspoolMatcher;
import mosdi.matching.Matcher;
import mosdi.util.Alphabet;
import static org.junit.Assert.assertArrayEquals;

public class MatcherTest extends TestCase {

	static final Alphabet dnaAlphabet = Alphabet.getDnaAlphabet();
	static final int[] text0 = dnaAlphabet.buildIndexArray("AGCTCGTTGATCTTCGCTCGTGAACTCTCAGGTATCAAGAACTCTCCGTGAAGCATGGCAAGCGATAGTTGGGTAACCCATCAGTGGGGGAATACTAGTGACCCCGACTGCGCCGAGGGTTAAAACCCGACTGCTGAGTATAGAGATTGCATGGCTTTCAGGCACCACACTACAAGGTGGCCAACGTGTACTCATCTGTGCATTAAAAATGAAGGGCATCCGCTGAGGCCACGGTTCGTTAATTGTATCATCTCAAGCAAGCAACGGGACACGCGCGTCATTCTTATTGTACTCAACGTCCTTAATCCATCGACCCATGGCTTGGAGATGCTCGACCCTAGTGTAATTCAAGGCCGGCGGTGTTGCCTAATCAAGTACCGAGTATAAAGGGCACCACCTGCTCGAGGCCACTTAAAATAGCCCGTCGGATGAGTAAGAAGCGAGGCGCGCACGCTATGCCCAACTAGAGGGTTGAGAGGATGTTAGTTTTCCTAAGCTCGTAATATTACCACCTGCTTTTTCGAGATTGTTATATCTGCGAAGGTCCTTCCGATAGGGCCTATATGATCCGCAAGTGTTCACGCCGGGACCCTTGCTTGCGCAAATACGCCTTCTTCCCTTACGCTACTCTCGGAGGTACGAGATGGCCCTGTCTCTATACTCGACGAGCCAAATTTTTGATATCAGGAAGATTTAGTATCGGAGGATGCGGTTCCGCTGTAGCGTCATGCTGACACAGTATTCTGCTTGATCGGTTTGCCACTCTGAGCACTGAATTAATATTTTCAGCCGCGACGGACGATCGTCAAAGCTTACACTAATTTCCCTAGTCTACTTCTGCCCGGCCGAACGATTGGCCTGCACTTCGGTCAGGTGGAGTGTCATGCTTCAATTTACTATTGCACCTCTATATTGATCCGTCCTCCAACCTAACCAAACAGGTCCTAGGTGCTTAATTGAGGTGCCATCATGAGCGATTTTTTCGCCTAAGTAGGTGACTTTCTGA");
	static final int[] text1 = dnaAlphabet.buildIndexArray("GACCAGTTCGTTTGAAACAATCGTGAACAACAAACAAAAATCTCGGCCAATCCCGTAGAAGCAGGAATGGGGGTCACGGTCATTGAAGAAGGCACCCTGTGTGCCCAGCGCCGTATTAGGGGACCTATTGCTGCAAAGGAACTAACACGTAAATCCGCGAAGCGGAACCTAGTGTTCTCTCAGGCGTTGCAAAGATGGCCACGCCAAGAAATCGGAGTATCCTCGTCAATCAGCCTTGATACGAGAGGTACATCTCGTCCGCTGCTAGATTTCCGCATTAAATGGTGATGAATGCCTATACTCTTGCACAACGGGGAAAACTGCCTGTCAGACCTTTTAACTGGAATGAGACAGGGCTCGCTCGCTGGATCGCTGGATTATCCAACTGGTATCACAGGTTCTTATTCCTGTGCCTAGAAGTTACCGGACTGGCTATAGACTAGTATTCACCCGCATGATTATGATTAGGGTGGCCCGAAGTACCGACCTCACTGAGAACTGTCAATCTGCTTTGGCCTGGTCGGGATTGCGTCAAAGTGCTTTTACGGTTAATCGTGAATTTAGTAAGAGATCAAAGAGCGGGGTTTCGGTGTCACTCTTTGTATACATTTCCCGTGACAGTATTACGGCCTGTGATAACAACTGCGTATTTATAATCTGAAAACCCTACGACTGCTACGAAAGAACCGCAAATCGGGGCCACACACGAAGCGGGATGTCTGTCGACAAGATGTGCTCTGCAGACTCTTTCGTCCGAATTTCCTTTGGATTCCGATCTGGTGGTGGCGGCTACACTCTAGAGCCCTGTTCTCCCCTCGGCGACCAATATTACGAAGAACGAAACTGTGCTTTGGAGAAGTGACTTGCAAGACAAGTCACAGCACTTTATAGCCCGACGGATGCCAAGTCCGGTCATTGGCGGCTCGGCTTAACAAACAATCCGCGTAACACAACAACAAGCTAACCTACGACCTCAATATGTTCTGGACAAGGCCCGATGACTTCCTACCCGCCCCCAGGGTCGTCAAAA");
	static final int[] text2 = dnaAlphabet.buildIndexArray("ACTTGGATGGGGGCCCACGGGGCCGGTGTGCTGTTGAGCCAGGACTGTGCTGGCACCCCACAGGGAGCCTTGGAGCCCTGCGTCCAGGAGGCCACTGCACTGCTCACTTGTGGCCCAGCGCGTCCTTGGAAATCTGTGGCCCTGGAGGAGGAACAGGAGGGCCCTGGGACCAGGCTCCCGGGGAACCTGAGCTCAGAGGATGTGCTGCCAGCAGGGTGTACGGAGTGGAGGGTACAGACGCTTGCCTATCTGCCACAGGAGGACTGGGCCCCCACGTCCCTGACTAGGCCGGCTCCCCCAGACTCAGAGGGCAGCAGGAGCAGCAGCAGCAGCAGCAGCAGCAACAACAACAACTACTGTGCCTTGGGCTGCTATGGGGGATGGCACCTCTCAGCCCTCCCAGGAAACACACAGAGCTCTGGGCCCATCCCAGCCCTGGCCTGTGGCCTTTCTTGTGACCATCAGGGCCTGGAGACCCAGCAAGGAGTTGCCTGGGTGCTGGCTGGTCACTGCCAGAGGCCTGGGCTGCATGAGGACCTCCAGGGCATGTTGCTCCCTTCTGTCCTCAGCAAGGCTCGGTCCTGGACATTCTAG");
	static final int[] pattern0 = dnaAlphabet.buildIndexArray("GCTCG");
	static final int[] pattern1 = dnaAlphabet.buildIndexArray("AACAA");
	static final int[] pattern2 = dnaAlphabet.buildIndexArray("CG");
	
	public static void assertMatcher0(Matcher matcher) {
		assertEquals(5, matcher.findMatches(text0));
		assertEquals(25, matcher.coveredPositions(text0));
		assertEquals(matcher.getCharacterAccesses(), matcher.costScheme().computeCost(text0));
		assertEquals(3, matcher.findMatches(text1));
		assertEquals(14, matcher.coveredPositions(text1));
		assertEquals(matcher.getCharacterAccesses(), matcher.costScheme().computeCost(text1));
		int[] matches0 = {1, 15, 329, 399, 495};
		assertArrayEquals(matches0, matcher.findAllMatchPositions(text0));
		int[] matches1 = {355, 359, 921};
		assertArrayEquals(matches1, matcher.findAllMatchPositions(text1));
	}
	
	public static void assertMatcher1(Matcher matcher) {
		assertEquals(0, matcher.findMatches(text0));
		assertEquals(0, matcher.coveredPositions(text0));
		assertEquals(matcher.getCharacterAccesses(), matcher.costScheme().computeCost(text0));
		assertEquals(9, matcher.findMatches(text1));
		assertEquals(39, matcher.coveredPositions(text1));
		assertEquals(matcher.getCharacterAccesses(), matcher.costScheme().computeCost(text1));
		int[] matches0 = {};
		assertArrayEquals(matches0, matcher.findAllMatchPositions(text0));
		int[] matches1 = {15, 25, 28, 32, 637, 930, 934, 951, 954};
		assertArrayEquals(matches1, matcher.findAllMatchPositions(text1));
	}

	public static void assertMatcher2(Matcher matcher) {
		int[] matches2 = {17, 23, 80, 118, 120, 178, 220, 238, 274, 289, 576};
		assertArrayEquals(matches2, matcher.findAllMatchPositions(text2));
	}

	public void testHorspool() {
		assertMatcher0(new HorspoolMatcher(dnaAlphabet.size(), pattern0));
		assertMatcher1(new HorspoolMatcher(dnaAlphabet.size(), pattern1));
		assertMatcher2(new HorspoolMatcher(dnaAlphabet.size(), pattern2));
	}
	
	public void testBNDM() {
		Matcher matcher = new BNDMatcher(dnaAlphabet.size(), pattern0); 
		assertMatcher0(matcher);
		// True value was determined using Sven's python implementation
		assertEquals(475, matcher.costScheme().computeCost(text0));
		assertMatcher1(new BNDMatcher(dnaAlphabet.size(), pattern1));
		assertMatcher2(new BNDMatcher(dnaAlphabet.size(), pattern2));
	}

	public void testBOM() {
		assertMatcher0(new BackwardOracleMatcher(dnaAlphabet.size(), pattern0)); 
		assertMatcher1(new BackwardOracleMatcher(dnaAlphabet.size(), pattern1));
		assertMatcher2(new BackwardOracleMatcher(dnaAlphabet.size(), pattern2));
	}

}
