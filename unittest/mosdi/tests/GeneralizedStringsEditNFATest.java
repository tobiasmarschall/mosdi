/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import mosdi.fa.GeneralizedString;
import mosdi.fa.GeneralizedStringsEditNFA;
import mosdi.fa.GeneralizedStringsNFA;
import mosdi.fa.NFA;
import mosdi.util.Alphabet;
import mosdi.util.BitArray;
import mosdi.util.Iupac;

public class GeneralizedStringsEditNFATest extends TestCase {

	private static final String text = "AATTAACGGGTCACCCCACCTCTCGGATGTAATTATATGCCAACCTCAGCATCTTACGTCAGTGTACTCCGTGGAGAGGCATATGACGCCGCTAACAAACCACTTGTAAGGTCCAGGAACCATATATTCACGAGGCATTTGGCCAATGTTCGAAGACTAAATGACGAGATACTCCCGCATCAGAATTCTGTGTTACCTGTTGCTGAAGACTTTGAAAGGACTGAGGATGGATTGTATCATAGCCACGTGTAGTCTCTTTCGGGAGCTGCAAGCCGTGCGTTAACGATGACTTCATCCACAGCCGTTGCGCTAATCGGAATGCCAAAGCTTCTGGGGAATTTTCCACGTTACCGCTGGAGGTGTCAAAATGCAGGTGTACCTTTGGTCGTTGTGGAACAAGCCGACATGAGGCGGAAATCCACTATTCTCTGTATTCGTTGCAGACAAAACCTCCTCCAGTGGTGCTGCCACAAAGAAGCAGAAGTTTGTCCTTCAAAACTGCAACTACAATACTTCGATTGTTGGGACAGGAGCATTGCACTGCTCGTCCTGACCAGAAACACCGGCGTCTATGGTGCTTGAACCCCGGTTAGGACGGTATGAGCATTTGATTAATGTGGTGAACTTGTCTTCCTCCCGGGAGACTCTGGTGAAGATCCACAGTAAGGTAAAACAAACTCGAGAAGCGCATTCGTATCCCATTGCTAATACAATTCGGTCTGGACGTCCCTGTGATGATGAAAAGTTCTCGGGATAAGGTCCCACACCTTACACTTGTGAATGGCATATATGACTAAAGCAGTCCATGTTCCGCCTCTAGGTCCTGGGTACTCACGCTAGTCGACGACGGCATGCCTGCTACCCCCAAAGTTTGTGCCATAGGCAGAACTCTTGGCCATGCATATGCGTTCAGCACTACGTCTATAATGCGGTTTTAAGTATGGTCTGTACTTTACACGGTATGCACTGGTCTGTACCACTGCACGAACACGTCCCGATG";
	
	public void test1() {
		Alphabet alphabet = Alphabet.getDnaAlphabet();
		List<GeneralizedString> l1 = new ArrayList<GeneralizedString>();
		l1.add(Iupac.toGeneralizedString("ARC"));
		NFA nfa1 = new GeneralizedStringsEditNFA(l1,1);
		List<GeneralizedString> l2 = new ArrayList<GeneralizedString>();
		// 0 errors
		l2.add(Iupac.toGeneralizedString("ARC"));
		// 1 mismatch
		l2.add(Iupac.toGeneralizedString("BRC"));
		l2.add(Iupac.toGeneralizedString("AYC"));
		l2.add(Iupac.toGeneralizedString("ARD"));
		// 1 insertion
		l2.add(Iupac.toGeneralizedString("NARC"));
		l2.add(Iupac.toGeneralizedString("ANRC"));
		l2.add(Iupac.toGeneralizedString("ARNC"));
		l2.add(Iupac.toGeneralizedString("ARCN"));
		// 1 deletion
		l2.add(Iupac.toGeneralizedString("AR"));
		l2.add(Iupac.toGeneralizedString("AC"));
		l2.add(Iupac.toGeneralizedString("RC"));
		NFA nfa2 = new GeneralizedStringsNFA(l2);
		BitArray state1 = nfa1.startStates();
		BitArray state2 = nfa2.startStates();
		for (int c : alphabet.buildIndexArray(text)) {
			state1 = nfa1.transition(state1, c);
			state2 = nfa2.transition(state2, c);
			boolean accept1 = !state1.allZeroAfterAnd(nfa1.acceptStates());
			boolean accept2 = !state2.allZeroAfterAnd(nfa2.acceptStates());
			assertEquals(accept2, accept1);
		}
	}
	
}
