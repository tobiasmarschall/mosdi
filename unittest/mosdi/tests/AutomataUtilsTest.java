/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.fa.AutomataUtils;
import mosdi.fa.CharacterAutomaton;
import mosdi.util.BitArray;

public class AutomataUtilsTest extends TestCase {

	private static class TestAutomaton implements CharacterAutomaton {
		@Override
		public int getAlphabetSize() { return 2; }
		@Override
		public int getStartState() { return 1; }
		@Override
		public int getStateCount() { return 5; }
		@Override
		public int getTransitionTarget(int state, int character) {
			switch (state) {
			case 0: return 1;
			case 1: return (character==0)?0:3;
			case 2: return 4;
			case 3: return 1;
			case 4: return 2;
			}
			throw new IllegalStateException();
		}
		
	}
	
	public void testReachability() { 
		CharacterAutomaton automaton = new TestAutomaton();
		BitArray reachability = AutomataUtils.findReachableStates(automaton);
		BitArray expected = new BitArray("01011");
		assertTrue(expected.equals(reachability));
	}
	
}
