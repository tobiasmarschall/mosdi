/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import static org.junit.Assert.assertArrayEquals;
import junit.framework.TestCase;
import mosdi.index.InsideOutReordering;

public class InsideOutReorderingTest extends TestCase {
	
	public void testReordering() {
		int[] pattern1 = {0,1,2,3,4,5,6,7};
		int[] reorderedPattern1 = {6,4,2,0,1,3,5,7};
		int[] revReorderedPattern1 = {7,5,3,1,0,2,4,6};
		int[] pattern2 = {0,1,2,3,4,5,6};
		int[] reorderedPattern2 = {6,4,2,0,1,3,5};
		int[] revReorderedPattern2 = {5,3,1,0,2,4,6};
		InsideOutReordering reordering1 = new InsideOutReordering(pattern1, false);
		assertArrayEquals(reorderedPattern1, reordering1.reorderedString());
		InsideOutReordering reordering2 = new InsideOutReordering(pattern2, false);
		assertArrayEquals(reorderedPattern2, reordering2.reorderedString());
		InsideOutReordering revReordering1 = new InsideOutReordering(pattern1, true);
		assertArrayEquals(revReorderedPattern1, revReordering1.reorderedString());
		InsideOutReordering revReordering2 = new InsideOutReordering(pattern2, true);
		assertArrayEquals(revReorderedPattern2, revReordering2.reorderedString());
		
		assertFalse(reordering1.appendedLeft(1));
		assertTrue(reordering1.appendedLeft(2));
		assertFalse(reordering1.appendedLeft(3));
		assertTrue(reordering1.appendedLeft(4));
		assertFalse(reordering1.appendedLeft(5));
		assertTrue(reordering1.appendedLeft(6));
		assertFalse(reordering1.appendedLeft(7));

		assertTrue(revReordering1.appendedLeft(1));
		assertFalse(revReordering1.appendedLeft(2));
		assertTrue(revReordering1.appendedLeft(3));
		assertFalse(revReordering1.appendedLeft(4));
		assertTrue(revReordering1.appendedLeft(5));
		assertFalse(revReordering1.appendedLeft(6));
		assertTrue(revReordering1.appendedLeft(7));

		assertFalse(reordering2.appendedLeft(1));
		assertTrue(reordering2.appendedLeft(2));
		assertFalse(reordering2.appendedLeft(3));
		assertTrue(reordering2.appendedLeft(4));
		assertFalse(reordering2.appendedLeft(5));
		assertTrue(reordering2.appendedLeft(6));

		assertTrue(revReordering2.appendedLeft(1));
		assertFalse(revReordering2.appendedLeft(2));
		assertTrue(revReordering2.appendedLeft(3));
		assertFalse(revReordering2.appendedLeft(4));
		assertTrue(revReordering2.appendedLeft(5));
		assertFalse(revReordering2.appendedLeft(6));
	}

}
