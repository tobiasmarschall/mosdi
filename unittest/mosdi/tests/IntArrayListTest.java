package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.IntArrayList;

//import static org.junit.Assert.assertArrayEquals;

public class IntArrayListTest extends TestCase {
	
	private final static int[] numbers = {7682, -6720, 2966, 7106, 8672, 4920, 4481, 2208, 4025, -694, -1015, -6061, -7917, 1517, 5822, 5778, -6766, -3844, 9849, -4735, -4135, -2116, -6428, -3322, 9329, 2485, 2034, -2310, 5445, 2093, 4554, -2865, -909, -1489, 2097, 1175, 5002, 8940, 5036, 9264, -7078, 3506, 3480, 5354, 9893, -968, 3692, -3623, 9749, 1414, 6099, -8335, -7192, -7479, -371, 424, 6594, -4027, 5141, -465, 8972, 9661, 7661, 9142, 5180, 8564, -5906, -1120, 7038, -7774, 3291, -2499, -3532, 7014, 8537, -4494, 5983, 2038, 3554, 6934, 1265, 3803, 5035, 7229, 9448, 4454, -7714, -692, -6081, 8122, 4153, -8731, -5902, -8362, 9188, 4834, 4254, -8031, 6387, -6956};
	
	public static void assertArray(IntArrayList a, int size) {
		assertEquals(size, a.size());
		int[] b = a.toIntArray();
		assertEquals(size, b.length);
		int i = 0;
		for (int n : a) {
			assertEquals(numbers[i], n);
			assertEquals(numbers[i], b[i]);
			assertEquals(numbers[i], a.get(i));
			i += 1;
		}
		for (i=0; i<numbers.length; ++i) {
			assertEquals(i<size, a.contains(numbers[i]));
		}
	}
	
	public void testList() {
		IntArrayList a = new IntArrayList();
		assertTrue(a.isEmpty());
		assertFalse(a.iterator().hasNext());
		int i=0;
		for (int n : numbers) {
			a.add(n);
			assertFalse(a.isEmpty());
			assertTrue(a.iterator().hasNext());
			assertArray(a,++i);
		}
	}

}
