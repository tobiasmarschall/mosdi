/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.ComposedSequence;

public class ComposedSequenceTest extends TestCase {
	
	public static void assertEquals(CharSequence expected, CharSequence actual) {
		assertEquals(expected.length(), actual.length());
		for (int i=0; i<expected.length(); ++i) {
			assertEquals(expected.charAt(i), actual.charAt(i));
		}
	}
	
	public void testComposedSequence() {
		ComposedSequence cs = new ComposedSequence();
		cs.append("ACACAG");
		cs.append("TAT");
		cs.append("GAGT");
		assertEquals(13, cs.length());
		assertEquals('A', cs.charAt(0));
		assertEquals('C', cs.charAt(1));
		assertEquals('A', cs.charAt(2));
		assertEquals('C', cs.charAt(3));
		assertEquals('A', cs.charAt(4));
		assertEquals('G', cs.charAt(5));
		assertEquals('T', cs.charAt(6));
		assertEquals('A', cs.charAt(7));
		assertEquals('T', cs.charAt(8));
		assertEquals('G', cs.charAt(9));
		assertEquals('A', cs.charAt(10));
		assertEquals('G', cs.charAt(11));
		assertEquals('T', cs.charAt(12));
		assertEquals("AC", cs.subSequence(0, 2));
		assertEquals("AGT", cs.subSequence(4, 7));
		assertEquals("GTATGA", cs.subSequence(5, 11));
		assertEquals("GT", cs.subSequence(4, 7).subSequence(1, 3));
	}
	
}
