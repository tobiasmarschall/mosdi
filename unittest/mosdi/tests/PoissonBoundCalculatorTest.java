/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.discovery.PoissonBoundCalculator;
import mosdi.distributions.PoissonDistribution;

public class PoissonBoundCalculatorTest extends TestCase {

	public void testBounds() {
		PoissonBoundCalculator[] pbcs = {
				new PoissonBoundCalculator(0.1, 10, 100, 10, false),
				new PoissonBoundCalculator(0.1, 10, 100, 1000, false),
				new PoissonBoundCalculator(0.1, 10, 100, 100, true)
		};
		for (double expectation=0.1; expectation<=10; expectation+=0.05) {
			PoissonDistribution pd = new PoissonDistribution(expectation);
			double[] ccdf = pd.getCCDF(101);
			int[] x = {0, 3, 17, 48, 100};
			for (int k : x) {
				for (PoissonBoundCalculator pbc : pbcs) {
					assertTrue(pbc.getLowerBound(expectation, k) <= ccdf[k] * (1.0+1e-10));
					assertTrue(ccdf[k] <= pbc.getUpperBound(expectation, k) * (1.0+1e-10));
				}
			}
		}
	}

}
