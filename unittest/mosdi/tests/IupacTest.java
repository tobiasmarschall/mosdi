/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.Alphabet;
import mosdi.util.Iupac;
import mosdi.util.IupacStringConstraints;
import mosdi.util.iterators.IupacPatternIterator;
import static org.junit.Assert.assertArrayEquals;

public class IupacTest extends TestCase {
	
	public void testReverse() {
		String s = "ABCDGHKMNRSTVWY";
		String complement = "RWBASYNKMDCHGVT";
		assertEquals(complement, Iupac.reverseComplementary(s));
		Alphabet a = Alphabet.getIupacAlphabet();
		assertEquals(complement, a.buildString(Iupac.reverseComplementary(a.buildIndexArray(s))));
	}
	
	public void testDnaReverse() {
		Alphabet a = Alphabet.getDnaAlphabet();
		String s = "ACCGTAAC";
		String complement = "GTTACGGT";
		assertEquals(complement, a.buildString(Iupac.dnaReverseComplementary(a.buildIndexArray(s))));
	}

	public static void assertRank(int[] pattern, IupacStringConstraints constraints) {
		IupacPatternIterator iterator = new IupacPatternIterator(pattern.length, constraints);
		iterator.skipByNumber(Iupac.rank(pattern, constraints));
		assertArrayEquals(iterator.next(), pattern);
	}
	
	public void testRank() {
		Alphabet iupacAlphabet = Alphabet.getIupacAlphabet();
		int[] minFrequencies = {1,2,0,0};
		int[] maxFrequencies = {10,7,2,6};
		IupacStringConstraints constraints = new IupacStringConstraints(minFrequencies, maxFrequencies);
		assertRank(iupacAlphabet.buildIndexArray("AACYYNTBNNAN"), constraints);
		assertRank(iupacAlphabet.buildIndexArray("AAAAAAAAAAKK"), constraints);
		assertRank(iupacAlphabet.buildIndexArray("AAAAAAAAAAKM"), constraints);
		assertRank(iupacAlphabet.buildIndexArray("AAAAAAAAAAMK"), constraints);
		assertRank(iupacAlphabet.buildIndexArray("YYYYYYYVVTTT"), constraints);
		assertRank(iupacAlphabet.buildIndexArray("YVYYYTYYYVTT"), constraints);
		assertRank(iupacAlphabet.buildIndexArray("GGGNNNYNNRBV"), constraints);
		assertRank(iupacAlphabet.buildIndexArray("NWAACNNGNRBV"), constraints);
	}

	public void testLargest() {
		Alphabet iupacAlphabet = Alphabet.getIupacAlphabet();
		int[] minFrequencies = {1,2,0,0};
		int[] maxFrequencies = {10,7,2,6};
		IupacStringConstraints constraints = new IupacStringConstraints(minFrequencies, maxFrequencies);
		int[] p = Iupac.largest(12, constraints);
		int[] e = iupacAlphabet.buildIndexArray("YYYYYYYVVTTT");
		assertArrayEquals(e, p);

		int[] bound = iupacAlphabet.buildIndexArray("NWAACNNGNRBV");
		p = Iupac.largest(12, constraints, bound);
		e = iupacAlphabet.buildIndexArray("NWAACNNGNRBT");
		assertArrayEquals(e, p);

		bound = iupacAlphabet.buildIndexArray("AAAAAAAAAAAA");
		p = Iupac.largest(12, constraints, bound);
		assertEquals(null, p);
		
		bound = iupacAlphabet.buildIndexArray("GGGGGGGGGGGG");
		p = Iupac.largest(12, constraints, bound);
		e = iupacAlphabet.buildIndexArray("GGGGGGGGGDYY");
		assertArrayEquals(e, p);

		bound = iupacAlphabet.buildIndexArray("MMKKWWSN");
		p = Iupac.largest(12, constraints, bound);
		e = iupacAlphabet.buildIndexArray("MMKKWWSHVTTT");
		assertArrayEquals(e, p);
		
		bound = iupacAlphabet.buildIndexArray("MMKKWWSN");
		p = Iupac.smallest(12, constraints, bound);
		e = iupacAlphabet.buildIndexArray("MMKKWWSNAAAA");
		assertArrayEquals(e, p);
		
		p = Iupac.smallest(12, constraints);
		e = iupacAlphabet.buildIndexArray("AAAAAAAAAAKK");
		assertArrayEquals(e, p);
	}

}
