/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import java.util.Arrays;

import junit.framework.TestCase;
import mosdi.fa.SuffixAutomaton;
import mosdi.util.Alphabet;

public class SuffixAutomatonTest extends TestCase {

	public void testSuffixAutomaton() {
		Alphabet alphabet = Alphabet.getDnaAlphabet();
		int[] text = alphabet.buildIndexArray("TACACATGTGCTTCAAAACT");
		SuffixAutomaton sa = new SuffixAutomaton(alphabet.size(), text);
		for (int i=0; i<=text.length; ++i) {
			int[] query = Arrays.copyOfRange(text, text.length-i, text.length);
			assertTrue(sa.isFactor(query));
			assertTrue(sa.isSuffix(query));
		}
		for (int i=0; i<text.length; ++i) {
			for (int j=i+1; j<=text.length; ++j) {
				int[] query = Arrays.copyOfRange(text, i, j);
				assertTrue(sa.isFactor(query));
			}
		}
		assertFalse(sa.isFactor(alphabet.buildIndexArray("CACAC")));
		assertFalse(sa.isFactor(alphabet.buildIndexArray("GA")));
		assertFalse(sa.isFactor(alphabet.buildIndexArray("TTT")));
		assertFalse(sa.isFactor(alphabet.buildIndexArray("GATTACA")));
		assertFalse(sa.isSuffix(alphabet.buildIndexArray("CCT")));
		assertFalse(sa.isSuffix(alphabet.buildIndexArray("A")));
		assertFalse(sa.isSuffix(alphabet.buildIndexArray("TACACATGTGCTTCAAAAC")));
		assertFalse(sa.isSuffix(alphabet.buildIndexArray("GATTACA")));
	}
	
}
