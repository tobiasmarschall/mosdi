/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.LogSpace;


public class LogSpaceTest extends TestCase {

	public void testLogAdd() {
		double x0=Math.log(0.0);
		double x1=Math.log(1.0);
		double x2=Math.log(70.0);
		assertEquals(Double.NEGATIVE_INFINITY, LogSpace.logAdd(x0, x0));
		assertEquals(Math.log(71.0), LogSpace.logAdd(x1, x2), 1e-12);
		assertEquals(Math.log(71.0), LogSpace.logAdd(x2, x1), 1e-12);
	}

	public void testLogSub() {
		double x0=Math.log(0.0);
		double x1=Math.log(1.0);
		double x2=Math.log(70.0);
		assertEquals(Double.NEGATIVE_INFINITY, LogSpace.logSub(x0, x0));
		assertEquals(Double.NaN, LogSpace.logSub(x1, x2));
		assertEquals(Math.log(69.0), LogSpace.logSub(x2, x1), 1e-12);
	}

	public void testToString() {
		assertEquals("1.000000e+4", LogSpace.toString(Math.log(10000)));
		double d = 123.456;
		assertEquals(d, Double.parseDouble(LogSpace.toString(Math.log(d))),1e-10);
		assertEquals("0.0", LogSpace.toString(Double.NEGATIVE_INFINITY));
		assertEquals("Infinity", LogSpace.toString(Double.POSITIVE_INFINITY));
		assertEquals("NaN", LogSpace.toString(Double.NaN));
	}
	
}
