/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.fa.GeneralizedString;
import mosdi.util.Iupac;

public class GeneralizedStringTest extends TestCase {

	public void testMatches() {
		String iupacString = "RNACGY";
		GeneralizedString gs = Iupac.toGeneralizedString(iupacString);
		assertFalse(gs.matches("AAAAAAAAAA", 0));
		assertTrue(gs.matches("ATACGT", 0));
		assertTrue(gs.matches("CCATACGT", 2));
		assertFalse(gs.matches("CCATACGG", 2));
	}

}
