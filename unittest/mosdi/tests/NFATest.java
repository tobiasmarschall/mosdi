/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import java.util.ArrayList;

import junit.framework.TestCase;
import mosdi.fa.GeneralizedString;
import mosdi.fa.GeneralizedStringsNFA;
import mosdi.fa.NFA;
import mosdi.util.Alphabet;
import mosdi.util.Iupac;

public class NFATest extends TestCase {
	
	private final Alphabet dnaAlphabet = Alphabet.getDnaAlphabet();
	private final String text = "GGTAGGCTCGTGCAAAGGCGGGTTCTTCTCTTCGCAAGATTTAATAGTACTGTTTTACGCTTGTACGACCATAATGAATGTTCAATCTCTATAAGCATTTCAGTGTTAAGTACTGCGTGATCAATGCTGGTTTCCGGGCCTAAATAGGCGCCGACATCCATTCTCTTTGGCCTCGGAGCAATGACCT";

	public void testmanNFAbuild() {
		ArrayList<GeneralizedString> list = new ArrayList<GeneralizedString>(2);
		list.add(Iupac.toGeneralizedString("AKM")); // 10
		list.add(Iupac.toGeneralizedString("MGR")); // 9
		NFA nfa = new GeneralizedStringsNFA(list);
		assertEquals(19, nfa.countMatches(dnaAlphabet.buildIndexArray(text)));
	}

}
