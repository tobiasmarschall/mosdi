package mosdi.tests;

import java.util.Iterator;

import junit.framework.TestCase;
import mosdi.util.Histogram;

public class HistogramTest extends TestCase {

	public void testHistogram() {
		Histogram h = new Histogram(2.0, 7.0, 10);
		h.add(2.0);
		h.add(2.49999999);
		h.add(7.0);
		h.add(6.0001);
		h.add(6.2);
		h.add(6.3);
		h.add(6.4999);
		Iterator<Histogram.Bin> iterator = h.iterator();
		Histogram.Bin b = iterator.next();
		assertEquals(2.00, b.getIntervalStart(), 1e-10);
		assertEquals(2.25, b.getIntervalMiddle(), 1e-10);
		assertEquals(2.50, b.getIntervalEnd(), 1e-10);
		assertEquals(2, b.getCount());
		b = iterator.next(); 
		assertEquals(2.50, b.getIntervalStart(), 1e-10);
		assertEquals(2.75, b.getIntervalMiddle(), 1e-10);
		assertEquals(3.00, b.getIntervalEnd(), 1e-10);
		assertEquals(0, b.getCount());
		b = iterator.next(); 
		assertEquals(3.00, b.getIntervalStart(), 1e-10);
		assertEquals(3.25, b.getIntervalMiddle(), 1e-10);
		assertEquals(3.50, b.getIntervalEnd(), 1e-10);
		assertEquals(0, b.getCount());
		b = iterator.next(); 
		assertEquals(3.50, b.getIntervalStart(), 1e-10);
		assertEquals(3.75, b.getIntervalMiddle(), 1e-10);
		assertEquals(4.00, b.getIntervalEnd(), 1e-10);
		assertEquals(0, b.getCount());
		b = iterator.next(); 
		assertEquals(4.00, b.getIntervalStart(), 1e-10);
		assertEquals(4.25, b.getIntervalMiddle(), 1e-10);
		assertEquals(4.50, b.getIntervalEnd(), 1e-10);
		assertEquals(0, b.getCount());
		b = iterator.next(); 
		assertEquals(4.50, b.getIntervalStart(), 1e-10);
		assertEquals(4.75, b.getIntervalMiddle(), 1e-10);
		assertEquals(5.00, b.getIntervalEnd(), 1e-10);
		assertEquals(0, b.getCount());
		b = iterator.next(); 
		assertEquals(5.00, b.getIntervalStart(), 1e-10);
		assertEquals(5.25, b.getIntervalMiddle(), 1e-10);
		assertEquals(5.50, b.getIntervalEnd(), 1e-10);
		assertEquals(0, b.getCount());
		b = iterator.next(); 
		assertEquals(5.50, b.getIntervalStart(), 1e-10);
		assertEquals(5.75, b.getIntervalMiddle(), 1e-10);
		assertEquals(6.00, b.getIntervalEnd(), 1e-10);
		assertEquals(0, b.getCount());
		b = iterator.next(); 
		assertEquals(6.00, b.getIntervalStart(), 1e-10);
		assertEquals(6.25, b.getIntervalMiddle(), 1e-10);
		assertEquals(6.50, b.getIntervalEnd(), 1e-10);
		assertEquals(4, b.getCount());
		b = iterator.next(); 
		assertEquals(6.50, b.getIntervalStart(), 1e-10);
		assertEquals(6.75, b.getIntervalMiddle(), 1e-10);
		assertEquals(7.00, b.getIntervalEnd(), 1e-10);
		assertEquals(1, b.getCount());
		assertFalse(iterator.hasNext());
	}
	
}
