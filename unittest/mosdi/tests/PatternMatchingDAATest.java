/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.matching.AlgorithmCostScheme;
import mosdi.matching.HorspoolMatcher;
import mosdi.paa.apps.PatternMatchingDAA;
import mosdi.util.Alphabet;

public class PatternMatchingDAATest extends TestCase {
	
	static final String text = "AGCTCGTTGATCTTCGCTCGTGAACTCTCAGGTATCAAGAACTCTCCGTGAAGCATGGCAAGCGATAGTTGGGTAACCCATCAGTGGGGGAATACTAGTGACCCCGACTGCGCCGAGGGTTAAAACCCGACTGCTGAGTATAGAGATTGCATGGCTTTCAGGCACCACACTACAAGGTGGCCAACGTGTACTCATCTGTGCATTAAAAATGAAGGGCATCCGCTGAGGCCACGGTTCGTTAATTGTATCATCTCAAGCAAGCAACGGGACACGCGCGTCATTCTTATTGTACTCAACGTCCTTAATCCATCGACCCATGGCTTGGAGATGCTCGACCCTAGTGTAATTCAAGGCCGGCGGTGTTGCCTAATCAAGTACCGAGTATAAAGGGCACCACCTGCTCGAGGCCACTTAAAATAGCCCGTCGGATGAGTAAGAAGCGAGGCGCGCACGCTATGCCCAACTAGAGGGTTGAGAGGATGTTAGTTTTCCTAAGCTCGTAATATTACCACCTGCTTTTTCGAGATTGTTATATCTGCGAAGGTCCTTCCGATAGGGCCTATATGATCCGCAAGTGTTCACGCCGGGACCCTTGCTTGCGCAAATACGCCTTCTTCCCTTACGCTACTCTCGGAGGTACGAGATGGCCCTGTCTCTATACTCGACGAGCCAAATTTTTGATATCAGGAAGATTTAGTATCGGAGGATGCGGTTCCGCTGTAGCGTCATGCTGACACAGTATTCTGCTTGATCGGTTTGCCACTCTGAGCACTGAATTAATATTTTCAGCCGCGACGGACGATCGTCAAAGCTTACACTAATTTCCCTAGTCTACTTCTGCCCGGCCGAACGATTGGCCTGCACTTCGGTCAGGTGGAGTGTCATGCTTCAATTTACTATTGCACCTCTATATTGATCCGTCCTCCAACCTAACCAAACAGGTCCTAGGTGCTTAATTGAGGTGCCATCATGAGCGATTTTTTCGCCTAAGTAGGTGACTTTCTGA";
	
	public void testCostScheme() {
		Alphabet alphabet = Alphabet.getDnaAlphabet();
		String pattern = "GCTCG";
		int[] intText = alphabet.buildIndexArray(text);
		HorspoolMatcher matcher = new HorspoolMatcher(alphabet.size(), alphabet.buildIndexArray(pattern));
		AlgorithmCostScheme costScheme = matcher.costScheme();
		int totalCost = costScheme.computeCost(intText);
		PatternMatchingDAA daa = new PatternMatchingDAA(costScheme, totalCost+20);
		assertEquals(totalCost, daa.computeValue(intText));
	}

}
