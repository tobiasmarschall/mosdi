/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import java.util.List;
import java.util.ListIterator;

import junit.framework.TestCase;
import mosdi.util.SingletonList;

public class SingletonListTest extends TestCase {

	public void testList() {
		List<String> l = new SingletonList<String>("Hallo");
		assertEquals(1, l.size());
		assertEquals("Hallo", l.get(0));
		ListIterator<String> iter = l.listIterator();
		assertTrue(iter.hasNext());
		assertEquals(0, iter.nextIndex());
		assertFalse(iter.hasPrevious());
		assertEquals("Hallo", iter.next());
		assertFalse(iter.hasNext());
		assertTrue(iter.hasPrevious());
		assertEquals(0, iter.previousIndex());
		assertEquals("Hallo", iter.previous());
		assertTrue(iter.hasNext());
		assertEquals(0, iter.nextIndex());
		assertFalse(iter.hasPrevious());
		assertEquals("Hallo", iter.next());
		assertTrue(l.contains("Hallo"));
		assertFalse(l.contains("Holla"));
		assertEquals(0, l.indexOf("Hallo"));
		assertEquals(-1, l.indexOf("Holla"));
		assertFalse(l.isEmpty());
	}
	
}
