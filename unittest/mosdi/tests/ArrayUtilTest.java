/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import mosdi.util.ArrayUtils;
import junit.framework.TestCase;
import static org.junit.Assert.assertArrayEquals;

public class ArrayUtilTest extends TestCase {
	
	public void testEncode() {
		int[] array = {7,3,0,1,6,5};
		int value = ArrayUtils.encode(array, 8); 
		assertEquals(188959, value);
		assertArrayEquals(array, ArrayUtils.decode(value, 8, 6));
	}

}
