/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.index.ScoreIndexCalculator;
import mosdi.index.SuffixTree;
import mosdi.util.Alphabet;

public class ScoreIndexCalculatorTest extends TestCase {
	static final String text = "AGTGTTTTCAATGGTGAGCGGATGGAGCACATGCTGAACACTTTGTCTATTCAGAACGCTGGTGTTGGCCGGCCATAATCCATGTCCGAACAAACAGGGG";
	static final int[] scores = {8, 8, 5, 1, 1, 9, 2, 3, 8, 9, 9, 9, 2, 8, 3, 0, 2, 3, 8, 7, 9, 6, 10, 1, 8, 3, 0, 5, 5, 0, 3, 10, 0, 2, 8, 10, 0, 3, 3, 4, 7, 0, 2, 9, 10, 4, 2, 6, 5, 3, 1, 1, 8, 7, 0, 7, 7, 2, 2, 3, 3, 10, 5, 4, 1, 3, 3, 0, 4, 8, 9, 1, 1, 4, 2, 6, 9, 8, 8, 7, 3, 4, 7, 9, 5, 5, 4, 8, 7, 9, 7, 3, 4, 3, 5, 10, 1, 2, 2, 8,-1};
	
	public void testScoreIndex() {
		Alphabet alphabet = Alphabet.getDnaAlphabet();
		SuffixTree st = new SuffixTree();
		ScoreIndexCalculator sic = new ScoreIndexCalculator(scores, 3);
		st.addNodeConstructionListener(sic);
		st.buildTree(alphabet.buildIndexArray(text,true), alphabet.size());
		int[] scoreMaximumAnnotation = sic.getScoreMaximumAnnotation();
		int[] scoreSumAnnotation = sic.getScoreSumAnnotation();
		assertEquals(21, scoreSumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("AGT"))]);
		assertEquals(21, scoreMaximumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("AGT"))]);
		assertEquals(16, scoreSumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("ACG"))]);
		assertEquals(16, scoreMaximumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("ACG"))]);
		assertEquals(55, scoreSumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("ACA"))]);
		assertEquals(19, scoreMaximumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("ACA"))]);
		assertEquals(31, scoreSumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("GGT"))]);
		assertEquals(18, scoreMaximumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("GGT"))]);
		assertEquals(61, scoreSumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("TGT"))]);
		assertEquals(23, scoreMaximumAnnotation[st.getNodeIndex(alphabet.buildIndexArray("TGT"))]);
	}
}
