package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.Alphabet;
import mosdi.util.CodonAlphabet;

public class CodonAlphabetTest extends TestCase {

	public void testCodons() {
		Alphabet dnaAlphabet = Alphabet.getDnaAlphabet();
		Alphabet aaAlphabet = Alphabet.getAminoAcidAlphabet();
		assertEquals(aaAlphabet.getIndex('K'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("AAA"))));
		assertEquals(aaAlphabet.getIndex('N'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("AAC"))));
		assertEquals(aaAlphabet.getIndex('K'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("AAG"))));
		assertEquals(aaAlphabet.getIndex('N'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("AAT"))));
		assertEquals(aaAlphabet.getIndex('T'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("ACA"))));
		assertEquals(aaAlphabet.getIndex('T'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("ACC"))));
		assertEquals(aaAlphabet.getIndex('T'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("ACG"))));
		assertEquals(aaAlphabet.getIndex('T'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("ACT"))));
		assertEquals(aaAlphabet.getIndex('R'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("AGA"))));
		assertEquals(aaAlphabet.getIndex('S'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("AGC"))));
		assertEquals(aaAlphabet.getIndex('R'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("AGG"))));
		assertEquals(aaAlphabet.getIndex('S'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("AGT"))));
		assertEquals(aaAlphabet.getIndex('I'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("ATA"))));
		assertEquals(aaAlphabet.getIndex('I'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("ATC"))));
		assertEquals(aaAlphabet.getIndex('M'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("ATG"))));
		assertEquals(aaAlphabet.getIndex('I'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("ATT"))));
		assertEquals(aaAlphabet.getIndex('Q'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CAA"))));
		assertEquals(aaAlphabet.getIndex('H'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CAC"))));
		assertEquals(aaAlphabet.getIndex('Q'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CAG"))));
		assertEquals(aaAlphabet.getIndex('H'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CAT"))));
		assertEquals(aaAlphabet.getIndex('P'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CCA"))));
		assertEquals(aaAlphabet.getIndex('P'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CCC"))));
		assertEquals(aaAlphabet.getIndex('P'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CCG"))));
		assertEquals(aaAlphabet.getIndex('P'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CCT"))));
		assertEquals(aaAlphabet.getIndex('R'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CGA"))));
		assertEquals(aaAlphabet.getIndex('R'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CGC"))));
		assertEquals(aaAlphabet.getIndex('R'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CGG"))));
		assertEquals(aaAlphabet.getIndex('R'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CGT"))));
		assertEquals(aaAlphabet.getIndex('L'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CTA"))));
		assertEquals(aaAlphabet.getIndex('L'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CTC"))));
		assertEquals(aaAlphabet.getIndex('L'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CTG"))));
		assertEquals(aaAlphabet.getIndex('L'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("CTT"))));
		assertEquals(aaAlphabet.getIndex('E'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GAA"))));
		assertEquals(aaAlphabet.getIndex('D'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GAC"))));
		assertEquals(aaAlphabet.getIndex('E'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GAG"))));
		assertEquals(aaAlphabet.getIndex('D'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GAT"))));
		assertEquals(aaAlphabet.getIndex('A'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GCA"))));
		assertEquals(aaAlphabet.getIndex('A'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GCC"))));
		assertEquals(aaAlphabet.getIndex('A'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GCG"))));
		assertEquals(aaAlphabet.getIndex('A'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GCT"))));
		assertEquals(aaAlphabet.getIndex('G'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GGA"))));
		assertEquals(aaAlphabet.getIndex('G'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GGC"))));
		assertEquals(aaAlphabet.getIndex('G'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GGG"))));
		assertEquals(aaAlphabet.getIndex('G'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GGT"))));
		assertEquals(aaAlphabet.getIndex('V'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GTA"))));
		assertEquals(aaAlphabet.getIndex('V'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GTC"))));
		assertEquals(aaAlphabet.getIndex('V'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GTG"))));
		assertEquals(aaAlphabet.getIndex('V'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("GTT"))));
		assertEquals(-1, CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TAA"))));
		assertEquals(aaAlphabet.getIndex('Y'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TAC"))));
		assertEquals(-1, CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TAG"))));
		assertEquals(aaAlphabet.getIndex('Y'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TAT"))));
		assertEquals(aaAlphabet.getIndex('S'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TCA"))));
		assertEquals(aaAlphabet.getIndex('S'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TCC"))));
		assertEquals(aaAlphabet.getIndex('S'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TCG"))));
		assertEquals(aaAlphabet.getIndex('S'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TCT"))));
		assertEquals(-1, CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TGA"))));
		assertEquals(aaAlphabet.getIndex('C'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TGC"))));
		assertEquals(aaAlphabet.getIndex('W'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TGG"))));
		assertEquals(aaAlphabet.getIndex('C'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TGT"))));
		assertEquals(aaAlphabet.getIndex('L'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TTA"))));
		assertEquals(aaAlphabet.getIndex('F'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TTC"))));
		assertEquals(aaAlphabet.getIndex('L'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TTG"))));
		assertEquals(aaAlphabet.getIndex('F'), CodonAlphabet.getAminoAcid(CodonAlphabet.getIndex(dnaAlphabet.buildIndexArray("TTT"))));
	}
	
}
