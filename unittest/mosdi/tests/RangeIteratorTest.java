/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.Alphabet;
import mosdi.util.IupacStringConstraints;
import mosdi.util.iterators.IupacPatternIterator;
import mosdi.util.iterators.LexicographicalIterator;
import mosdi.util.iterators.RangeIterator;

public class RangeIteratorTest extends TestCase {
	
	private void assertBounds(String lowerBound, String upperBound, int expectedQuantity) {
		Alphabet alphabet = Alphabet.getIupacAlphabet();
		int[] minFrequencies = {0,0,0,1};
		int[] maxFrequencies = {4,1,2,1};
		IupacStringConstraints constraints = new IupacStringConstraints(minFrequencies, maxFrequencies);
		int[] lowerBoundInt = (lowerBound==null)?null:alphabet.buildIndexArray(lowerBound);
		int[] upperBoundInt = (upperBound==null)?null:alphabet.buildIndexArray(upperBound);
		LexicographicalIterator it = new RangeIterator(new IupacPatternIterator(4, constraints),lowerBoundInt,upperBoundInt);
		String last = "AAAA";
		int n = 0;
		while (it.hasNext()) {
			String s = alphabet.buildString(it.next());
			assertTrue(s.compareTo(last)>0);
			assertTrue(constraints.check(s));
			if (n==0) {
				assertEquals(0, it.getLeftmostChangedPosition());
			} else {
				int j=0;
				for (;(j<s.length())&&(s.charAt(j)==last.charAt(j)); ++j);
				assertEquals(j, it.getLeftmostChangedPosition());
			}
			if (lowerBound!=null) assertTrue(s.compareTo(alphabet.buildString(lowerBoundInt))>=0);
			if (upperBound!=null) assertTrue(s.compareTo(alphabet.buildString(upperBoundInt))<0);
			last = s;
			++n;
		}
		assertEquals(expectedQuantity,n);
	}
	
	public void testRange() {
		assertBounds("CAGA", "TAN", 4196);
		assertBounds("AAAN", "YYYY", 6400);
		assertBounds("NAVG", "NAVG", 0);
		assertBounds("C", "A", 0);
		assertBounds("NASN", "NASN", 0);
		assertBounds(null, "C", 912);
		assertBounds("C", null, 6400-912);
		assertBounds(null, "HMNT", 2476);
		assertBounds("HMNT", null, 6400-2476);
		assertBounds(null, "NNTT", 4160);
		assertBounds("NNTT", null, 6400-4160);
		assertBounds(null, "HMNTA", 2477);
		assertBounds("HMNTA", null, 6400-2477);
	}

	public void testSkip() {
		Alphabet alphabet = Alphabet.getIupacAlphabet();
		IupacStringConstraints constraints = new IupacStringConstraints(4, false);
		int[] lowerBoundInt = alphabet.buildIndexArray("TT");
		int[] upperBoundInt = alphabet.buildIndexArray("YYYY");
		LexicographicalIterator it = new RangeIterator(new IupacPatternIterator(4, constraints),lowerBoundInt,upperBoundInt);
		assertTrue(it.hasNext());
		String s = alphabet.buildString(it.next());
		assertTrue(s.equals("TTAA"));
		it.skip(1);
		assertFalse(it.hasNext());
	}
	
}
