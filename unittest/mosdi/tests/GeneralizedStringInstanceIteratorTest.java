package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.Alphabet;
import mosdi.util.Iupac;
import mosdi.util.iterators.GeneralizedStringInstanceIterator;
import mosdi.util.iterators.LexicographicalIterator;

public class GeneralizedStringInstanceIteratorTest extends TestCase {
	
	public void testIterator() {
		Alphabet dnaAlphabet = Alphabet.getDnaAlphabet();
		Alphabet iupacAlphabet = Alphabet.getIupacAlphabet();
		LexicographicalIterator iterator = new GeneralizedStringInstanceIterator(dnaAlphabet.size(), iupacAlphabet.buildIndexArray("ANRNT"), Iupac.asGeneralizedAlphabet());
		assertEquals("AAAAT", dnaAlphabet.buildString(iterator.next()));
		assertEquals(0, iterator.getLeftmostChangedPosition());
		assertEquals("AAACT", dnaAlphabet.buildString(iterator.next()));
		assertEquals(3, iterator.getLeftmostChangedPosition());
		assertEquals("AAAGT", dnaAlphabet.buildString(iterator.next()));
		assertEquals(3, iterator.getLeftmostChangedPosition());
		assertEquals("AAATT", dnaAlphabet.buildString(iterator.next()));
		assertEquals(3, iterator.getLeftmostChangedPosition());
		assertEquals("AAGAT", dnaAlphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		iterator.skip(2);
		assertEquals("ACAAT", dnaAlphabet.buildString(iterator.next()));
		assertEquals(1, iterator.getLeftmostChangedPosition());
		assertEquals("ACACT", dnaAlphabet.buildString(iterator.next()));
		assertEquals(3, iterator.getLeftmostChangedPosition());
		iterator.skip(1);
		assertEquals("AGAAT", dnaAlphabet.buildString(iterator.next()));
		assertEquals(1, iterator.getLeftmostChangedPosition());
		iterator.skip(0);
		assertFalse(iterator.hasNext());
	}

}
