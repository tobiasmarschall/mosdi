/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.Alphabet;

public class AlphabetTest extends TestCase {

	public void testAlphabet() {
		Alphabet a = new Alphabet("HELLO");
		assertEquals(4, a.size());
		int[] x = a.buildIndexArray("OHH");
		assertEquals(3,x.length);
		assertEquals(3,x[0]);
		assertEquals(1,x[1]);
		assertEquals(1,x[2]);
		x = a.buildIndexArray("HELL", true);
		assertEquals(5,x.length);
		assertEquals(1,x[0]);
		assertEquals(0,x[1]);
		assertEquals(2,x[2]);
		assertEquals(2,x[3]);
		assertEquals(-1,x[4]);
		a.setSeparator('$');
		x = a.buildIndexArray("HO$LEO", true);
		assertEquals(7,x.length);
		assertEquals(1,x[0]);
		assertEquals(3,x[1]);
		assertEquals(-1,x[2]);
		assertEquals(2,x[3]);
		assertEquals(0,x[4]);
		assertEquals(3,x[5]);
		assertEquals(-1,x[6]);
		assertEquals("HO$LEO$", a.buildString(a.buildIndexArray("HO$LEO$")));
	}
	
	public void testCompleteAlphabet() {
		Alphabet a = new Alphabet();
		int[] s = a.buildIndexArray("ABC");
		assertEquals(3,s.length);
		assertEquals(65,s[0]);
		assertEquals(66,s[1]);
		assertEquals(67,s[2]);
		assertEquals("ABC",a.buildString(s));
	}
}
