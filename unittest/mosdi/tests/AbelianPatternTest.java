/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.TestCase;
import mosdi.matching.AbelianPattern;


public class AbelianPatternTest extends TestCase {

	public void testMatch() {
		AbelianPattern ap = new AbelianPattern();
		ap.set('a', 4);
		ap.set('b', 2);
		ap.set('c', 3);
		ap.set('d', 2);
		assertEquals(5, ap.match("daabaabcccddaccabdaaaabdcccba", null));
		ArrayList<Integer> list = new ArrayList<Integer>(); 
		assertEquals(5, ap.match("daabaabcccddaccabdaaaabdcccba", list));
		assertEquals(5, list.size());
		assertEquals(0, (int)list.get(0));
		assertEquals(1, (int)list.get(1));
		assertEquals(2, (int)list.get(2));
		assertEquals(16, (int)list.get(3));
		assertEquals(17, (int)list.get(4));

		// test string based constructor
		ap = new AbelianPattern("a4;b2;c3;d2");
		assertEquals(5, ap.match("daabaabcccddaccabdaaaabdcccba", null));
		
		ap.set('a', 2);
		ap.set('d', 0);
		assertEquals(1, ap.match("daabaabcccddaccabdaaaabdcccba",null));
		assertEquals(1, ap.match("daabaabcccddaccabdaaaabdcccba",list));
		assertEquals(1, list.size());
		assertEquals(3, (int)list.get(0));		
	}

	public void testGenerateAllPatterns() {
		AbelianPattern ap = new AbelianPattern();
		ap.set('a', 2);
		ap.set('b', 2);
		ap.set('c', 1);
		List<String> l = ap.generateAllPatterns();
		Collections.sort(l);
		
		assertEquals(30, l.size());
		
		assertEquals("aabbc", l.get(0));
		assertEquals("aabcb", l.get(1));
		assertEquals("aacbb", l.get(2));
		assertEquals("ababc", l.get(3));
		assertEquals("abacb", l.get(4));
		assertEquals("abbac", l.get(5));
		assertEquals("abbca", l.get(6));
		assertEquals("abcab", l.get(7));
		assertEquals("abcba", l.get(8));
		assertEquals("acabb", l.get(9));
		assertEquals("acbab", l.get(10));
		assertEquals("acbba", l.get(11));
		assertEquals("baabc", l.get(12));
		assertEquals("baacb", l.get(13));
		assertEquals("babac", l.get(14));
		assertEquals("babca", l.get(15));
		assertEquals("bacab", l.get(16));
		assertEquals("bacba", l.get(17));
		assertEquals("bbaac", l.get(18));
		assertEquals("bbaca", l.get(19));
		assertEquals("bbcaa", l.get(20));
		assertEquals("bcaab", l.get(21));
		assertEquals("bcaba", l.get(22));
		assertEquals("bcbaa", l.get(23));
		assertEquals("caabb", l.get(24));
		assertEquals("cabab", l.get(25));
		assertEquals("cabba", l.get(26));
		assertEquals("cbaab", l.get(27));
		assertEquals("cbaba", l.get(28));
		assertEquals("cbbaa", l.get(29));
	}
}
