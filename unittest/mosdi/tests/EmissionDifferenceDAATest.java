/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.matching.BNDMatcher;
import mosdi.matching.HorspoolMatcher;
import mosdi.matching.Matcher;
import mosdi.paa.DAA;
import mosdi.paa.MinimizedDAA;
import mosdi.paa.apps.EmissionDifferenceDAA;
import mosdi.paa.apps.PatternMatchingDAA;
import mosdi.util.Alphabet;

public class EmissionDifferenceDAATest extends TestCase {
	
	private static final int[] text = Alphabet.getDnaAlphabet().buildIndexArray("GCCCACGCAGGGTCAGGCACCACAATTTTCCTGTCAATGTTCTGCTTCCTGCGTCCGTAAAGAATTTCCTATTACCCTAGGGCTCAAAATAGCCTGGTAATCTTTACACTTCCGTCGATGAGATTACACACGCACTTTAGCGAACAAAGACTACTCTAGGACAGGCGCGGGCGCATGAATTGGCCACTACTACCACACTTTGCGGAGGGTGATTATCCGTTTCCAGCGTCTCCAAAATGGGATACCCACGGACGAATTGAAGCGGATGCGGAGTGTTACTTTGCTGGGTTCGTTGCGAGCTGCAACTTTTAAGTATATCAGAAATTATGACCATTCCGTCACCGCGAGAAGGTGCGCTGGATTTCGTCGATTTCTACAACGCGTATAGGGCTCGAGTACCGGGTCTGCACAGTGTTGGTGAATGCATTTCTTTTTGAGTCCTCGACAAAGCAAAGCGCTCGGGTAGGTACCTCCCTAAACAATTCGCATTAGAGAACTAAGACACCGCCTATCTAACGTTACAACAGATGGGCAGATCAGACTCTGAGGGCGACTTAAGCAAACTGGCGCAGACTGTACTGGTTCAACACATGCCTGGCAGCTGCTTCGCAAATGCCTCATTTCGCACAGTTGTTCGTACTTCAGCGCTTGGAGGGTGTCATAGCCCTCTTGGGTACCCCACGAATACGACATGCTCGGGTCCACGCAGGATGAAGGTTTGATCCATACGGGAGGGAAATTCCACCAACAGGCTGGCGAAAGGGGATTGGAAATGCCTTAGGACGTCCTATGGCGGCGATACTCTAATAGCTGACGTTAAACTCGTGAGCCGACCCTTGATTTGTGCCAGTTCCTCGTGCACGTTAAAAGCTGTCCTTCTTGTTAGAAACTGTAAACATGATACCATCACATAGGCATATAAGGGAGTTCGGATATACGATGGTGCGAGGGGCGGCAGTTCCTTATAGGTCAATCTCTACGGATCGGAGTTCGTTTCGAT");
	
	public static void assertDAAs(Matcher matcher) {
	}

	public void testDifference() {
		Alphabet alphabet = Alphabet.getDnaAlphabet();
		Matcher matcher0 = new HorspoolMatcher(alphabet.size(), alphabet.buildIndexArray("GCTCG"));
		Matcher matcher1 = new BNDMatcher(alphabet.size(), alphabet.buildIndexArray("AAATG"));
		DAA daa0 = new MinimizedDAA(new PatternMatchingDAA(matcher0.costScheme(), matcher0.costScheme().computeCost(text)+5));
		DAA daa1 = new PatternMatchingDAA(matcher1.costScheme(), matcher1.costScheme().computeCost(text)+5);
		EmissionDifferenceDAA diffDaa = new EmissionDifferenceDAA(daa0, daa1, 20);
		DAA minDiffDaa = new MinimizedDAA(diffDaa);
		int state0 = daa0.getStartState();
		int state1 = daa1.getStartState();
		int diffState = minDiffDaa.getStartState();
		int value0 = daa0.getStartValue();
		int value1 = daa1.getStartValue();
		int diffValue = minDiffDaa.getStartValue();
		int lastDiffValue = 0;
		assertEquals(0, diffDaa.decodeValue(diffValue));
		int diff = 0;
		for (int c : text) {
			state0 = daa0.getTransitionTarget(state0, c);
			value0 = daa0.performOperation(state0, value0, daa0.getEmission(state0));
			state1 = daa1.getTransitionTarget(state1, c);
			value1 = daa1.performOperation(state1, value1, daa1.getEmission(state1));
			diffState = minDiffDaa.getTransitionTarget(diffState, c);
			lastDiffValue = diffValue;
			diffValue = minDiffDaa.performOperation(diffState, diffValue, minDiffDaa.getEmission(diffState));
			if (diff==EmissionDifferenceDAA.INVALID_VALUE) {
				assertEquals(lastDiffValue, diffValue);
			}
			diff = diffDaa.decodeValue(diffValue);
			if (diff!=EmissionDifferenceDAA.INVALID_VALUE) {
				assertEquals(value0-value1, diff);
			}
		}
	}
	
}
