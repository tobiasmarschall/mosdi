/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.Alphabet;
import mosdi.util.iterators.StringIterator;

public class StringIteratorTest extends TestCase {

	public void testIterator() {
		Alphabet alphabet = new Alphabet("ABC"); 
		StringIterator iterator = new StringIterator(alphabet.size(),3);
		assertTrue(iterator.hasNext());
		assertEquals("AAA", alphabet.buildString(iterator.next()));
		assertEquals(0, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("AAB", alphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("AAC", alphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("ABA", alphabet.buildString(iterator.next()));
		assertEquals(1, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("ABB", alphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("ABC", alphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("ACA", alphabet.buildString(iterator.next()));
		assertEquals(1, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("ACB", alphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("ACC", alphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("BAA", alphabet.buildString(iterator.next()));
		assertEquals(0, iterator.getLeftmostChangedPosition());
		iterator.skip(0);
		assertTrue(iterator.hasNext());
		assertEquals("CAA", alphabet.buildString(iterator.next()));
		assertEquals(0, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("CAB", alphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		iterator.skip(1);
		assertTrue(iterator.hasNext());
		assertEquals("CBA", alphabet.buildString(iterator.next()));
		assertEquals(1, iterator.getLeftmostChangedPosition());
		iterator.skip(2);
		assertTrue(iterator.hasNext());
		assertEquals("CBB", alphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("CBC", alphabet.buildString(iterator.next()));
		assertEquals(2, iterator.getLeftmostChangedPosition());
		assertTrue(iterator.hasNext());
		assertEquals("CCA", alphabet.buildString(iterator.next()));
		assertEquals(1, iterator.getLeftmostChangedPosition());
		iterator.skip(1);
		assertFalse(iterator.hasNext());
	}
	
	public void testIterator2() {
		StringIterator iterator = new StringIterator(4,100);
		assertTrue(iterator.hasNext());

	}

}
