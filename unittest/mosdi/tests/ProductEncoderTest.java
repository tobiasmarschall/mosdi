/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.util.ProductEncoder;
import static org.junit.Assert.assertArrayEquals;

public class ProductEncoderTest extends TestCase {

	public void testEncode1() {
		ProductEncoder pe = new ProductEncoder(8,8,8,8,8,8);
		assertEquals(262144, pe.getValueCount());
		int[] array = {7,3,0,1,6,5};
		int value = pe.encode(array); 
		assertEquals(188959, value);
		assertEquals(7, pe.decodeComponent(0, value));
		assertEquals(3, pe.decodeComponent(1, value));
		assertEquals(0, pe.decodeComponent(2, value));
		assertEquals(1, pe.decodeComponent(3, value));
		assertEquals(6, pe.decodeComponent(4, value));
		assertEquals(5, pe.decodeComponent(5, value));
		assertArrayEquals(array, pe.decode(value));
	}
	
	public void testEncode1reverse() {
		ProductEncoder pe = new ProductEncoder(false,8,8,8,8,8,8);
		assertEquals(262144, pe.getValueCount());
		int[] array = {7,3,0,1,6,5};
		int value = pe.encode(array); 
		assertEquals(241781, value);
		assertEquals(7, pe.decodeComponent(0, value));
		assertEquals(3, pe.decodeComponent(1, value));
		assertEquals(0, pe.decodeComponent(2, value));
		assertEquals(1, pe.decodeComponent(3, value));
		assertEquals(6, pe.decodeComponent(4, value));
		assertEquals(5, pe.decodeComponent(5, value));
		assertArrayEquals(array, pe.decode(value));
	}

	public void testEncode2() {
		ProductEncoder pe = new ProductEncoder(8,5,2,10);
		assertEquals(800, pe.getValueCount());
		int[] array = {7,0,1,2};
		int value = pe.encode(array);
		assertEquals(207, value);
		assertEquals(7, pe.decodeComponent(0, value));
		assertEquals(0, pe.decodeComponent(1, value));
		assertEquals(1, pe.decodeComponent(2, value));
		assertEquals(2, pe.decodeComponent(3, value));
		assertArrayEquals(array, pe.decode(value));
	}

	public void testEncode2reverse() {
		ProductEncoder pe = new ProductEncoder(false,8,5,2,10);
		assertEquals(800, pe.getValueCount());
		int[] array = {7,0,1,2};
		int value = pe.encode(array);
		assertEquals(712, value);
		assertEquals(7, pe.decodeComponent(0, value));
		assertEquals(0, pe.decodeComponent(1, value));
		assertEquals(1, pe.decodeComponent(2, value));
		assertEquals(2, pe.decodeComponent(3, value));
		assertArrayEquals(array, pe.decode(value));
	}

	public void testEncodeSequence() {
		ProductEncoder pe = new ProductEncoder(2,4,10);
		assertEquals(80, pe.getValueCount());
		int[] sequence0 = {0,1,1,0,-1,1,1,0,0,-1};
		int[] sequence1 = {0,1,0,3,-1,0,1,2,3,-1};
		int[] sequence2 = {5,7,0,2,-1,3,3,5,9,-1};
		
		int[] product = pe.encodeSequence(sequence0, sequence1, sequence2);
		int[][] ts = pe.decodeSequence(product);
		assertArrayEquals(sequence0, ts[0]);
		assertArrayEquals(sequence1, ts[1]);
		assertArrayEquals(sequence2, ts[2]);
	}

	public void testEncodeSequenceReverse() {
		ProductEncoder pe = new ProductEncoder(false,2,4,10);
		assertEquals(80, pe.getValueCount());
		int[] sequence0 = {0,1,1,0,-1,1,1,0,0,-1};
		int[] sequence1 = {0,1,0,3,-1,0,1,2,3,-1};
		int[] sequence2 = {5,7,0,2,-1,3,3,5,9,-1};
		
		int[] product = pe.encodeSequence(sequence0, sequence1, sequence2);
		int[][] ts = pe.decodeSequence(product);
		assertArrayEquals(sequence0, ts[0]);
		assertArrayEquals(sequence1, ts[1]);
		assertArrayEquals(sequence2, ts[2]);
	}

}
