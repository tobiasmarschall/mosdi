/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.matching.AlgorithmCostScheme;
import mosdi.matching.BNDMatcher;
import mosdi.matching.BackwardOracleMatcher;
import mosdi.matching.HorspoolMatcher;
import mosdi.matching.Matcher;
import mosdi.paa.DAA;
import mosdi.paa.MinimizedDAA;
import mosdi.paa.apps.PatternMatchingDAA;
import mosdi.paa.apps.PatternMatchingDAAAlternative;
import mosdi.util.Alphabet;

public class PatternMatchingDAAAlternativeTest extends TestCase {
	
	private static final int[] text = Alphabet.getDnaAlphabet().buildIndexArray("GCCCACGCAGGGTCAGGCACCACAATTTTCCTGTCAATGTTCTGCTTCCTGCGTCCGTAAAGAATTTCCTATTACCCTAGGGCTCAAAATAGCCTGGTAATCTTTACACTTCCGTCGATGAGATTACACACGCACTTTAGCGAACAAAGACTACTCTAGGACAGGCGCGGGCGCATGAATTGGCCACTACTACCACACTTTGCGGAGGGTGATTATCCGTTTCCAGCGTCTCCAAAATGGGATACCCACGGACGAATTGAAGCGGATGCGGAGTGTTACTTTGCTGGGTTCGTTGCGAGCTGCAACTTTTAAGTATATCAGAAATTATGACCATTCCGTCACCGCGAGAAGGTGCGCTGGATTTCGTCGATTTCTACAACGCGTATAGGGCTCGAGTACCGGGTCTGCACAGTGTTGGTGAATGCATTTCTTTTTGAGTCCTCGACAAAGCAAAGCGCTCGGGTAGGTACCTCCCTAAACAATTCGCATTAGAGAACTAAGACACCGCCTATCTAACGTTACAACAGATGGGCAGATCAGACTCTGAGGGCGACTTAAGCAAACTGGCGCAGACTGTACTGGTTCAACACATGCCTGGCAGCTGCTTCGCAAATGCCTCATTTCGCACAGTTGTTCGTACTTCAGCGCTTGGAGGGTGTCATAGCCCTCTTGGGTACCCCACGAATACGACATGCTCGGGTCCACGCAGGATGAAGGTTTGATCCATACGGGAGGGAAATTCCACCAACAGGCTGGCGAAAGGGGATTGGAAATGCCTTAGGACGTCCTATGGCGGCGATACTCTAATAGCTGACGTTAAACTCGTGAGCCGACCCTTGATTTGTGCCAGTTCCTCGTGCACGTTAAAAGCTGTCCTTCTTGTTAGAAACTGTAAACATGATACCATCACATAGGCATATAAGGGAGTTCGGATATACGATGGTGCGAGGGGCGGCAGTTCCTTATAGGTCAATCTCTACGGATCGGAGTTCGTTTCGAT");
	
	public static void assertDAAs(Matcher matcher) {
		AlgorithmCostScheme costScheme = matcher.costScheme();
		int totalCost = costScheme.computeCost(text);
		PatternMatchingDAAAlternative daa0 = new PatternMatchingDAAAlternative(costScheme, totalCost+20);
		PatternMatchingDAA daa1 = new PatternMatchingDAA(costScheme, totalCost+20);
		DAA daa0m = new MinimizedDAA(daa0);
		DAA daa1m = new MinimizedDAA(daa1);
		int state0 = daa0.getStartState();
		int state1 = daa1.getStartState();
		int state0m = daa0m.getStartState();
		int state1m = daa1m.getStartState();
		int value0 = daa0.getStartValue();
		int value1 = daa1.getStartValue();
		int value0m = daa0m.getStartValue();
		int value1m = daa1m.getStartValue();
		assertEquals(0, daa0.decodeValueToCost(value0));
		assertEquals(0, value1);
		for (int c : text) {
			state0 = daa0.getTransitionTarget(state0, c);
			value0 = daa0.performOperation(state0, value0, daa0.getEmission(state0));
			state0m = daa0m.getTransitionTarget(state0m, c);
			value0m = daa0m.performOperation(state0m, value0m, daa0m.getEmission(state0m));
			assertEquals(value0, value0m);
			state1 = daa1.getTransitionTarget(state1, c);
			value1 = daa1.performOperation(state1, value1, daa1.getEmission(state1));
			state1m = daa1m.getTransitionTarget(state1m, c);
			value1m = daa1m.performOperation(state1m, value1m, daa1m.getEmission(state1m));
			assertEquals(value1, value1m);
			int cost0 = daa0.decodeValueToCost(value0);
			int cost1 = value1;
			assertEquals(cost0, cost1);
		}
		assertEquals(totalCost, value1);
	}
	
	public void testDAA() {
		Alphabet alphabet = Alphabet.getDnaAlphabet();
		assertDAAs(new HorspoolMatcher(alphabet.size(), alphabet.buildIndexArray("GCTCG")));
		assertDAAs(new BNDMatcher(alphabet.size(), alphabet.buildIndexArray("AAATG")));
		assertDAAs(new BackwardOracleMatcher(alphabet.size(), alphabet.buildIndexArray("GAGAG")));
	}
	
}
