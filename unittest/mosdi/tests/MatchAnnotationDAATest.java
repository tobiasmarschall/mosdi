/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tests;

import junit.framework.TestCase;
import mosdi.fa.CDFA;
import mosdi.fa.DFAFactory;
import mosdi.fa.GeneralizedString;
import mosdi.paa.apps.MatchAnnotationDAA;
import mosdi.util.Alphabet;
import mosdi.util.Iupac;
import mosdi.util.ProductEncoder;
import mosdi.util.SingletonList;

public class MatchAnnotationDAATest extends TestCase {

	// Pattern: [AC]CC[CGT]
	// CCGACTCAAGATGGACGGTGTGTGTGATGTCTGGCCATGGGGACTGCGTTGTTCGTCCAATGTCTCAGTCCCCAACGAATTGGGAAGGAAGAAAATCCCACTAAATATTCACCCCCTGGGCGACTGACACCTTGTTAGTAAGACAACCGACTCAATATAGATCACGAGGGTCAGGATCTCTGACCAGCTCTCTCAGATCGACGACACCTGCTAGTTAACGACGATTGCTGCCGGACCAGTCGTCTATTAT
	// 3248288033297117941190326462534134271027274928020840669115923798240596081723814743198555127197524918673978673999148550482057973270101917974091329724012903344845749454928683737225796482273009568049614081688468809843004740572852100922040649525154414676
	//                                                                      XXXX                                     XXXX              XXXX             XXXX                                                        XXXX                                         
	//                                                                                                                XXXX                                         
	//                                                                                                                 XXXX                                         
	//                                                                                                                  XXXX                                         
	
	final static String sequence = "CCGACTCAAGATGGACGGTGTGTGTGATGTCTGGCCATGGGGACTGCGTTGTTCGTCCAATGTCTCAGTCCCCAACGAATTGGGAAGGAAGAAAATCCCACTAAATATTCACCCCCTGGGCGACTGACACCTTGTTAGTAAGACAACCGACTCAATATAGATCACGAGGGTCAGGATCTCTGACCAGCTCTCTCAGATCGACGACACCTGCTAGTTAACGACGATTGCTGCCGGACCAGTCGTCTATTAT";
	final static int[] annotations = {3, 2, 4, 8, 2, 8, 8, 0, 3, 3, 2, 9, 7, 1, 1, 7, 9, 4, 1, 1, 9, 0, 3, 2, 6, 4, 6, 2, 5, 3, 4, 1, 3, 4, 2, 7, 1, 0, 2, 7, 2, 7, 4, 9, 2, 8, 0, 2, 0, 8, 4, 0, 6, 6, 9, 1, 1, 5, 9, 2, 3, 7, 9, 8, 2, 4, 0, 5, 9, 6, 0, 8, 1, 7, 2, 3, 8, 1, 4, 7, 4, 3, 1, 9, 8, 5, 5, 5, 1, 2, 7, 1, 9, 7, 5, 2, 4, 9, 1, 8, 6, 7, 3, 9, 7, 8, 6, 7, 3, 9, 9, 9, 1, 4, 8, 5, 5, 0, 4, 8, 2, 0, 5, 7, 9, 7, 3, 2, 7, 0, 1, 0, 1, 9, 1, 7, 9, 7, 4, 0, 9, 1, 3, 2, 9, 7, 2, 4, 0, 1, 2, 9, 0, 3, 3, 4, 4, 8, 4, 5, 7, 4, 9, 4, 5, 4, 9, 2, 8, 6, 8, 3, 7, 3, 7, 2, 2, 5, 7, 9, 6, 4, 8, 2, 2, 7, 3, 0, 0, 9, 5, 6, 8, 0, 4, 9, 6, 1, 4, 0, 8, 1, 6, 8, 8, 4, 6, 8, 8, 0, 9, 8, 4, 3, 0, 0, 4, 7, 4, 0, 5, 7, 2, 8, 5, 2, 1, 0, 0, 9, 2, 2, 0, 4, 0, 6, 4, 9, 5, 2, 5, 1, 5, 4, 4, 1, 4, 6, 7, 6};
	
	public void testDAA() {
		Alphabet dnaAlphabet = Alphabet.getDnaAlphabet();
		ProductEncoder productAlphabet = new ProductEncoder(4,10);
		int[] productSequence = productAlphabet.encodeSequence(dnaAlphabet.buildIndexArray(sequence), annotations);
		GeneralizedString pattern = Iupac.toGeneralizedString("MCCB");
		CDFA cdfa = DFAFactory.build(dnaAlphabet, new SingletonList<GeneralizedString>(pattern));
		MatchAnnotationDAA daa = new MatchAnnotationDAA(10, cdfa, pattern.length(), 10, 200);
		int value = daa.computeValue(productSequence);
		assertEquals(8, daa.valueToMatchCount(value));
		assertEquals(147, daa.valueToAnnotationSum(value));
		daa = new MatchAnnotationDAA(10, cdfa, pattern.length(), 10, 100);
		value = daa.computeValue(productSequence);
		assertEquals(8, daa.valueToMatchCount(value));
		assertEquals(100, daa.valueToAnnotationSum(value));
		daa = new MatchAnnotationDAA(10, cdfa, pattern.length(), 5, 150);
		value = daa.computeValue(productSequence);
		assertEquals(5, daa.valueToMatchCount(value));
		assertEquals(147, daa.valueToAnnotationSum(value));
		daa = new MatchAnnotationDAA(10, cdfa, pattern.length(), 5, 100);
		value = daa.computeValue(productSequence);
		assertEquals(5, daa.valueToMatchCount(value));
		assertEquals(100, daa.valueToAnnotationSum(value));
	}
	
	public void testPalindromic() {
		final String sequence = "CCGACGTAAGCCGTA";
		//                       324828803329711
		final int[] annotations = {3, 2, 4, 8, 2, 8, 8, 0, 3, 3, 2, 9, 7, 1, 1};
		Alphabet dnaAlphabet = Alphabet.getDnaAlphabet();
		ProductEncoder productAlphabet = new ProductEncoder(4,10);
		int[] productSequence = productAlphabet.encodeSequence(dnaAlphabet.buildIndexArray(sequence), annotations);
		String pattern = "MCGT";
		CDFA cdfa = DFAFactory.buildFromIupacPattern(pattern, false);
		MatchAnnotationDAA daa = new MatchAnnotationDAA(10, cdfa, pattern.length(), 10, 200);
		int value = daa.computeValue(productSequence);
		assertEquals(2, daa.valueToMatchCount(value));
		assertEquals(45, daa.valueToAnnotationSum(value));
		cdfa = DFAFactory.buildFromIupacPattern(pattern, true);
		daa = new MatchAnnotationDAA(10, cdfa, pattern.length(), 10, 200);
		value = daa.computeValue(productSequence);
		assertEquals(3, daa.valueToMatchCount(value));
		assertEquals(71, daa.valueToAnnotationSum(value));
	}
	
}
