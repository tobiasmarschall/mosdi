/*    Copyright 2013 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.subcommands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mosdi.fa.CDFA;
import mosdi.fa.DFAFactory;
import mosdi.fa.GeneralizedString;
import mosdi.util.Alphabet;
import mosdi.util.FileUtils;
import mosdi.util.Iupac;
import mosdi.util.Log;
import mosdi.util.NamedSequence;
import mosdi.util.SequenceUtils;

public class FindOccurrencesSubcommand extends Subcommand {

	@Override
	public String usage() {
		return 
		super.usage()+" [options] <fasta-file> <iupac-pattern>\n" +
		"\n" +
		"Options:\n" +
		"  -r: also output matches of reverse complementary motif\n" +
		"  -F: <iupac-pattern> is a file with patterns instead of a single pattern\n" +
		"  -i: ignore unknown characters in input sequences";
	}

	@Override
	public String description() {
		return "Reports all matches of a given (set of) IUPAC pattern.";
	}

	@Override
	public String name() {
		return "find-matches";
	}

	@Override
	public int run(String[] args) {
		parseOptions(args, 2, "rFi");

		// Option dependencies

		// Mandatory arguments
		String fastaFilename = getStringArgument(0);
		String patternArgument = getStringArgument(1);

		// Options
		boolean considerReverse = getBooleanOption("r", false);
		boolean readPatternsFromFile = getBooleanOption("F", false);
		boolean ignoreUnknownChars = getBooleanOption("i", false);

		Alphabet alphabet = Alphabet.getDnaAlphabet();
		List<NamedSequence> namedSequences = null;

		try {
			namedSequences = SequenceUtils.readFastaFile(fastaFilename, alphabet, ignoreUnknownChars);
		} catch (Exception e) {
			Log.errorln(e.toString());
			System.exit(1);
		}

		// create list of patterns
		List<String> patternList = null;
		if (readPatternsFromFile) {
			// read patterns from file
			patternList = FileUtils.readPatternFile(patternArgument);
		} else {
			patternList = new ArrayList<String>(1);
			patternList.add(patternArgument);
		}

		// construct automaton
		List<GeneralizedString> l = new ArrayList<GeneralizedString>(1);
		for (String pattern : patternList) {
			l.add(Iupac.toGeneralizedString(pattern));
			if (considerReverse) l.add(Iupac.toGeneralizedString(Iupac.reverseComplementary(pattern)));
		}
		CDFA cdfa = DFAFactory.build(alphabet, l, 10000);
		int total_matches = 0;
		for (NamedSequence ns : namedSequences) {
			for (CDFA.MatchPosition mp : cdfa.findMatchPositions(ns.getSequence(), ignoreUnknownChars)) {
				System.out.println(">> " + ns.getName() + " " + mp.getPosition());
				total_matches += 1;
			}
		}
		Log.printf(Log.Level.STANDARD, "Total matches: %d%n", total_matches);
		return 0;
	}	
	
}
