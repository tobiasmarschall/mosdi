/*    Copyright 2011 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.subcommands;

import java.util.List;

import mosdi.fa.FiniteMemoryTextModel;
import mosdi.fa.IIDTextModel;
import mosdi.fa.MarkovianTextModel;
import mosdi.paa.DAA;
import mosdi.paa.MinimizedDAA;
import mosdi.paa.PAA;
import mosdi.paa.TextBasedPAA;
import mosdi.paa.apps.DispensationOrderDAA;
import mosdi.util.Alphabet;
import mosdi.util.Log;
import mosdi.util.NamedSequence;
import mosdi.util.SequenceUtils;

public class ReadLengthDistSubcommand extends Subcommand {

	@Override
	public String usage() {
		return
		super.usage()+" [options] <dispensation-order> <flow-count>\n" +
		"Options:\n" +
		"  -M <order>: Order of the Markov model. Default: 0 (i.i.d.)\n" +
		"  -t <fasta-file>: Estimate text model from given sequence(s).\n" +
		"  -q <q-gram-table-file>: Estimate text model from given q-gram table.\n" +
		"                          A q-gram table can be created by using\n" +
		"                          \"mosdi-utils count-qgrams\".\n" +
		"  -m <max-length>: maximal read length up to which the distribution is\n" +
		"                   computed (default: 2*<flow-count>).";
	}

	@Override
	public String description() {
		return "Computes the distribution of read lengths for a given dispensation order.";
	}

	@Override
	public String name() {
		return "read-length-dist";
	}

	@Override
	public int run(String[] args) {
		parseOptions(args, 2, "t:M:q:m:");

		// Option dependencies
		exclusiveOptions("q", "t");
		exclusiveOptions("q", "M");
		impliedOptions("M", "t");
		
		Alphabet alphabet = Alphabet.getDnaAlphabet();

		// Mandatory arguments
		int[] dispensationOrder = alphabet.buildIndexArray(getStringArgument(0));
		int flowCount = getIntArgument(1);

		// Options
		int textModelOrder = getNonNegativeIntOption("M", 0);
		String qGramTableFilename = getStringOption("q", null);
		String fastaFilename = getStringOption("t", null);
		int maxReadLength = getNonNegativeIntOption("m", 2*flowCount);
		
		// Create text model
		FiniteMemoryTextModel textModel = null;
		try {
			if (fastaFilename!=null) {
				List<NamedSequence> sequences = SequenceUtils.readFastaFile(fastaFilename, alphabet, true);
				if (textModelOrder==0) {
					textModel = new IIDTextModel(alphabet.size(), SequenceUtils.sequenceList(sequences));
				} else {
					textModel = new MarkovianTextModel(textModelOrder, alphabet.size(), SequenceUtils.sequenceList(sequences));
				}
			}
			if (qGramTableFilename!=null) {
				textModel = SequenceUtils.buildTextModelFromQGramFile(qGramTableFilename);
			}
		} catch (Exception e) {
			Log.errorln(e.toString());
			System.exit(1);
		}
		if (textModel==null) {
			textModel = new IIDTextModel(alphabet.size());
		}

		DispensationOrderDAA daa = new DispensationOrderDAA(alphabet.size(), dispensationOrder, flowCount+1);
		Log.printf(Log.Level.VERBOSE, "DAA has %d states.\n", daa.getStateCount());
		DAA minimizedDaa = new MinimizedDAA(daa);
		Log.printf(Log.Level.VERBOSE, "Minimized DAA has %d states.\n", minimizedDaa.getStateCount());
		PAA paa = new TextBasedPAA(minimizedDaa, textModel);
		Log.printf(Log.Level.VERBOSE, "PAA has %d states.\n", paa.getStateCount());		
		double[] distribution = paa.waitingTimeForValue(maxReadLength+1, flowCount+1);
		double expectation = 0.0;
		for (int i=0; i<=maxReadLength; ++i) {
			expectation += distribution[i+1] * i;
		}
		
		StringBuffer sb = new StringBuffer();
		sb.append(String.format(">>expectation>> %e >>distribution>>", expectation));
		for (int i=0; i<=maxReadLength; ++i) {
			sb.append(String.format(" %e", distribution[i+1]));
		}
		Log.println(Log.Level.STANDARD, sb.toString());
		
		return 0;
	}
}
