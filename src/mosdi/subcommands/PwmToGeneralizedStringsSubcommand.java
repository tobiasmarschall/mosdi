package mosdi.subcommands;

import mosdi.matching.PositionWeightMatrix;
import mosdi.util.Alphabet;
import mosdi.util.Iupac;
import mosdi.util.Log;
import mosdi.util.SequenceUtils;

public class PwmToGeneralizedStringsSubcommand extends Subcommand {
	@Override
	public String usage() {
		return
		super.usage()+" <pwm-file> <threshold>";
	}

	@Override
	public String description() {
		return
		"Given a position weight matrix (PWM) and a threshold t, generates a set of IUPAC strings "+
		"such that they match a string iff the PWM score for this string is >=t.";
	}

	@Override
	public String name() {
		return "pwm-to-iupac";
	}

	@Override
	public int run(String[] args) {
		parseOptions(args, 2, "x");

		// Option dependencies
		// -- none --

		// Mandatory arguments
		String pwmFilename = getStringArgument(0);
		double threshold = getDoubleArgument(1);

		// Options
		// -- none --

		Alphabet alphabet = Alphabet.getDnaAlphabet();
		PositionWeightMatrix pwm = new PositionWeightMatrix(SequenceUtils.readPositionMatrix(pwmFilename, alphabet)); 
		Alphabet iupacAlphabet = Alphabet.getIupacAlphabet();
		StringBuffer sb = new StringBuffer();
		for (int[] s : pwm.toGeneralizedStrings(threshold, Iupac.asGeneralizedAlphabet())) {
			if (sb.length()>0) sb.append(',');
			sb.append(iupacAlphabet.buildString(s));
		}
		Log.println(Log.Level.STANDARD, ">> "+sb.toString());
		return 0;
	}
	
}
