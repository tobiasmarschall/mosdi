/*    Copyright 2010,2012 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.subcommands;

import java.util.List;

import mosdi.fa.IIDTextModel;
import mosdi.fa.MarkovianTextModel;
import mosdi.fa.TextModel;
import mosdi.util.Alphabet;
import mosdi.util.Log;
import mosdi.util.NamedSequence;
import mosdi.util.SequenceUtils;

public class RandomTextSubcommand extends Subcommand {

	@Override
	public String usage() {
		return
		super.usage()+" [options] <length>\n" +
		"Options:\n" +
		"  -M <order>: Order of the Markov model. Default: 0 (i.i.d.)\n" +
		"  -t <fasta-file>: Estimate text model from given sequence(s).\n" +
		"  -q <q-gram-table-file>: Estimate text model from given q-gram table.\n" +
		"                          A q-gram table can be created by using\n" +
		"                          \"mosdi-utils count-qgrams\".\n" +
		"  -n <number-of-sequences>: number of sequences to be generated\n" +
		"  -f: enable FASTA output\n" +
		"  -A <alphabet>: if either \"dna\" or \"protein\", the respective alphabet of\n" +
		"                 nucleotides or amino acids is used, respectively. Otherwise,\n" +
		"                 an alphabet consisting of the given characters is used (default:\"dna\").";
	}

	@Override
	public String description() {
		return "Generates a random text.";
	}

	@Override
	public String name() {
		return "random-text";
	}

	@Override
	public int run(String[] args) {
		parseOptions(args, 1, "t:M:n:q:fA:");

		// Option dependencies
		exclusiveOptions("q", "t");
		exclusiveOptions("q", "M");
		impliedOptions("M", "t");

		// Mandatory arguments
		int length = getIntArgument(0);

		// Options
		int textModelOrder = getNonNegativeIntOption("M", 0);
		String qGramTableFilename = getStringOption("q", null);
		int n = getPositiveIntOption("n", 1);
		boolean fastaOutput = getBooleanOption("f", false);
		String fastaFilename = getStringOption("t", null);
		String alphabetParameter = getStringOption("A", "dna");

		Alphabet alphabet;
		if (alphabetParameter.equals("dna")) {
			alphabet = Alphabet.getDnaAlphabet();
		} else if (alphabetParameter.equals("protein")) {
			alphabet = Alphabet.getAminoAcidAlphabet();
		} else {
			alphabet = new Alphabet(alphabetParameter);
		}
		
		// Create text model
		TextModel textModel = null;
		try {
			if (fastaFilename!=null) {
				List<NamedSequence> sequences = SequenceUtils.readFastaFile(fastaFilename, alphabet, true);
				if (textModelOrder==0) {
					textModel = new IIDTextModel(alphabet.size(), SequenceUtils.sequenceList(sequences));
				} else {
					textModel = new MarkovianTextModel(textModelOrder, alphabet.size(), SequenceUtils.sequenceList(sequences));
				}
			}
			if (qGramTableFilename!=null) {
				textModel = SequenceUtils.buildTextModelFromQGramFile(qGramTableFilename);
			}
		} catch (Exception e) {
			Log.errorln(e.toString());
			System.exit(1);
		}
		if (textModel==null) {
			textModel = new IIDTextModel(alphabet.size());
		}
		
		for (int i=0; i<n; ++i) {
			if (fastaOutput) Log.printf(Log.Level.STANDARD, ">random%d\n", i); 
			Log.println(Log.Level.STANDARD, alphabet.buildString(textModel.generateRandomText(length)));
		}
		return 0;
	}
}
