/*    Copyright 2011 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.subcommands;

import mosdi.util.Alphabet;
import mosdi.util.Iupac;
import mosdi.util.Log;

public class ReverseComplementSubcommand extends Subcommand{
	
	@Override
	public String usage() {
		return 
		super.usage()+" <iupac-pattern>\n" +
		"\n" +
		"Options:\n" +
		"  -b: print both, given pattern and reverse complement.";
	}

	@Override
	public String description() {
		return "Returns the reverse complement of a given IUPAC pattern.";
	}

	@Override
	public String name() {
		return "rev-comp";
	}

	@Override
	public int run(String[] args) {
		parseOptions(args, 1, "b");

		// Option dependencies
		// -- none --

		// Mandatory arguments
		String pattern = getStringArgument(0);
		
		// Options
		boolean printBoth = getBooleanOption("b", false);

		Alphabet iupacAlphabet = Alphabet.getIupacAlphabet();
		int[] s = iupacAlphabet.buildIndexArray(pattern);
		if (printBoth) {
			Log.println(Log.Level.STANDARD, pattern + " " + iupacAlphabet.buildString(Iupac.reverseComplementary(s)));
		} else {
			Log.println(Log.Level.STANDARD, iupacAlphabet.buildString(Iupac.reverseComplementary(s)));	
		}
		
		return 0;
	}

}
