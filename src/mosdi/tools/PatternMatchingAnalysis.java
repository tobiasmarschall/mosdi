/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tools;

import java.util.ArrayList;
import java.util.List;

import mosdi.matching.BNDMatcher;
import mosdi.matching.BackwardOracleMatcher;
import mosdi.matching.HorspoolMatcher;
import mosdi.matching.Matcher;
import mosdi.subcommands.AutomataSizesSubcommand;
import mosdi.subcommands.CostDistributionSubcommand;
import mosdi.subcommands.ShowDAASubcommand;
import mosdi.subcommands.Subcommand;
import mosdi.util.SubcommandApplication;

public class PatternMatchingAnalysis {
	
	public static void main(String[] args) {
		List<Subcommand> subcommandList = new ArrayList<Subcommand>();
		subcommandList.add(new CostDistributionSubcommand());
		subcommandList.add(new AutomataSizesSubcommand());
		subcommandList.add(new ShowDAASubcommand());
		SubcommandApplication app = new SubcommandApplication("mosdi-pm-analysis", subcommandList);
		app.run(args);
	}

	public static String allMatcherNames() {
		return "horspool, bndm, bom";
	}
	
	public static Matcher getMatcher(String algorithmName, int alphabetSize, int[] pattern) {
		if (algorithmName.equals("horspool")) return new HorspoolMatcher(alphabetSize,pattern);
		if (algorithmName.equals("bndm")) return new BNDMatcher(alphabetSize,pattern);
		if (algorithmName.equals("bom")) return new BackwardOracleMatcher(alphabetSize,pattern);
		return null;
	}

	public static void usage() {
		System.err.println("Usage: PatternMatchingAnalysis <pattern> <textlength>");
		System.exit(-1);
	}
	
}
