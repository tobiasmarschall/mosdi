/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.tools;

import java.util.ArrayList;
import java.util.List;

import mosdi.subcommands.Subcommand;
import mosdi.util.SubcommandApplication;

public class Utils {
	public static void main(String[] args) {
		List<Subcommand> subcommandList = new ArrayList<Subcommand>();
		subcommandList.add(new mosdi.subcommands.IupacGenSubcommand());
		subcommandList.add(new mosdi.subcommands.IupacRangesSubcommand());
		subcommandList.add(new mosdi.subcommands.IupacAbelianGenSubcommand());
		subcommandList.add(new mosdi.subcommands.AnnotateSubcommand());
		subcommandList.add(new mosdi.subcommands.CountMatchesSubcommand());
		subcommandList.add(new mosdi.subcommands.GeneratePfmSubcommand());
		subcommandList.add(new mosdi.subcommands.PfmToPwmSubcommand());
		subcommandList.add(new mosdi.subcommands.PwmToGeneralizedStringsSubcommand());
		subcommandList.add(new mosdi.subcommands.RandomTextSubcommand());
		subcommandList.add(new mosdi.subcommands.RandomCopySubcommand());
		subcommandList.add(new mosdi.subcommands.CutOutMotifSubcommand());
		subcommandList.add(new mosdi.subcommands.CountQgramsSubcommand());
		subcommandList.add(new mosdi.subcommands.QgramExpectations());
		subcommandList.add(new mosdi.subcommands.ReverseComplementSubcommand());
		subcommandList.add(new mosdi.subcommands.LengthHistogramSubcommand());
		subcommandList.add(new mosdi.subcommands.FindOccurrencesSubcommand());
		SubcommandApplication app = new SubcommandApplication("mosdi-utils", subcommandList);
		app.run(args);
	}

}
