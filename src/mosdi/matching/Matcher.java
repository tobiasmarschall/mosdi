/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.matching;

public abstract class Matcher {

	protected int characterAccesses = -1;
	protected int alphabetSize;
	
	protected Matcher(int alphabetSize) {
		this.alphabetSize = alphabetSize;
	}
	
	/** Returns the number of matches found in the given text. */
	public abstract int findMatches(int[] text);
	
	/** Returns the cost scheme that gives shift and the number of character
	 *  accesses for each possible window. */
	public abstract AlgorithmCostScheme costScheme();
	
	/** Returns the number of character accesses performed by the last
	 *  execution of findMatches(). If findMatches() has not been
	 *  executed so far, -1 is returned. */
	public int getCharacterAccesses() {
		return characterAccesses;
	}
	
	/** Returns the number of positions in the text that are covered by pattern occurrences. */
	public abstract int coveredPositions(int[] text);

	/** Returns all start positions of matches in the given text. */
	public abstract int[] findAllMatchPositions(int[] text);
}
