/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.matching;

import java.util.Arrays;

/** Cost scheme for a window-based pattern matching algorithm. */
public abstract class AlgorithmCostScheme {

	public abstract int alphabetSize();
	
	public abstract int windowSize();
	
	public abstract int maxWindowCost();
	
	public abstract int windowCost(int[] windowContent);
	
	public abstract int shift(int[] windowContent);
	
	public int computeCost(int[] text) {
		int windowSize = windowSize();
		// starting position of current window
		int pos = 0;
		int cost = 0;
		while (pos+windowSize <= text.length) {
			int[] window = Arrays.copyOfRange(text, pos, pos+windowSize);
			cost += windowCost(window);
			pos += shift(window);
		}
		return cost;
	}
	
}
