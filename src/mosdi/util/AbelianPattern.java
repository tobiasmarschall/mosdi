/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

import java.util.Iterator;

import mosdi.util.iterators.AbelianPatternInstanceIterator;

/** Represents an abelian pattern, i.e. a pattern given by a character
 *  composition. All strings with this compositions are instance of the
 *  given pattern. Iterating over an AbelianPattern yields all strings that
 *  are instances of the pattern in lexicographical order.
 */
public class AbelianPattern implements Iterable<int[]> {

	private int length;
	private int[] frequencies;
	
	public AbelianPattern(int[] frequencies) {
		this.frequencies = frequencies;
		length = 0;
		for (int i : frequencies) length+=i;
	}

	@Override
	public Iterator<int[]> iterator() {
		return new AbelianPatternInstanceIterator(frequencies);
	}
	
	public long instanceCount() {
		return Combinatorics.multinomial(length, frequencies);
	}

}
