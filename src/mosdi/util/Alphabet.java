/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

/** Represents an alphabet by providing a mapping between
 *  characters and an indices between 0 and n-1. 
 *  <p>
 *  Note that MoSDi uses integers from 0 to n-1 as characters in most places. The mapping
 *  provided by this class is then used to translate input and output to and
 *  from this integer representation.
 */
public class Alphabet implements Iterable<Character> {
	private char[] chars;
	private int[] charIndices;
	
	private boolean useSeparator;
	private char separator;

	/** Construct alphabet containing all characters (0-255). */
	public Alphabet() {
		this.useSeparator = false;
		this.separator = 0;
		chars = new char[256];
		charIndices = new int[256];
		for (int i=0; i<256; ++i) {
			chars[i] = (char)i;
			charIndices[i] = i;
		}
	}
	
	public Alphabet(Collection<Character> l) {
		this.useSeparator = false;
		this.separator = 0;
		List<Character> sortedList = new ArrayList<Character>(l);
		Collections.sort(sortedList);
		chars = new char[l.size()];
		charIndices = new int[256];
		Arrays.fill(charIndices, -1);
		int i=0;
		for (char c : sortedList) {
			chars[i]=c;
			charIndices[c]=i;
			++i;
		}
	}
	
	/** Constructs an alphabet that contains all characters occuring in 
	 *  a given string s.
	 */
	public Alphabet(String s) {
		boolean[] charsPresent = new boolean[256];
		int charCount = 0;
		for (int i=0; i<s.length(); ++i) {
			int c = (int)s.charAt(i);
			if (!charsPresent[c]) {
				charsPresent[c] = true;
				charCount+=1;
			}
		}
		chars = new char[charCount];
		charIndices = new int[256];
		Arrays.fill(charIndices, -1);
		int n = 0;
		for (int c=0; c<charsPresent.length; ++c) {
			if (charsPresent[c]) {
				chars[n]=(char)c;
				charIndices[c]=n;
				n+=1;
			}
		}
	}

	/** Set special character that separates sequences. This character is
	 *  NOT part of the alphabet. It is numerically represented by -1. */
	public void setSeparator(char separator) {
		if (getIndex(separator)>=0) throw new IllegalArgumentException("Separator must not be contained in alphabet.");
		this.separator = separator;
		this.useSeparator = true;
	}

	private class ArrayIterator implements Iterator<Character> {
		private int i;
		ArrayIterator() { i=0; }
		public boolean hasNext() { return i<chars.length; }
		public Character next() {
			try {
				return chars[i++];
			} catch (IndexOutOfBoundsException e) {
				throw new NoSuchElementException();
			}
		}
		public void remove() { throw new UnsupportedOperationException(); }
	}
	
	public Iterator<Character> iterator() { return new ArrayIterator(); }
	
	public int size() { return chars.length; }
	
	public char get(int index) { return chars[index]; }
	
	/**
	 * @param set a set of characters
	 * @return a set containing characters of this alphabet which are not present in the given set.
	 */
	public Set<Character> getComplementSet(Set<Character> set) {
		Set<Character> complementSet = new TreeSet<Character>(); 
		for(char c : chars) {
			if(!set.contains(c)) {
				complementSet.add(c);
			}
		}
		return complementSet;
	}
	
	public int getIndex(char c) { return charIndices[c]; }
	public boolean contains(char c) { return charIndices[c]>=0; }

	/** Given an array of character indices, this methods returns the 
	 *  corresponding string. */
	public String buildString(int[] indices) {
		StringBuilder sb = new StringBuilder(indices.length);
		for (int c : indices) {
			if (useSeparator && (c==-1)) sb.append(separator);
			else sb.append(chars[c]);
		}
		return sb.toString();
	}

	/** Reverse function to buildString(). */
	public int[] buildIndexArray(CharSequence s) {
		return buildIndexArray(s,false, false);
	}
	public int[] buildIndexArray(CharSequence s, boolean appendMinusOne) {
		return buildIndexArray(s, appendMinusOne, false);
	}
	
	/** Reverse function to buildString(). */
	public int[] buildIndexArray(CharSequence s, boolean appendMinusOne, boolean replaceUnknownByMinusOne) {
		int[] result = new int[appendMinusOne?s.length()+1:s.length()];
		for (int i=0; i<s.length(); ++i) {
			if (useSeparator && (s.charAt(i)==separator)) {
				result[i] = -1;	
			} else {
				int c = charIndices[s.charAt(i)];
				if ((c==-1) && !replaceUnknownByMinusOne) throw new UnknownCharacterException("Unknown character: " + s.charAt(i));
				result[i] = c;
			}
		}
		if (appendMinusOne) result[s.length()] = -1;
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Alphabet)) return false;
		return Arrays.equals(chars, ((Alphabet)obj).chars);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(chars);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		for(char c : this) {
			sb.append(c);
			sb.append(" ");
		}
		sb.append("}");
		return sb.toString();	
	}

   /*********************** creation methods ***********************/

   public static Alphabet getAminoAcidAlphabet() {
      // IUPAC codes, see http://bioinformatics.org/sms2/iupac.html
      Character[] chars = {'A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y'};
      return new Alphabet(Arrays.asList(chars));
   }
   
   public static Alphabet getDnaAlphabetLowercase() {
      Character[] chars = {'a','c','g','t'};
      return new Alphabet(Arrays.asList(chars));
   }

   public static final Alphabet getDnaAlphabet() {
      return new Alphabet(Arrays.asList('A', 'C', 'G', 'T'));
   }

   public static final Alphabet getIupacAlphabet() {
      return new Alphabet(Arrays.asList('A', 'C', 'G', 'T', 'R', 'Y', 'S', 'W', 'K', 'M', 'B', 'D', 'H', 'V', 'N'));
   }
	
}
