/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util.iterators;

import java.util.Iterator;

import mosdi.util.IupacStringConstraints;

public class IupacPatternGenerator implements Iterable<int[]> {
	private int length;
	private IupacStringConstraints constraints;
	
	public IupacPatternGenerator(int length, IupacStringConstraints constraints) {
		this.length = length;
		this.constraints = constraints;
	}
	
	public IupacPatternGenerator(int length, int[] minFrequency, int[] maxFrequency) {
		if ((minFrequency.length!=4) || (maxFrequency.length!=4)) throw new IllegalArgumentException();
		this.length = length;
		this.constraints = new IupacStringConstraints(minFrequency, maxFrequency);
	}

	@Override
	public Iterator<int[]> iterator() {
		return new IupacPatternIterator(length, constraints);
	}
	
}
