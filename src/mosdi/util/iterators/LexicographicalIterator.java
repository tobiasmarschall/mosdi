/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util.iterators;

import java.util.Iterator;

/** Common interface for iterators that iterate over strings of equal length
 *  in lexicographical order. */
public abstract class LexicographicalIterator implements Iterator<int[]> {

	/** Gives the string that will be returned by next call to next(). */
	public abstract int[] peek();

	/** Advance to next character at given position, that means a subsequent call
	 *  to next() will return the next string in lexicographical order, such that
	 *  leftmostChangedPosition<=position. */
	public abstract void skip(int position);

	/** Returns the leftmost position that changed when the last returned
	 *  string was generated. */
	public abstract int getLeftmostChangedPosition();

	public abstract int getStringLength();
	
	/** Skip as many strings such that the next call to next() returns a pattern >= lowerBound. */ 
	public void skipByLowerBound(int[] lowerBound) {
		int length = getStringLength();
		int i=0;
		for (; i<Math.min(lowerBound.length, length); ++i) {
			while (hasNext() && (peek()[i]<lowerBound[i])) {
				next();
				skip(i);
			}
			if (!hasNext()) return;
			if (peek()[i]>lowerBound[i]) break;
		}
		if ((i==length) && (lowerBound.length>length)) next();
	}
}
