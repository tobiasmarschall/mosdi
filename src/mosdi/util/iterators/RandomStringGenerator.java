/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util.iterators;

import java.util.Iterator;
import java.util.Random;

import mosdi.util.Alphabet;

/** Produces infinitely many uniformly distributed random strings. */
public class RandomStringGenerator implements Iterable<String> {
	private Alphabet alphabet;
	private int length;
	private Random random;
	
	public RandomStringGenerator(Alphabet alphabet, int length) {
		random = new Random();
		this.length = length;
		this.alphabet = alphabet;
	}
	
	private class RandomStringIterator implements Iterator<String> {

		public boolean hasNext() { return true; }

		public String next() {
			StringBuffer sb = new StringBuffer(length);
			for (int i=0; i<length; ++i) {
				sb.append(alphabet.get(random.nextInt(alphabet.size())));
			}
			return sb.toString();
		}

		public void remove() { throw new UnsupportedOperationException(); }
	}
	
	public Iterator<String> iterator() { return new RandomStringIterator(); }
	
}
