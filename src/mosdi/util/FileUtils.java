/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class FileUtils {

   /** WARNING: ignores all whitespaces in input-file. */
   public static List<String> readPatternFile(String filename) {
   	List<String> patternList = new ArrayList<String>();
   	FileInputStream file = null;
   	try {
   		file = new FileInputStream(filename);
   	} catch (FileNotFoundException e) {
   		Log.errorln("Patternfile not found, sorry!");
   		System.exit(1);
   	}
   	BufferedReader br = new BufferedReader(new InputStreamReader(file));
   	try {
   		while (true) {
   			String line = br.readLine();
   			if (line==null) break;
   			// remove white-spaces and comments
   			StringBuilder sb = new StringBuilder(line.length());
   			for (int i=0; i<line.length(); ++i) {
   				char c = line.charAt(i);
   				if ((c==' ') || (c=='\t')) continue;
   				if (c=='#') break;
   				sb.append(c);
   			}
   			if (sb.length()>0) patternList.add(sb.toString());
   			
   		}
   	} catch (IOException e) {
   		Log.errorln("I/O failure while reading pattern file, sorry!");
   		System.exit(1);
   	}
   	return patternList;
   }
   
   

}
