/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

import java.util.HashMap;
import java.util.Map;

public class NamedSequence {
	
	private String name;
	private int[] sequence;
	private Map<String,int[]> annotationTracks;
	
	public NamedSequence(String name, int[] sequence) {
		this.name = name;
		this.sequence = sequence;
		this.annotationTracks = new HashMap<String,int[]>();
	}
	
	public String getName() { 
		return name;
	}
	
	public int[] getSequence() {
		return sequence;
	}
	
	public int[] getAnnotationTrack(String name) {
		return annotationTracks.get(name);
	}
	
	public void addAnnotationTrack(String name, int[] data) {
		if (data.length!=sequence.length) throw new IllegalArgumentException();
		annotationTracks.put(name, data);
	}
	
	public int length() {
		return sequence.length;
	}
	
}
