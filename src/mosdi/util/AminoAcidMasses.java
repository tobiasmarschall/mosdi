/*    Copyright 2012 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

/** Class providing masses of all amino acids. */
public class AminoAcidMasses {
	
	/** Returns the mass of a given amino acid w.r.t. the most common isotope.
	 *  @param aminoAcid The index of the amino acid as given by Alphabet.getAminoAcidAlphabet().
	 */
	static public double getMainMass(int aminoAcid) {
		char a = Alphabet.getAminoAcidAlphabet().get(aminoAcid);
		// Average masses taken from
		// http://education.expasy.org/student_projects/isotopident/htdocs/aa-list.html
		switch (a) {
		case 'A': return 71.0788;
		case 'C': return 103.1388;
		case 'D': return 115.0886;
		case 'E': return 129.1155;
		case 'F': return 147.1766;
		case 'G': return 57.0519;
		case 'H': return 137.1411;
		case 'I': return 113.1594;
		case 'K': return 128.1741;
		case 'L': return 113.1594;
		case 'M': return 131.1926;
		case 'N': return 114.1038;
		case 'P': return 97.1167;
		case 'Q': return 128.1307;
		case 'R': return 156.1875;
		case 'S': return 87.0782;
		case 'T': return 101.1051;
		case 'V': return 99.1326;
		case 'W': return 186.2132;
		case 'Y': return 163.1760;
		default: throw new IllegalStateException();
		}
	}

	/** Returns the mass of a given amino acid w.r.t. the most common isotope.
	 *  @param aminoAcid The index of the amino acid as given by Alphabet.getAminoAcidAlphabet().
	 *  @param massAccuracy Mass unit w.r.t. which masses are discretized; e.g., a value
	 *                      of 0.1 means that masses are handled in discrete units of 0.1Da.
	 */
	static public int getMainMass(int aminoAcid, double massAccuracy) {
		return (int)Math.round(getMainMass(aminoAcid) / massAccuracy); 
	}
	
	/** Returns the isotopic mass distribution of the given amino acid. 
	 *  @param aminoAcid The index of the amino acid as given by Alphabet.getAminoAcidAlphabet().
	 */
	static public MassDistribution getMassDistribution(int aminoAcid) {
		// TODO TODO
		return null;
	}
}
