/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

import java.util.Arrays;

public class ArrayUtils {

	/** Compares w.r.t lexicographic ordering. If null is given as 
	 *  a parameter, null is considered smaller than any non-null array. */
	public static int compare(int[] array1, int[] array2) {
		if (array1==array2) return 0;
		if (array1==null) return -1;
		if (array2==null) return 1;
		for (int i=0; i<Math.min(array1.length, array2.length); ++i) {
			if (array1[i]<array2[i]) return -1;
			if (array1[i]>array2[i]) return 1;
		}
		if (array1.length<array2.length) return -1;
		if (array1.length>array2.length) return 1;
		return 0;
	}
	
	/** Returns a copy of the given array where the order has been reversed. */
	public static int[] reverseCopy(int[] array) {
		int[] result = new int[array.length];
		for (int i=0; i<array.length; ++i) {
			result[i] = array[array.length-i-1];
		}
		return result;
	}
	
	/** Given an array with values in [0..basis-1], returns 
	 *  \sum_{i=0}^{len-1} array[i]*basis^i. */
	public static int encode(int[] array, int basis) {
		int result = 0;
		int n = 1;
		for (int i=0; i<array.length; ++i) {
			result += array[i]*n;
			n *= basis;
		}
		return result;
	}
	
	/** Counterpart to encode(). */
	public static int[] decode(int value, int basis, int length) {
		int[] result = new int[length];
		for (int i=0; i<length; ++i) {
			result[i] = value%basis;
			value /= basis;
		}
		return result;
	}

	public static double sum(double[] array) {
		double sum = 0.0;
		for (double element : array) sum += element;
		return sum;
	}
	
	public static double[] normalizedCopy(double[] array) {
		return normalizedCopy(array, 1.0);	
	}
	
	public static double[] normalizedCopy(double[] array, double sum) {
		double[] copy = Arrays.copyOf(array, array.length);
		normalize(copy, sum);
		return copy;
	}
	
	/** Normalizes the given array (in place) such the sum of elements to the given value. */
	public static void normalize(double[] array, double sum) {
		double currentSum = 0.0;
		for (double element : array) currentSum += element;
		double f = sum/currentSum;
		for (int i=0; i<array.length; ++i) array[i] = array[i]*f;
	}

	/** Normalizes the given array (in place) such the sum of elements is 1.0. */
	public static void normalize(double[] array) {
		normalize(array, 1.0);
	}
	
}
