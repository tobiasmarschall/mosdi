/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Represents a sequence composed of multiple shorter sequences. Additional 
 *  sequences can be appended. */
public class ComposedSequence implements CharSequence {

	private List<CharSequence> sequences;
	// accumulatedLengths[i] = strings[0].length + ... + strings[i].length 
	private List<Integer> accumulatedLengths;
	
	public ComposedSequence() {
		this.sequences = new ArrayList<CharSequence>();
		this.accumulatedLengths = new ArrayList<Integer>();
	}
	
	public void append(CharSequence sequence) {
		if (sequences.isEmpty()) {
			accumulatedLengths.add(sequence.length());
		} else {
			accumulatedLengths.add(length() + sequence.length());
		}
		sequences.add(sequence);
	}
	
	@Override
	public char charAt(int index) {
		int totalLength = accumulatedLengths.get(accumulatedLengths.size()-1);
		if (sequences.isEmpty() || (index<0) || (index>=totalLength)) throw new IndexOutOfBoundsException(Integer.toString(index));
		int i = Collections.binarySearch(accumulatedLengths, index);
		int stringIndex;
		int charIndex;
		// was key exactly found ? 
		if (i>=0) {
			stringIndex = i+1;
			charIndex = 0;
		} else {
			stringIndex = -(i+1);
			charIndex = index;
			if (stringIndex>0) charIndex -= accumulatedLengths.get(stringIndex-1);
		}
		return sequences.get(stringIndex).charAt(charIndex);
	}

	@Override
	public int length() {
		return accumulatedLengths.get(accumulatedLengths.size()-1);
	}

	private static class Subsequence implements CharSequence {
		private int start;
		private int end;
		private ComposedSequence parent;
		private Subsequence(ComposedSequence parent, int start, int end) {
			if ((start<0) || (end<0) || (end<start) || (end>parent.length())) {
				throw new IndexOutOfBoundsException();
			}
			this.parent = parent;
			this.start = start;
			this.end = end;
		}

		@Override
		public char charAt(int index) {
			return parent.charAt(index+start);
		}

		@Override
		public int length() {
			return end - start;
		}

		@Override
		public CharSequence subSequence(int start, int end) {
			if ((start<0) || (end<0) || (end<start) || (end>length())) {
				throw new IndexOutOfBoundsException();
			}
			return new Subsequence(parent, start + this.start, end + this.start);
		}
	}
	
	@Override
	public CharSequence subSequence(int start, int end) {
		return new Subsequence(this, start, end);
	}

}
