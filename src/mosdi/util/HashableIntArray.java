/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

import java.util.Arrays;

public class HashableIntArray {
	private int[] array;
	
	public HashableIntArray(int size) {
		array = new int[size];
	}

	public HashableIntArray(int[] array) {
		this.array = array;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof HashableIntArray)) return false;
		return Arrays.equals(array, ((HashableIntArray)obj).array);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(array);
	}	

	public int[] array() {
		return array;
	}

	@Override
	public String toString() {
		return Arrays.toString(array);
	}
	
}
