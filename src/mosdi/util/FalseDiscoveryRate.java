package mosdi.util;

import java.util.List;

public class FalseDiscoveryRate {
	
	/** For a given list of hypotheses with a p-value (which is assumed to be sorted by p-value), performs
	 *  Benjamini-Hochberg procedure and returns the number of hypotheses to retain. Objects with a p-value
	 *  greater than fdrThreshold may be omitted from the list, but the total number of hypotheses must
	 *  be given as parameter totalHypotheses. */
	static public int control(List<? extends ObjectWithPValue> hypotheses, double fdrThreshold, long totalHypotheses) {
		int result = 0;
		double f = Math.log(fdrThreshold) - Math.log(totalHypotheses);
		for (int i=0; i<hypotheses.size(); ++i) {
			if (hypotheses.get(i).getLogPValue() <= f + Math.log(i+1)) {
				result = i+1;
			}
		}
		return result;
	}

	/** For a given list of hypotheses with a p-value (which is assumed to be sorted by p-value), performs
	 *  Benjamini-Hochberg procedure and returns the number of hypotheses to retain. Objects with a p-value
	 *  greater than fdrThreshold may be omitted from the list, but the total number of hypotheses must
	 *  be given as parameter totalHypotheses. */
	static public int control(List<? extends ObjectWithPValue> hypotheses, double fdrThreshold) {
		return control(hypotheses, fdrThreshold, hypotheses.size());
	}

}
