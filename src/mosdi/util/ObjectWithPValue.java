package mosdi.util;

public interface ObjectWithPValue {
	
	public double getPValue();
	
	public double getLogPValue();
	
}
