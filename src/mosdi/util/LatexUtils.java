/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

public class LatexUtils {

	/** Makes a strings suitable for latex printing, i.e. escape critical characters, etc.. */
	static public String format(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i=0; i<s.length(); ++i) {
			sb.append(format(s.charAt(i)));
		}
		return sb.toString();
	}

	/** Makes a character suitable for latex printing, i.e. escape critical characters, etc.. */
	static public String format(char c) {
		if (c=='#') return "\\#";
		if (c=='$') return "\\$";
		if (c=='%') return "\\%";
		return ""+c;
	}

}
