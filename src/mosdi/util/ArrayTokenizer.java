/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.util;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayTokenizer implements Iterator<int[]> {

	private int[] input;
	private int delimiter;
	/** Begin if next element. */
	private int begin;
	private int[] next;
	
	public ArrayTokenizer(int[] input, int delimiter) {
		super();
		this.input = input;
		this.delimiter = delimiter;
		begin = 0;
		findNext();
	}

	private void findNext() {
		if (begin>=input.length) {
			next = null;
			return;
		}
		while (input[begin]==delimiter) {
			begin+=1;
			if (begin==input.length) {
				next = null;
				return;
			}
		}
		int i=begin+1;
		for (; i<input.length; ++i) {
			if (input[i]==delimiter) break;
		}
		next = Arrays.copyOfRange(input, begin, i);
		begin = i+1;
	}
	
	
	@Override
	public boolean hasNext() {
		return next!=null;
	}

	@Override
	public int[] next() {
		if (next==null) throw new NoSuchElementException();
		int[] result = next;
		findNext();
		return result;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}
