/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.discovery.strategies;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mosdi.discovery.EvaluatedPattern;
import mosdi.discovery.MotifFinder;
import mosdi.discovery.ObjectiveFunction;
import mosdi.discovery.ParetoSet;
import mosdi.discovery.ScoreAndPValue;
import mosdi.discovery.MotifFinder.SearchState;
import mosdi.util.Alphabet;

public class BruteForceSearch implements MotifFinder.SearchSpecification {

	private List<ObjectiveFunction> objectives;
	private Alphabet alphabet;
	private PrintStream printStream;
	private ParetoSet<EvaluatedPattern> paretoSet;
	private SearchState searchState;
	
	public BruteForceSearch(ObjectiveFunction objective, Alphabet alphabet, PrintStream printStream) {
		this.objectives = new ArrayList<ObjectiveFunction>(1);
		this.objectives.add(objective);
		this.alphabet = alphabet;
		this.printStream = printStream;
		this.paretoSet = new ParetoSet<EvaluatedPattern>(objectives.size());
	}

	public BruteForceSearch(List<ObjectiveFunction> objectives, Alphabet alphabet, PrintStream printStream) {
		this.objectives = objectives;
		this.alphabet = alphabet;
		this.printStream = printStream;
		this.paretoSet = new ParetoSet<EvaluatedPattern>(objectives.size());
	}

	@Override
	public void initialize(SearchState searchState) {
		this.searchState = searchState;
		for (ObjectiveFunction o : objectives) {
			o.initialize(searchState);
		}
		this.paretoSet = new ParetoSet<EvaluatedPattern>(objectives.size());
	}

	@Override
	public void updatePattern(int newCharacter, int leftmostChangedPosition) {
		for (ObjectiveFunction o : objectives) {
			o.updatePattern(newCharacter, leftmostChangedPosition);		
		}
	}

	@Override
	public boolean check(int prefixLength, int[] nodes) {
		return true;
	}

	@Override
	public void evaluateCandidate(int[] nodes) {
		ScoreAndPValue[] scores = new ScoreAndPValue[objectives.size()];
		for (int i=0; i<objectives.size(); ++i) {
			scores[i] = objectives.get(i).evaluate(nodes, 1.0);
		}
		EvaluatedPattern ep = new EvaluatedPattern(searchState.getPattern(), scores);
		if (!ep.containsNaNs()) paretoSet.update(ep);
		if (printStream!=null) {
			printStream.println(ep.toString(alphabet, objectives));
		}
	}
	
	@Override
	public Collection<EvaluatedPattern> getResults() {
		return paretoSet.getParetoSet();
	}

}
