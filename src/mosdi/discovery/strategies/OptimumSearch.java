/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.discovery.strategies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mosdi.discovery.EvaluatedPattern;
import mosdi.discovery.MotifFinder;
import mosdi.discovery.ObjectiveFunction;
import mosdi.discovery.ParetoSet;
import mosdi.discovery.ScoreAndPValue;
import mosdi.discovery.MotifFinder.SearchState;

public class OptimumSearch implements MotifFinder.SearchSpecification {

	private List<ObjectiveFunction> objectives;
	private ParetoSet<EvaluatedPattern> paretoSet;
	private SearchState searchState;
	

	public OptimumSearch(ObjectiveFunction objective) {
		this.objectives = new ArrayList<ObjectiveFunction>(1);
		this.objectives.add(objective);
		this.paretoSet = new ParetoSet<EvaluatedPattern>(objectives.size());
	}

	public OptimumSearch(List<ObjectiveFunction> objectives) {
		this.objectives = objectives;
		this.paretoSet = new ParetoSet<EvaluatedPattern>(objectives.size());
	}

	@Override
	public void initialize(SearchState searchState) {
		this.searchState = searchState;
		for (ObjectiveFunction o : objectives) {
			o.initialize(searchState);
		}
		this.paretoSet = new ParetoSet<EvaluatedPattern>(objectives.size());
	}
	
	@Override
	public void updatePattern(int newCharacter, int leftmostChangedPosition) {
		for (ObjectiveFunction o : objectives) {
			o.updatePattern(newCharacter, leftmostChangedPosition);
		}
	}

	@Override
	public boolean check(int prefixLength, int[] nodes) {
		ScoreAndPValue[] scores = new ScoreAndPValue[objectives.size()];
		for (int i=0; i<objectives.size(); ++i) {
			scores[i] = new ScoreAndPValue(-1,-Math.log(objectives.get(i).pValueLowerBound(prefixLength, nodes)));
		}
		return !paretoSet.isDominated(new EvaluatedPattern(null, scores));
	}

	@Override
	public void evaluateCandidate(int[] nodes) {
		// case distinction not strictly necessary, but (a bit) faster in case
		// of objectives.size()==1.
		if (objectives.size()==1) {
			ScoreAndPValue s;
			if (paretoSet.getParetoSet().isEmpty()) {
				s = objectives.get(0).evaluate(nodes, 1.0);
			} else {
				s = objectives.get(0).evaluate(nodes, Math.exp(-paretoSet.getParetoSet().first().getMinusLogPValue()));
			}
			if (s!=null) {
				if (!Double.isNaN(s.getMinusLogPValue())) paretoSet.update(new EvaluatedPattern(searchState.getPattern(),s));
			}
		} else {
			ScoreAndPValue[] scores = new ScoreAndPValue[objectives.size()];
			int last = objectives.size()-1; 
			// Evaluate w.r.t all but one objective
			for (int i=0; i<last; ++i) {
				scores[i] = objectives.get(i).evaluate(nodes, 1.0);
			}
			scores[last] = new ScoreAndPValue();
			EvaluatedPattern ep = new EvaluatedPattern(searchState.getPattern(), scores);
			// evaluate w.r.t. last objective, now we can determine a threshold
			ScoreAndPValue s = objectives.get(last).evaluate(nodes, Math.exp(-paretoSet.dominationThreshold(ep)));
			if (s!=null) {
				ep.setScore(last, s);
				if (!ep.containsNaNs()) paretoSet.update(ep);
			}
		}
	}
	
	@Override
	public Collection<EvaluatedPattern> getResults() {
		return paretoSet.getParetoSet();
	}
	
}
