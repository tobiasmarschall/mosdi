/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.discovery.strategies;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import mosdi.discovery.EvaluatedPattern;
import mosdi.discovery.MotifFinder;
import mosdi.discovery.ObjectiveFunction;
import mosdi.discovery.ScoreAndPValue;
import mosdi.discovery.MotifFinder.SearchState;
import mosdi.util.Alphabet;

public class ThresholdSearch implements MotifFinder.SearchSpecification {

	private List<ObjectiveFunction> objectives;
	private double pValueThreshold;
	private double minusLogPValueThreshold;
	private ArrayList<EvaluatedPattern> results;
	private PrintStream resultStream;
	private SearchState searchState;
	private boolean andMode;
	

	public ThresholdSearch(ObjectiveFunction objective, double pValueThreshold) {
		this(objective, pValueThreshold, null);
	}

	public ThresholdSearch(ObjectiveFunction objective, double pValueThreshold, PrintStream resultStream) {
		this.objectives = new ArrayList<ObjectiveFunction>(1);
		this.objectives.add(objective);
		this.pValueThreshold = pValueThreshold;
		this.minusLogPValueThreshold = -Math.log(pValueThreshold);
		this.results = null;
		this.resultStream = resultStream;
	}
	
	public ThresholdSearch(List<ObjectiveFunction> objectives, double pValueThreshold) {
		this(objectives, pValueThreshold, null, false);
	}

	/** If this constructor is used, the results are not stored but written 
	 *  to the given stream. */
	public ThresholdSearch(List<ObjectiveFunction> objectives, double pValueThreshold, PrintStream resultStream) {
		this(objectives, pValueThreshold, resultStream, false);
	}
	
	/** Constructor.
	 * @param andMode If true, only motifs are found for which ALL objectives are below threshold. */
	public ThresholdSearch(List<ObjectiveFunction> objectives, double pValueThreshold, PrintStream resultStream, boolean andMode) {
		this.objectives = objectives;
		this.pValueThreshold = pValueThreshold;
		this.minusLogPValueThreshold = -Math.log(pValueThreshold);
		this.results = null;
		this.resultStream = resultStream;
		this.andMode = andMode;
	}

	@Override
	public void initialize(SearchState searchState) {
		this.searchState = searchState;
		for (ObjectiveFunction o : objectives) {
			o.initialize(searchState);
		}
		results = new ArrayList<EvaluatedPattern>();
	}

	@Override
	public void updatePattern(int newCharacter, int leftmostChangedPosition) {
		for (ObjectiveFunction o : objectives) {
			o.updatePattern(newCharacter, leftmostChangedPosition);
		}
	}

	@Override
	public boolean check(int prefixLength, int[] nodes) {
		if (andMode) {
			for (ObjectiveFunction o : objectives) {
				if (o.pValueLowerBound(prefixLength, nodes)>pValueThreshold) return false;
			}
			return true;
		} else {
			for (ObjectiveFunction o : objectives) {
				if (o.pValueLowerBound(prefixLength, nodes)<=pValueThreshold) return true;
			}
			return false;
		}
	}

	@Override
	public void evaluateCandidate(int[] nodes) {
		ScoreAndPValue[] scores = new ScoreAndPValue[objectives.size()];
		boolean valid;
		if (andMode) {
			valid = true;
			for (int i=0; i<objectives.size(); ++i) {
				scores[i] = objectives.get(i).evaluate(nodes, pValueThreshold);
				if ((scores[i]==null) || Double.isNaN(scores[i].getMinusLogPValue()) || (scores[i].getMinusLogPValue()<minusLogPValueThreshold)) {
					valid = false;
				}
			}
		} else {
			valid = false;
			for (int i=0; i<objectives.size(); ++i) {
				scores[i] = objectives.get(i).evaluate(nodes, pValueThreshold);
				if ((scores[i]!=null) && !(Double.isNaN(scores[i].getMinusLogPValue())) && (scores[i].getMinusLogPValue()>=minusLogPValueThreshold)) {
					valid = true;
				}
			}
		}
		if (valid) {
			for (int i=0; i<objectives.size(); ++i) {
				if (scores[i]==null) {
					scores[i] = objectives.get(i).evaluate(nodes, 1.0);
				}
			}
			EvaluatedPattern ep = new EvaluatedPattern(searchState.getPattern(), scores);
			if (resultStream==null) {
				results.add(ep);
			} else {
				resultStream.println(ep.toString(Alphabet.getIupacAlphabet(), objectives));
			}
		}
	}

	@Override
	public List<EvaluatedPattern> getResults() {
		return results;
	}
	
}
