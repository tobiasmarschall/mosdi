/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.discovery;

/** Models possible solutions in multi-objective optimization. Solution candidates 
 *  must be comparable to allow storage in a sorted set. The imposed order should be
 *  compatible with equals(). It is not necessary (though not harmful) to use the
 *  objective scores for the compareTo() method. 
 */
public interface SolutionCandidate extends Comparable<SolutionCandidate> {
	/** Returns the number of objectives with respect to which the solution
	 *  candidate has be evaluated.
	 */
	public int getObjectiveCount();

	/** Returns the value of the objective function with the given number.
	 *  Larger values are assumed to be better.
	 */
	public double getObjectiveScore(int objectiveNumber);
	
}
