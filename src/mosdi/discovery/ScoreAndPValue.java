/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.discovery;

import mosdi.util.LogSpace;

public class ScoreAndPValue implements Comparable<ScoreAndPValue> {
	private int score;
	private double minusLogPValue;
	
	public ScoreAndPValue() {
		score = -1;
		minusLogPValue = Double.NaN;
	}
	
	public ScoreAndPValue(int score) {
		this.score = score;
		this.minusLogPValue = Double.NaN;
	}

	public ScoreAndPValue(int score, double minusLogPValue) {
		this.score = score;
		this.minusLogPValue = minusLogPValue;
	}

	public int getScore() { return score; }
	
	public double getPValue() { return Math.exp(-minusLogPValue); }

	public double getMinusLogPValue() {	return minusLogPValue; }

	@Override
	public int compareTo(ScoreAndPValue o) {
		double d1 = Double.isNaN(minusLogPValue)?-1.0:minusLogPValue;
		double d2 = Double.isNaN(o.minusLogPValue)?-1.0:o.minusLogPValue;
		return Double.compare(d1, d2);
	}

	@Override
	public String toString() {
		return String.format("(%d,%s)",score,LogSpace.toString(-minusLogPValue));
	}

}
