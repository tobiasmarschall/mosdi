/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.index;

import mosdi.util.BitArray;

/** Class encapsulating an interactive walk on a suffix tree. After
 *  each movement, the current set of active nodes is returned. */
public interface SuffixTreeWalker {

	/** Advances current position by one character. */
	int[] forward(int character);

	/** Advances current position by one generalized character. A generalized
	 *  character possibly matches more the one character. Therefore the number
	 *  of active nodes may increase. */
	int[] forward(BitArray generalizedChar);

	/** (Possibly) shortens the pattern that is implicitly encoded in the 
	 *  current position on suffix tree. */
	int[] backward(int charactersToKeep);
	
	/** Returns the currently active nodes. */
	int[] getNodes();
}
