package mosdi.examples;

import mosdi.util.Alphabet;

public class SimplePatternStatistics {

	public static void main(String[] args) {
		Alphabet alphabet = Alphabet.getDnaAlphabet();
		GMinusCDAA daa = new GMinusCDAA(10);
		int[] string = alphabet.buildIndexArray("CCGCACCATTTAGG");
		int value = daa.computeValue(string);
		System.out.println(daa.valueToDifference(value));
		
		
//		String iupacPattern = "ACGNNRT";
//		CDFA cdfa = DFAFactory.buildFromIupacPattern(iupacPattern);
//		int maxValue = 100;
//		DAA daa = new MatchCountDAA(cdfa, maxValue);
//		
//		Alphabet dnaAlphabet = Alphabet.getDnaAlphabet();
//		List<NamedSequence> sequences = SequenceUtils.readFastaFile("background.fasta", dnaAlphabet);
//		int modelOrder = 3;
//		FiniteMemoryTextModel textModel = SequenceUtils.buildTextModelFromNamedSequences(sequences, dnaAlphabet.size(), modelOrder);
//		
//		PAA paa = new TextBasedPAA(daa, textModel);
//		
//		// int iterations = 250;
//		// double[] d = paa.computeValueDistribution(iterations);
//		
//		long iterations = 3000000000L;
//		double[] d = paa.computeValueDistributionViaDoubling(iterations);
	}

}
