package mosdi.examples;

import mosdi.paa.DAA;
import mosdi.util.Alphabet;

public class GMinusCDAA extends DAA {
	private Alphabet alphabet = Alphabet.getDnaAlphabet();
	private int maxAbsValue;
	
	public GMinusCDAA(int maxAbsValue) {
		super();
		this.maxAbsValue = maxAbsValue;
	}

	@Override
	public int getAlphabetSize() {
		return alphabet.size();
	}

	@Override
	public int getStartState() {
		return 0;
	}

	@Override
	public int getStateCount() {
		return 3;
	}

	@Override
	public int getTransitionTarget(int state, int character) {
		if (character == alphabet.getIndex('C')) {
			return 0;
		} else if (character == alphabet.getIndex('G')) {
			return 2;
		} else {
			return 1;
		}
	}

	@Override
	public int getEmission(int state) {
		return state;
	}

	@Override
	public int getEmissionCount() {
		return 3;
	}

	@Override
	public int getStartValue() {
		return maxAbsValue;
	}

	@Override
	public int getValueCount() {
		return 2 * maxAbsValue + 1;
	}

	@Override
	public int performOperation(int state, int value, int emission) {
		if ((value == 0) || (value == 2*maxAbsValue)) return value;
		return value + (emission-1);
	}

	public int valueToDifference(int value) {
		return value - maxAbsValue;
	}
}
