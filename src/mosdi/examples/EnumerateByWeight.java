/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.examples;

import mosdi.util.iterators.AbelianPatternInstanceIterator;

public class EnumerateByWeight {
	
	public static void main(String[] args) {
		int maxWeight = 3;
		int length = 10;
		for (int weight=0; weight<=maxWeight; ++weight) {
			System.out.println(String.format("---- weight: %d ----",weight));
			// array with letter frequencies
			int[] frequencies = {length-weight, weight};
			AbelianPatternInstanceIterator iterator = new AbelianPatternInstanceIterator(frequencies);
			while (iterator.hasNext()) {
				int[] string = iterator.next();
				StringBuilder sb = new StringBuilder();
				for (int i : string) sb.append(i);
				System.out.println(sb.toString());
			}
		}
	}
	
}
