/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.distributions;

/** Encapsulates a distribution and is able to fastly compute the 
 *  x-fold convolution of this distribution with itself. */
public interface DistributionConvolver {

	public abstract double[] getDistribution(int selfConvolution);

	/** Returns the (right) cumulative distribution function, i.e. result[0]==1. */
	public abstract double[] getCCDF(int selfConvolution);

	/** If D is the underlying distribution (given on construction time),
	 *  then D^{*selfConvolution}(>=value) is returned. */
	public abstract double getPValue(int selfConvolution, int value);

}
