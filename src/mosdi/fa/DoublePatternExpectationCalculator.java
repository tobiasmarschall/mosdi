/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.fa;

import mosdi.discovery.TextModelIupacBounds;
import mosdi.util.Iupac;
import mosdi.util.IupacStringConstraints;

/** Class to compute the joint expectation of pattern and its reverse complement. */
public class DoublePatternExpectationCalculator implements PatternExpectationCalculator {
	private int[] pattern;
	private PatternExpectationCalculator forwardPec;
	private PatternExpectationCalculator backwardPec;
	
	public DoublePatternExpectationCalculator(FiniteMemoryTextModel textModel, int[] pattern) {
		this(textModel, pattern, new TextModelIupacBounds(textModel,false), new TextModelIupacBounds(textModel.reverseTextModel(), false));
	}
	
	public DoublePatternExpectationCalculator(FiniteMemoryTextModel textModel, int[] pattern, TextModelIupacBounds forwardBounds, TextModelIupacBounds backwardBounds) {
		this.pattern = pattern;
		forwardPec = new ForwardPatternExpectationCalculator(textModel, pattern, forwardBounds);
		backwardPec = new ForwardPatternExpectationCalculator(textModel.reverseTextModel(), Iupac.complementary(pattern), backwardBounds);
	}
	
	@Override
	public double getPrefixExpectation(int prefixLength) {
		return forwardPec.getPrefixExpectation(prefixLength) + backwardPec.getPrefixExpectation(prefixLength);
	}

	@Override
	public void setPatternPosition(int position, int newChar) {
		forwardPec.setPatternPosition(position, newChar);
		backwardPec.setPatternPosition(position, Iupac.complementary(newChar));
	}

	@Override
	public double getExpectation() {
		return getPrefixExpectation(pattern.length);
	}

	@Override
	public double getLowerBound(int prefixLength, IupacStringConstraints suffixConstraints) {
		return forwardPec.getLowerBound(prefixLength, suffixConstraints) + backwardPec.getLowerBound(prefixLength, suffixConstraints);
	}

	@Override
	public double getUpperBound(int prefixLength, IupacStringConstraints suffixConstraints) {
		return forwardPec.getUpperBound(prefixLength, suffixConstraints) + backwardPec.getUpperBound(prefixLength, suffixConstraints);
	}

}
