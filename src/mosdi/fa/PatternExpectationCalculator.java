/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.fa;

import mosdi.util.IupacStringConstraints;

/** Abstracts classes able to compute a pattern's expectation given a text model.
 *  The pattern can be changed; implementing classes should recycle as much 
 *  intermediate results as possible.
 */
public interface PatternExpectationCalculator {

	/** Returns the expected number of occurrences of the pattern. */
	public abstract double getExpectation();

	/** Returns the expectation of the patterns prefix. */
	public abstract double getPrefixExpectation(int prefixLength);

	/** Update pattern.
	 * 
	 * @param position Index in pattern[0,...,n-1] where charakter pattern[position] shall be changed
	 * @param newChar the new character which shall replace pattern[position]
	 */
	public abstract void setPatternPosition(int position, int newChar);

	/** Returns an upper bound for the expectation of a pattern consisting of the prefix
	 *  of given length an a suffix which is subject to the given constraints. */
	public abstract double getUpperBound(int prefixLength, IupacStringConstraints suffixConstraints);
	
	/** Returns a lower bound for the expectation of a pattern consisting of the prefix
	 *  of given length an a suffix which is subject to the given constraints. */
	public abstract double getLowerBound(int prefixLength, IupacStringConstraints suffixConstraints);
	
}
