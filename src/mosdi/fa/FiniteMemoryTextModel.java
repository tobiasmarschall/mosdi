/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.fa;

import mosdi.util.BitArray;

public abstract class FiniteMemoryTextModel implements TextModel {
	
	/** Returns the number of states. */
	public abstract int getStateCount();
	
	/** Returns all targets with probability > 0.0. */
	public abstract int[] getTransitionTargets(int sourceState, int character);
	
	/** Probability of going from state sourceState to targetState and thereby
	 *  emitting the given character. */
	public abstract double getProbability(int sourceState, int character, int targetState);
	
	public abstract double getEquilibriumProbability(int state);
	
	public abstract FiniteMemoryTextModel reverseTextModel();
	
	/** Derived classes that do not have an "order", are allowed to return -1. */
	public abstract int order();
	
	/* ****************************************************************************** */
	
	/** Returns the maximal probability P(g0|g1, A), no matter what condition A
	 *  is imposed on history or future. 
	 *  @param g0 a generalized character, g0.size() must equal the alphabet size.
	 *  @param g1 a generalized character, g1.size() must equal the alphabet size.
	 *  @throws May throw an UnsupportedOperationException.
	 */
	public double maxConditionalProbability(BitArray g0, BitArray g1) {
		throw new UnsupportedOperationException();
	}

	/** Returns the minimal probability P(g0|g1, A), no matter what condition A
	 *  is imposed on history or future. 
	 *  @param g0 a generalized character, g0.size() must equal the alphabet size.
	 *  @param g1 a generalized character, g1.size() must equal the alphabet size.
	 *  @throws May throw an UnsupportedOperationException.
	 */
	public double minConditionalProbability(BitArray g0, BitArray g1) {
		throw new UnsupportedOperationException();		
	}

	/** Returns the maximal probability 
	 *  P(S_t=g0|S_{t-x}...S_{t-1}=history, S_t=g1, S_{t+1}...S_{t+x'}=future, A), 
	 *  no matter what condition A is imposed on the positions t'<t-x or t'>t+x'.
	 *  @param g0 a generalized character, g0.size() must equal the alphabet size.
	 *  @param g1 a generalized character, g1.size() must equal the alphabet size.
	 *  @param history may be null.
	 *  @param future may be null.
	 *  @throws May throw an UnsupportedOperationException.
	 */
	public double maxConditionalProbability(BitArray g0, BitArray g1, BitArray[] history, BitArray[] future) {
		throw new UnsupportedOperationException();
	}

	/** Returns the maximal probability 
	 *  P(S_t=g0|S_{t-x}...S_{t-1}=history, S_t=g1, S_{t+1}...S_{t+x'}=future, A), 
	 *  no matter what condition A is imposed on the positions t'<t-x or t'>t+x'.
	 *  @param g0 a generalized character, g0.size() must equal the alphabet size.
	 *  @param g1 a generalized character, g1.size() must equal the alphabet size.
	 *  @param history may be null.
	 *  @param future may be null.
	 *  @throws May throw an UnsupportedOperationException.
	 */
	public double minConditionalProbability(BitArray g0, BitArray g1, BitArray[] history, BitArray[] future) {
		throw new UnsupportedOperationException();		
	}

	/** Returns the maximal probability P(g0|g1, A), no matter what condition A
	 *  is imposed on history. IMPORTANT: "A" may not contain conditions on the future!
	 *  Otherwise, the bound might be wrong.
	 *  @param g0 a generalized character, g0.size() must equal the alphabet size.
	 *  @param g1 a generalized character, g1.size() must equal the alphabet size.
	 *  @throws May throw an UnsupportedOperationException.
	 */
	public double maxConditionalProbabilityNoFuture(BitArray g0, BitArray g1) {
		throw new UnsupportedOperationException();
	}

	/** Returns the minimal probability P(g0|g1, A), no matter what condition A
	 *  is imposed on history. IMPORTANT: "A" may not contain conditions on the future!
	 *  Otherwise, the bound might be wrong. 
	 *  @param g0 a generalized character, g0.size() must equal the alphabet size.
	 *  @param g1 a generalized character, g1.size() must equal the alphabet size.
	 *  @throws May throw an UnsupportedOperationException.
	 */
	public double minConditionalProbabilityNoFuture(BitArray g0, BitArray g1) {
		throw new UnsupportedOperationException();		
	}

	
	/* ****************************************************************************** */

	public double[] getEquilibriumDistribution() {
		double[] distribution = new double[getStateCount()];
		for (int state=0; state<getStateCount(); ++state) {
			distribution[state] = getEquilibriumProbability(state);
		}
		return distribution;
	}
	
	/** Probability that, in the given state, the given character is generated. */
	public double getProbability(int sourceState, int character) {
		double p = 0.0;
		for (int targetState : getTransitionTargets(sourceState, character)) {
			p+=getProbability(sourceState, character, targetState);
		}
		return p;
	}

	
	/** Expectation of observing a single occurrence of a pattern assuming to 
	 *  be in equilibrium. */	
	public double expectation(int[] pattern) {
		double[] stateDistribution = stateWiseExpectation(pattern);
		double expectation = 0.0d;
		for (int state = 0; state<getStateCount(); ++state) {
			expectation += stateDistribution[state];
		}
		return expectation;
	}

	/** Assuming to be in equilibrium, returns the expectation of observing a 
	 *  single occurrence of a pattern and ending this occurrence in a specific 
	 *  state. That means, result[c] gives the probability that, starting in 
	 *  equilibrium, the given pattern is found and after the occurrence
	 *  the text model is in state c. */	
	public double[] stateWiseExpectation(int[] pattern) {
		return statewiseProductionProbability(getEquilibriumDistribution(), pattern);
	}

	/** Returns the probability that, starting from initialState, the given
	 *  pattern is produced. */
	public double productionProbability(int initialState, int[] pattern) {
		double result = 0.0;
		for (double d : statewiseProductionProbability(initialState,pattern)) {
			result += d;
		}
		return result;
	}
	
	
	/** Result[state] is the probability that, starting from initialState, the given
	 *  pattern is produced and we end up being in state "state". */
	public double[] statewiseProductionProbability(int initialState, int[] pattern) {
		double[] initialDistribution = new double[getStateCount()];
		initialDistribution[initialState] = 1;
		return statewiseProductionProbability(initialDistribution,pattern);
	}
	
	/** Result[state] is the probability that, starting from initialDistribution, the given
	 *  pattern is produced and we end up being in state "state". */
	public double[] statewiseProductionProbability(double[] initialDistribution, int[] pattern) {
		double[] d0 = initialDistribution;
		for (int c : pattern) {
			double[] d1 = new double[getStateCount()];
			for (int sourceState=0; sourceState<getStateCount(); ++sourceState) {
				if (d0[sourceState]==0.0) continue;
				for (int targetState : getTransitionTargets(sourceState, c)) {
					d1[targetState] += d0[sourceState]*getProbability(sourceState,c,targetState);
				}
			}
			d0 = d1;
		}
		return d0;
	}
	
	/** Expectation of observing a single occurrence of a pattern assuming to 
	 *  be in equilibrium. */	
	public double expectation(BitArray[] pattern) {
		double[] stateDistribution = getEquilibriumDistribution();
		for (BitArray characterSet : pattern) {
			double[] newStateDistribution = new double[getStateCount()];
			if (characterSet.size()!=getAlphabetSize()) throw new IllegalArgumentException("Alphabet size mismatch.");
			for (int c = 0; c < characterSet.size(); ++c) {
				if (!characterSet.get(c)) continue;
				for (int sourceState = 0; sourceState < getStateCount(); ++sourceState) {
					if (stateDistribution[sourceState] == 0.0d) continue;
					for (int targetState : getTransitionTargets(sourceState, c)) {
						newStateDistribution[targetState] += stateDistribution[sourceState] * getProbability(sourceState, c, targetState);	
					}
				}
			}
			stateDistribution = newStateDistribution;
		}
		double expectation = 0.0d;
		for (int state = 0; state<getStateCount(); ++state) {
			expectation += stateDistribution[state];
		}
		return expectation;
	}

	/** Returns, for each of the given generalized characters, the minimal
	 *  possible probability of observing this character. That means, the 
	 *  minimum is taken over all possible conditions on history or future. 
	 */
	public double[] minProbabilityTable(BitArray[] generalizedAlphabet) {
		double[] result = new double[generalizedAlphabet.length];
		BitArray gAll = new BitArray(getAlphabetSize());
		gAll.invert();
		for (int i=0; i<generalizedAlphabet.length; ++i) {
			result[i] = minConditionalProbability(generalizedAlphabet[i], gAll);
		}
		return result;
	}

	/** Returns, for each of the given generalized characters, the maximal
	 *  possible probability of observing this character. That means, the 
	 *  maximum is taken over all possible conditions on history or future. 
	 */
	public double[] maxProbabilityTable(BitArray[] generalizedAlphabet) {
		double[] result = new double[generalizedAlphabet.length];
		BitArray gAll = new BitArray(getAlphabetSize());
		gAll.invert();
		for (int i=0; i<generalizedAlphabet.length; ++i) {
			result[i] = maxConditionalProbability(generalizedAlphabet[i], gAll);
		}
		return result;
	}
	
	/** Returns, for each pair (a,b) of the given generalized characters, the maximal
	 *  possible conditional probability P(S_t=a|S_t=b, A), no matter what condition A
	 *  is imposed on history or future.
	 *  
	 *   @return result[a][b]>=P(a|b, A) for all conditions A.
	 */
	public double[][] maxConditionalProbabilityTable(BitArray[] generalizedAlphabet) {
		double[][] result = new double[generalizedAlphabet.length][generalizedAlphabet.length];
		for (int i=0; i<generalizedAlphabet.length; ++i) {
			for (int j=0; j<generalizedAlphabet.length; ++j) {
				result[i][j] = maxConditionalProbability(generalizedAlphabet[i],generalizedAlphabet[j]);
			}
		}
		return result;
	}

	/** Returns, for each pair (a,b) of the given generalized characters, the minimal
	 *  possible conditional probability P(S_t=a|S_t=b, A), no matter what condition A
	 *  is imposed on history or future.
	 *  
	 *   @return result[a][b]<=P(a|b, A) for all conditions A.
	 */
	public double[][] minConditionalProbabilityTable(BitArray[] generalizedAlphabet) {
		double[][] result = new double[generalizedAlphabet.length][generalizedAlphabet.length];
		for (int i=0; i<generalizedAlphabet.length; ++i) {
			for (int j=0; j<generalizedAlphabet.length; ++j) {
				result[i][j] = minConditionalProbability(generalizedAlphabet[i],generalizedAlphabet[j]);
			}
		}
		return result;
	}
	
}
