/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.fa;

import mosdi.util.ProductEncoder;

public class ProductTextModel extends FiniteMemoryTextModel {

	private FiniteMemoryTextModel textModel0;
	private FiniteMemoryTextModel textModel1;
	private ProductEncoder characterEncoder;
	private ProductEncoder stateEncoder;

	public ProductTextModel(FiniteMemoryTextModel textModel0, FiniteMemoryTextModel textModel1) {
		this.textModel0 = textModel0;
		this.textModel1 = textModel1;
		this.characterEncoder = new ProductEncoder(textModel0.getAlphabetSize(), textModel1.getAlphabetSize());
		this.stateEncoder = new  ProductEncoder(textModel0.getStateCount(), textModel1.getStateCount());
	}

	@Override
	public double getEquilibriumProbability(int state) {
		int[] states = stateEncoder.decode(state);
		return textModel0.getEquilibriumProbability(states[0]) * textModel1.getEquilibriumProbability(states[1]);
	}

	@Override
	public double getProbability(int sourceState, int character, int targetState) {
		int[] sourceStates = stateEncoder.decode(sourceState);
		int[] characters = characterEncoder.decode(character);
		int[] targetStates = stateEncoder.decode(targetState);
		return 
			textModel0.getProbability(sourceStates[0], characters[0], targetStates[0]) * 
			textModel1.getProbability(sourceStates[1], characters[1], targetStates[1]);
	}

	@Override
	public int getStateCount() {
		return textModel0.getStateCount() * textModel1.getStateCount();
	}

	@Override
	public int[] getTransitionTargets(int sourceState, int character) {
		int[] sourceStates = stateEncoder.decode(sourceState);
		int[] characters = characterEncoder.decode(character);
		int[] targets0 = textModel0.getTransitionTargets(sourceStates[0], characters[0]);
		int[] targets1 = textModel1.getTransitionTargets(sourceStates[1], characters[1]);
		int[] result = new int[targets0.length * targets1.length];
		int n = 0;
		for (int i=0; i<targets0.length; ++i) {
			for (int j=0; j<targets1.length; ++j) {
				result[n++] = stateEncoder.encode(targets0[i], targets1[j]);
			}
		}
		return result;
	}

	@Override
	public ProductTextModel reverseTextModel() {
		return new ProductTextModel(textModel0.reverseTextModel(), textModel1.reverseTextModel());
	}

	@Override
	public int[] generateRandomText(int length) {
		return characterEncoder.encodeSequence(textModel0.generateRandomText(length), textModel1.generateRandomText(length));
	}

	@Override
	public int getAlphabetSize() {
		return characterEncoder.getValueCount();
	}

	@Override
	public int order() {
		int order0 = textModel0.order();
		int order1 = textModel1.order();
		if (order0==order1) return order0;
		return -1;
	}

}
