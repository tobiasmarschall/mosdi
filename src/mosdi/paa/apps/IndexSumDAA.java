/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.paa.apps;

import mosdi.paa.DAA;


/** DAA that sums up the indices of read text characters up to a given maximum value. */
public class IndexSumDAA extends DAA {

	private int alphabetSize;
	private int maxValue;
	
	public IndexSumDAA(int alphabetSize, int maxValue) {
		this.alphabetSize = alphabetSize;
		this.maxValue = maxValue;
	}

	@Override
	public int getAlphabetSize() {
		return alphabetSize;
	}

	@Override
	public int getEmissionCount() {
		return 1;
	}

	@Override
	public int getEmission(int state) {
		return 0;
	}

	@Override
	public int getStartState() {
		return 0;
	}

	@Override
	public int getStartValue() {
		return 0;
	}

	@Override
	public int getStateCount() {
		return alphabetSize;
	}

	@Override
	public int getValueCount() {
		return maxValue+1;
	}

	@Override
	public int performOperation(int state, int value, int emission) {
		return Math.min(maxValue, value+state);
	}

	@Override
	public int getTransitionTarget(int state, int character) {
		return character;
	}

}
