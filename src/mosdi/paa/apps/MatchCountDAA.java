/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.paa.apps;

import mosdi.fa.CDFA;
import mosdi.paa.DAA;
import mosdi.paa.SimpleOperation;

/** DAA counting the matches of a given DFA in the read text. */
public class MatchCountDAA extends DAA {

	private CDFA cdfa;
	private int maxEmission;
	private int maxValue;
	
	public MatchCountDAA(CDFA cdfa, int maxValue) {
		this.cdfa = cdfa;
		this.maxValue = maxValue;
		maxEmission = 0;
		for (int state : cdfa.getOutputStates()) {
			if (cdfa.getStateOutput(state)>maxEmission) {
				maxEmission = cdfa.getStateOutput(state);
			}
		}
	}

	@Override
	public int getAlphabetSize() {
		return cdfa.getAlphabetSize();
	}

	@Override
	public int getEmissionCount() {
		return maxEmission+1;
	}

	@Override
	public int getEmission(int state) {
		return cdfa.getStateOutput(state);
	}

	@Override
	public int getStartState() {
		return cdfa.getStartState();
	}

	@Override
	public int getStartValue() {
		return 0;
	}

	@Override
	public int getStateCount() {
		return cdfa.getStateCount();
	}

	@Override
	public int getValueCount() {
		return maxValue+1;
	}

	@Override
	public int performOperation(int state, int value, int emission) {
		return Math.min(maxValue, value+emission);
	}

	@Override
	public int getTransitionTarget(int state, int character) {
		return cdfa.getTransitionTarget(state, character);
	}

	private class Operation implements SimpleOperation {
		@Override
		public int joinValues(int value0, int value1) {
			return Math.min(maxValue, value0+value1);
		}
		@Override
		public int performOperation(int value, int emission) {
			return Math.min(maxValue, value+emission);
		}
	}

	@Override
	public SimpleOperation getOperation() {
		return new Operation();
	}
	
}
