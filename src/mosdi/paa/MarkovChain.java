/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.paa;

/** Abstraction of a discrete-time Markov chain with finite state space. */
public interface MarkovChain {
	
	/** Returns the number of states. */
	public int getStateCount();
	
	/** Returns the probability for the transition from <code>state</code>
	 *  to <code>targetState</code>. */
	public double getTransitionProbability(int state, int targetState);
	
	/** Returns the probability of being in the given <code>state</code>
	 *  at time zero, that is, before any state transition has taken place. */
	public double getInitialProbability(int state);
	
}
