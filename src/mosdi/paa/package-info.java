/**
 * (Abstract) classes / interfaces and generice algorithms for the probabilistic 
 * arithmetic automata (PAA) framework. 
 * This framework has been introduced by Marschall and Rahmann (Proceedings of the 19th
 * Annual Symposium on Combinatorial Pattern Matching (CPM), pages 95-106, 2008) and is 
 * presented in more detail in an <a href="http://arxiv.org/abs/1011.5778">arXiv article</a>.
 * <p>
 * This package also contains deterministic arithmetic automata (DAA) which can be combined
 * with a FiniteMemoryTextModel in order to obtain a PAA. This allows the use of all
 * functionality of a PAA once a DAA has been implemented. 
 */
 package mosdi.paa;
