/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.paa;

/** A PAA implementing this interface has to make several guarantees detailed below. One
 *  important example is a PAA whose operations are additions in every state. Formally,
 *  we say a PAA has simple operations, if
 * <p>
 *  1) the operations are the same in all states,
 * <p> 
 *  2) the operations are commutative in the sense that v*e_1*e_2 = v*e_2*e_1,
 *     where v is an arbitrary value, e_1 and e_2 are arbitrary emissions
 *     and * is the single operation attached to all states.
 * <p>    
 *  3) there exists a joinValues operations such that 
 *     v_0*e_1*...*e_k*e'_1*...*e'_l = join(v_0*e_1*...*e_k, v_0*e'_1*...*e'_l),
 *     where v_0 is the start value. 
 */
public interface SimpleOperation {
	
	public int performOperation(int value, int emission);
	
	public int joinValues(int value0, int value1);
	
}
