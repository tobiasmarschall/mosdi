/*    Copyright 2010 Tobias Marschall
 *
 *    This file is part of MoSDi.
 *
 *    MoSDi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MoSDi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MoSDi.  If not, see <http://www.gnu.org/licenses/>.
 */

package mosdi.paa;

import mosdi.fa.CharacterAutomaton;

/** Abstract base class for a deterministic arithmetic automaton as defined in <a href="http://arxiv.org/abs/1011.5778">this arXiv article</a>. 
 *  All states, values, and emissions must be be numbered starting at zero, i.e. the state/emission/value set is
 *  assumed to be (identified with) the set {0,...,n-1}, where n is the number of possible
 *  states/emissions/value. 
 *  <p>
 *  The alphabet a DAA acts on is assumed to be a set of integers from 0 to n.
 *  See {@link mosdi.util.Alphabet}. 
 */ 
public abstract class DAA implements CharacterAutomaton {

	/** Returns the size of the alphabet the DAA acts on. */
	@Override
	public abstract int getAlphabetSize();
	@Override
	public abstract int getStartState();
	@Override
	public abstract int getStateCount();
	/** Returns the state that results from reading the given <code>character</code> when 
	 *  in the given <code>state</code>.*/
	@Override
	public abstract int getTransitionTarget(int state, int character);
	/** Returns the emission that is (deterministically) made by the given <code>state</code>. */
	public abstract int getEmission(int state);
	/** Returns the size of the emission set. */
	public abstract int getEmissionCount();
	public abstract int getStartValue();
	/** Returns the size of the value set. */
	public abstract int getValueCount(); 
	/** Returns the value resulting from applying the operation associated with
	 *  the given <code>state</code> to the given <code>value</code> and <code>emission</code>. */
	public abstract int performOperation(int state, int value, int emission); 

	/** Returns the value computed by the DAA when starting in the start state
	 *  and processing all characters of the given <code>sequence</code> one by one. */
	public int computeValue(int[] sequence) {
		int state = getStartState();
		int value = getStartValue();
		for (int c : sequence) {
			state = getTransitionTarget(state,c);
			value = performOperation(state, value, getEmission(state));
		}
		return value;
	}

	/** Returns the operation if it can be cast into a {@link SimpleOperation}, otherwise returns null. */
	public SimpleOperation getOperation() { return null; }
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (int state=0; state<getStateCount(); ++state) {
			sb.append(String.format("%d(%d): ", state, getEmission(state)));
			for (int c=0; c<getAlphabetSize(); ++c) {
				sb.append(String.format("chr(%d)-->%d, ", c, getTransitionTarget(state,c)));
			}
			sb.append("\n");
		}
		return sb.toString();
	}

}
