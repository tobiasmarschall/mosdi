#!/bin/bash

if [ -z "$MOSDI_ALL" ] ; then  
	MOSDI_ALL="java mosdi.tools.All"
fi

function call {
	echo "-------------------------------------------------------------------------------"
	echo "Call: \"$MOSDI_ALL $1 2>&1\""
	echo "-------------------------------------------------------------------------------"
	$MOSDI_ALL $1 2>&1
}

call 'count-dist exact ACGGGCT'
call 'count-dist -f iupac exact ACGWSSVNCT'
call 'count-dist -r exact ACGWSSVNCT'
call 'count-dist -f prosite -F exact prosite-patterns'
call 'count-dist -r -F exact iupac-patterns'
call 'count-dist -c O exact AANNAA'
call 'count-dist -c N exact AANNAA'
call 'count-dist -c M exact AANNAA'
call 'count-dist -r -c O exact ACGT'
call 'count-dist -r -c M exact ACGT'
call '-v count-dist -r -n 1000 -m 20 exact ACGWSSVNCT'
call '-v count-dist -r -n 1000 -m 20 -D exact ACGWSSVNCT'
call '-v count-dist -r -n 1000 -m 20 -C exact ACGWSSVNCT'
call '-v2 count-dist -r -n 1000 -m 20 -q character-distribution exact ACGWSSVNCT'
call '-v2 count-dist -r -p one-sequence.fa exact ACVNCT'
call '-v2 count-dist -r -V3 -p one-sequence.fa exact ACVNCT'
call '-v2 count-dist -r -n 1000 -m 20 -q hg18.3grams exact AYBNNGAT'
call '-v2 count-dist -r -n 1000 -m 20 -N 10 exact ACGWSSVNCT'

call 'iupac-gen 4'
call 'iupac-gen -m 2,1,0,0 -M 4,4,4,4 4'
call 'iupac-gen -r -m 2,1,0,0 -M 4,4,4,4 4'
call 'iupac-gen -m 4,1,0,0 -M 10,4,4,4 -c 10'
call 'iupac-gen -m 1,1,0,0 -M 8,4,4,4 -c 8'
call 'iupac-ranges -m 7,0,0,1 -M 10,2,2,3 10 10'

call 'iupac-abelian-gen -m 2,2,0,0 -M 4,4,4,4 4'
call 'iupac-abelian-gen -n -m 2,2,0,0 -M 4,4,4,4 4'

call 'count-dist -f iupac poisson ACGWSSVNCT'
call 'count-dist -r poisson ACGWSSVNCT'
call 'count-dist -f prosite -F poisson prosite-patterns'
call 'count-dist -r -F poisson iupac-patterns'
call 'count-dist -c O poisson AANNAA'
call 'count-dist -c N poisson AANNAA'
call 'count-dist -c M poisson AANNAA'
call 'count-dist -r -c O poisson ACGT'
call 'count-dist -r -c M poisson ACGT'
call 'count-dist -r -n 1000 -m 20 poisson ACGWSSVNCT'
call 'count-dist -r -n 1000 -m 20 -q character-distribution poisson ACGWSSVNCT'
call 'count-dist -r -p one-sequence.fa poisson ACVNCT'
call 'count-dist -r -p one-sequence.fa -F comp-poisson iupac-patterns'

call '-v abelian-discovery -t 1e-15 -F more-sequences abelian_patterns'
call '-v abelian-discovery -t 1e-4 -F more-sequences abelian_patterns'
call '-v abelian-discovery -t 1e-4 -a -F more-sequences abelian_patterns'
call '-v abelian-discovery -t 1e-4 -E 0.1 -F more-sequences abelian_patterns'

call 'annotate ACNNGTW one-sequence.fa'
call 'annotate -r ACNNGTW one-sequence.fa'

call 'count-matches -r more-sequences.fa ACNNGT'
call 'count-matches -r -t more-sequences.fa ACNNGT'
call 'count-matches -r -s more-sequences.fa ACNNGT'

call 'generate-pfm -f - ACNNGWW more-sequences.fa'
call 'generate-pfm -f - -r ACNNGT more-sequences.fa'

call 'local-search more-sequences.fa ARA'
call 'local-search -r -l 6 more-sequences.fa ARA'

call 'discovery-mult -t 1e-15 -F more-sequences abelian_patterns'
call 'discovery-mult -t 1e-4 -F more-sequences abelian_patterns'
call 'discovery-mult -t 1e-4 -R 0.6 -F more-sequences abelian_patterns'
call 'discovery-mult -t 1e-4 -r -R 0.6 -F more-sequences abelian_patterns'

call 'calc-scores -r one-sequence.fa TANRACW'
call 'calc-scores -r more-sequences.fa TANRACW'
call 'calc-scores -r -T truth-file one-sequence.fa TANRACW'

# TODO: add option for random seed and test these two subcommands:
# call 'random_text' 
# call 'empiric_clump_stat'

call 'comp-poisson-eval TANRACW'
call 'comp-poisson-eval -F iupac-patterns'
call 'comp-poisson-eval -M2 -r -t more-sequences.fa -c 16 -n 1000 -m 30 -F iupac-patterns'

call 'cut-out-motif more-sequences.fa TAA'
call 'cut-out-motif -r more-sequences.fa TAA'

call 'count-qgrams more-sequences.fa 4'
call 'count-qgrams -r -p 1 more-sequences.fa 3'
call 'count-qgrams -r -p 0.1 more-sequences.fa 3'

call 'min-max-table more-sequences.3grams'
call 'min-max-table -g more-sequences.3grams'

call 'exp-clump-size -q more-sequences.1grams AGAGSCARANNW'
call 'exp-clump-size -F -q more-sequences.1grams iupac-patterns'
call 'exp-clump-size -r -F -q more-sequences.1grams iupac-patterns'
call 'exp-clump-size -r -F -q more-sequences.3grams iupac-patterns'

call 'discovery -m 2,2,0,0 -M 6,2,0,2 -T 1e-5 6 more-sequences.fa'
call 'discovery -m 2,2,0,0 -M 6,2,0,2 6 more-sequences.fa'
call 'discovery -m 2,2,0,0 -M 4,2,0,2 -B 4 more-sequences.fa'
call '-v 2 discovery -m 2,2,0,0 -M 4,2,0,2 -B 4 more-sequences.fa'
