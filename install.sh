#!/bin/bash

mosdi_jar="mosdi-XXX.jar"

function usage {
	echo "usage: $(basename $0) <prefix-dir> [<jar-file>]"
	exit 1
}

if [ $# -eq 0 ]; then usage; fi
if [ $# -gt 2 ]; then usage; fi

if [ $# -gt 1 ]; then
	mosdi_jar="$2"
fi

if [ ! -d $1 ]; then
	echo "Directory \"$1\" not found."
	exit 1
fi
prefixdir="$(readlink -f $1)"

if [ ! -f ${mosdi_jar} ]; then
	echo "File \"${mosdi_jar}\" not found."
	exit 1
fi

mkdir -p ${prefixdir}/share/java
mkdir -p ${prefixdir}/bin

jar="${prefixdir}/share/java/$mosdi_jar"
cp -v $mosdi_jar $jar
chmod 644 $jar

function create_start_script {
	script="${prefixdir}/bin/$1"
	echo "Creating ${script}"
	echo "#!/bin/bash" > $script
	echo "java \$MOSDI_JVM_ARGS -classpath $jar $2 \$@" >> $script
	chmod 755 $script
}

create_start_script mosdi-stat mosdi.tools.Stat
create_start_script mosdi-discovery mosdi.tools.Discovery
create_start_script mosdi-pm-analysis mosdi.tools.PatternMatchingAnalysis
create_start_script mosdi-utils mosdi.tools.Utils
create_start_script mosdi-predict-islands mosdi.tools.PredictIslands
create_start_script mosdi-dispensation-order mosdi.tools.ReadLengthAnalysis
create_start_script mosdi-seed-stat mosdi.tools.AlignmentSeedStats
create_start_script mosdi-fragment-stat mosdi.tools.ProteinFragmentStats
