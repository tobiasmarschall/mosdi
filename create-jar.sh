#!/bin/bash

# To create tarball for mosdi-1.0, call "./create-jar.sh 1.0"

set -e

joptsimple="../externals/jopt-simple-3.2.jar"

svn up
if [ 0 -ne $(svn st -q|wc -l) ]; then
	echo "Commit all modified files first."
	exit 1
fi
if [ 0 -ne $(svn st|grep -c '\.java$') ]; then
	echo "Commit all source files first."
	exit 1
fi
if [ -e jar-tmp ] ; then
	echo "Directory jar-tmp already exists, move it out of the way."
	exit 1
fi

revision=$(svn info | grep Revision | cut -d ' ' -f 2)
target="mosdi-revision${revision}.jar"

if [ -e ${target} ]; then 
	echo "Target file ${target} already exists, move it out of the way."
	exit 1
fi

mkdir jar-tmp
cd jar-tmp
echo "Compiling sources"
javac -d . -sourcepath ../src $(find ../src -name '*.java')
echo "Extracting ${joptsimple}"
jar xf ${joptsimple} joptsimple
echo "Creating jar-file"
jar cfm ../${target} ../Manifest mosdi joptsimple
cd ..
rm -rf jar-tmp

if [ $# -gt 0 ]; then
	targetversion="$1"
	jar="mosdi-${targetversion}.jar"
	tarballdir="mosdi-${targetversion}"
	tarball="${tarballdir}.tar.gz"
	echo "Creating $tarball"
	rm -rf $jar $tarball $tarballdir
	mkdir $tarballdir
	cp README COPYING install.sh $tarballdir
	cd $tarballdir
	ln -s ../$target $jar
	sed -i "s/mosdi-XXX/mosdi-${targetversion}/g" README install.sh
	cd ..
	tar -czhf $tarball $tarballdir
fi
